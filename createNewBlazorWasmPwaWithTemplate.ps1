﻿param
(
	[string] $solutionName,
	[int]    $httpPortValue
)

# 
# Articoli di riferimento utilizzati per la creazione dello script:
# 
# https://docs.microsoft.com/it-it/dotnet/core/tools/dotnet-new#blazorwasm
# https://docs.microsoft.com/it-it/dotnet/standard/frameworks
# https://docs.microsoft.com/it-it/dotnet/core/tutorials/cli-templates-create-item-template
# https://docs.microsoft.com/it-it/dotnet/core/tools/custom-templates
# https://devblogs.microsoft.com/dotnet/how-to-create-your-own-templates-for-dotnet-new/
# https://github.com/dotnet/templating/issues/2210
# https://github.com/dotnet/templating/wiki/Reference-for-template.json#symbols
# http://json.schemastore.org/template
# https://github.com/dotnet/templating/issues/1667
# 

$frameworkVersion = "net5.0"

if ($httpPortValue -ge 32000 -and $httpPortValue -le 32998) 
{
	#------------------------------------------------------------------------------------


	# VISUAL STUDIO - PORTS

	# Porta HTTPS utilizzata da Visual Studio
	$httpsPortValue = $httpPortValue + 1


	#------------------------------------------------------------------------------------


	# DEV - PORTS

	# Porta HTTPS utilizzata da Visual Studio
	$httpPortValueDEV = $httpPortValue
	$httpsPortValueDEV = $httpPortValueDEV + 1


	#------------------------------------------------------------------------------------


	# TEST - PORTS

	# Porta HTTPS utilizzata da Visual Studio
	$httpPortValueTEST = $httpPortValue - 1000
	$httpsPortValueTEST = $httpPortValueTEST + 1

	#------------------------------------------------------------------------------------
	
	
	# PROD - PORTS

	# Porta HTTPS utilizzata da Visual Studio
	$httpPortValuePROD = $httpPortValue - 2000
	$httpsPortValuePROD = $httpPortValuePROD + 1


	#------------------------------------------------------------------------------------



	# Reset del proxy necessario per i progetti "mstest" che necessariamente devono scaricare dei pacchetti nuget aggiuntivi
	set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyEnable -value 0



	#------------------------------------------------------------------------------------

	# Compongo il path della cartella relativa al repository
	$repositoryFolderPath = $PSScriptRoot

	# Modifico la working directory
	cd $repositoryFolderPath

	# Creo la cartella relativa alla nuova solution
	mkdir $solutionName

	# Modifico la working directory
	cd "$repositoryFolderPath\$solutionName"

	#------------------------------------------------------------------------------------

	$solutionNameCamelCase = $solutionName
	
	$solutionNameCamelCase = $solutionNameCamelCase.Replace($solutionNameCamelCase[0], $solutionNameCamelCase[0].ToString().ToLower())

	dotnet new M2FWasmPWA `
		--HttpPortValue $httpPortValue `
		--HttpsPortValue $httpsPortValue `
		--HttpPortValueDEV $httpPortValueDEV `
		--HttpsPortValueDEV $httpsPortValueDEV `
		--HttpPortValueTEST $httpPortValueTEST `
		--HttpsPortValueTEST $httpsPortValueTEST `
		--HttpPortValuePROD $httpPortValuePROD `
		--HttpsPortValuePROD $httpsPortValuePROD `
		--SolutionName $solutionName `
		--SolutionNameCamelCase $solutionNameCamelCase



	#------------------------------------------------------------------------------------



	# Riattivazione del proxy
	set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyEnable -value 1
}
else 
{
	Write-Error "Il valore della porta Http deve essere compreso nel range [32000, 32998]"
}