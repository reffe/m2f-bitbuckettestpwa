using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace M2FBitbucketTestPWADataLayerTest
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="UnitTest1" />
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        #region Methods

        /// <summary>
        /// The TestMethod1
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
        }

        #endregion
    }

    #endregion
}
