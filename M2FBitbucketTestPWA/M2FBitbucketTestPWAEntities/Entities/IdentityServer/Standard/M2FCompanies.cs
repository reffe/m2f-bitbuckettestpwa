﻿using M2FDto.Constant;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="M2FCompanies" />
    /// </summary>
    [Table(nameof(M2FCompanies), Schema = GeneralConstantDto.DBDboSchema)]
    public class M2FCompanies
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the M2FBusinessUnits
        /// </summary>
        public virtual ICollection<M2FBusinessUnits> M2FBusinessUnits { get; set; }

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        public string Name { get; set; }

        #endregion
    }

    #endregion
}
