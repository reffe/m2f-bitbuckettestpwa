﻿using M2FDto.Constant;
using System.ComponentModel.DataAnnotations.Schema;

namespace M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="M2FBusinessUnits" />
    /// </summary>
    [Table(nameof(M2FBusinessUnits), Schema = GeneralConstantDto.DBDboSchema)]
    public class M2FBusinessUnits
    {
        #region Properties

        /// <summary>
        /// Gets or sets the CompanyId
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsItDepartment
        /// </summary>
        public bool IsItDepartment { get; set; }

        /// <summary>
        /// Gets or sets the M2FCompanies
        /// </summary>
        public virtual M2FCompanies M2FCompanies { get; set; }

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        public string Name { get; set; }

        #endregion
    }

    #endregion
}
