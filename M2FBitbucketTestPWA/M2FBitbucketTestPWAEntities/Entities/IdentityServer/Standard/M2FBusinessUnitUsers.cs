﻿using M2FDto.Constant;
using System.ComponentModel.DataAnnotations.Schema;

namespace M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="M2FBusinessUnitUsers" />
    /// </summary>
    [Table(nameof(M2FBusinessUnitUsers), Schema = GeneralConstantDto.DBDboSchema)]
    public class M2FBusinessUnitUsers
    {
        #region Properties

        /// <summary>
        /// Gets or sets the BusinessUnitId
        /// </summary>
        public string M2FBusinessUnitsId { get; set; }

        /// <summary>
        /// Gets or sets the UserId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the M2FBusinessUnits
        /// </summary>
        public virtual M2FBusinessUnits M2FBusinessUnits { get; set; }

        /// <summary>
        /// Gets or sets the Role
        /// </summary>
        public virtual ApplicationUser User { get; set; }

        #endregion
    }

    #endregion
}
