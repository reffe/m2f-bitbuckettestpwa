using Microsoft.AspNetCore.Identity;

namespace M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="ApplicationUser" />.
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        #region Properties

        /// <summary>
        /// Gets or sets the DallasKey.
        /// </summary>
        public string DallasKey { get; set; }

        /// <summary>
        /// Gets or sets the EmplId.
        /// </summary>
        public string EmplId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsDomainUser.
        /// </summary>
        public bool IsDomainUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsTechnicalUser.
        /// </summary>
        public bool IsTechnicalUser { get; set; }

        #endregion
    }

    #endregion
}
