﻿using M2FDto.Constant;
using Microsoft.AspNetCore.Identity;

using System.ComponentModel.DataAnnotations.Schema;

namespace M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="M2FApplications" />
    /// </summary>
    [Table(nameof(M2FApplications), Schema = GeneralConstantDto.DBDboSchema)]
    public class M2FApplications
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Description
        /// </summary>
        public string Description { get; set; }

        #endregion
    }

    #endregion
}
