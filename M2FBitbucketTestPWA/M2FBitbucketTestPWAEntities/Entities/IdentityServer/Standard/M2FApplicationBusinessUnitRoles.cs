﻿using M2FDto.Constant;
using Microsoft.AspNetCore.Identity;

using System.ComponentModel.DataAnnotations.Schema;

namespace M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="M2FApplicationBusinessUnitRoles" />
    /// </summary>
    [Table(nameof(M2FApplicationBusinessUnitRoles), Schema = GeneralConstantDto.DBDboSchema)]
    public class M2FApplicationBusinessUnitRoles
    {
        #region Properties

        /// <summary>
        /// Gets or sets the M2FApplicationsId
        /// </summary>
        public string M2FApplicationsId { get; set; }

        /// <summary>
        /// Gets or sets the M2FApplications
        /// </summary>
        public virtual M2FApplications M2FApplications { get; set; }

        /// <summary>
        /// Gets or sets the BusinessUnitId
        /// </summary>
        public string M2FBusinessUnitsId { get; set; }

        /// <summary>
        /// Gets or sets the M2FBusinessUnits
        /// </summary>
        public virtual M2FBusinessUnits M2FBusinessUnits { get; set; }

        /// <summary>
        /// Gets or sets the RoleId
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// Gets or sets the Role
        /// </summary>
        public virtual IdentityRole Role { get; set; }

        #endregion
    }

    #endregion
}
