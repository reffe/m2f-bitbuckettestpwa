using Microsoft.AspNetCore.Identity;

using System.ComponentModel.DataAnnotations.Schema;

namespace M2FBitbucketTestPWAEntities.Entities.IdentityServer.Custom
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="UserRole" />
    /// </summary>
    [Table("UserRoles", Schema = "M2FBitbucketTestPWA")]
    public class UserRole : IdentityUserRole<string>
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Role
        /// </summary>
        public virtual Role Role { get; set; }

        #endregion
    }

    #endregion
}
