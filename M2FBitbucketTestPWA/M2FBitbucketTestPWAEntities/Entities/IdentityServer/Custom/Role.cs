using Microsoft.AspNetCore.Identity;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace M2FBitbucketTestPWAEntities.Entities.IdentityServer.Custom
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="Role" />
    /// </summary>
    [Table("Roles", Schema = "M2FBitbucketTestPWA")]
    public class Role : IdentityRole
    {
        /// <summary>
        /// Gets or sets the Role
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }

    #endregion
}
