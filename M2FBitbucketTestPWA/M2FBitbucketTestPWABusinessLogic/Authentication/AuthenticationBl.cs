using M2FDto;

using Microsoft.Extensions.Options;

using Serilog;

using System.Collections.Generic;

using M2FBitbucketTestPWABusinessLogicContracts.Authentication;

using M2FBitbucketTestPWADataLayerContracts.Authentication;

using M2FBitbucketTestPWADto;

using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Custom;
using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard;
using Microsoft.AspNetCore.Identity;

namespace M2FBitbucketTestPWABusinessLogic.Authentication
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="AuthenticationBl" />
    /// </summary>
    public class AuthenticationBl : IAuthenticationBl
    {
        #region Fields

        /// <summary>
        /// Defines the aboutInfo
        /// </summary>
        private readonly AboutDto aboutInfo;

        /// <summary>
        /// Defines the appSettings
        /// </summary>
        private readonly IOptions<AppSettings<AppSpecificSettings>> appSettings;

        /// <summary>
        /// Defines the authenticationDl
        /// </summary>
        private readonly IAuthenticationDl authenticationDl;

        /// <summary>
        /// Defines the logger
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationBl"/> class.
        /// </summary>
        /// <param name="appSettings">The appSettings<see cref="IOptions{AppSettings{AppSpecificSettings}}"/></param>
        /// <param name="logger">The logger<see cref="ILogger"/></param>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/></param>
        /// <param name="authenticationDl">The authenticationDl<see cref="IAuthenticationDl"/></param>
        public AuthenticationBl(
            IOptions<AppSettings<AppSpecificSettings>> appSettings,
            ILogger logger,
            AboutDto aboutInfo,
            IAuthenticationDl authenticationDl)
        {
            this.appSettings = appSettings;
            this.logger = logger;
            this.aboutInfo = aboutInfo;
            this.authenticationDl = authenticationDl;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The GetRolesByUserId
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="List{Role}"/></returns>
        public IEnumerable<IdentityRole> GetRolesByUserId(string id)
        {
            return this.authenticationDl.GetRolesByUserId(id);
        }

        /// <summary>
        /// The GetUserById
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="ApplicationUser"/></returns>
        public ApplicationUser GetUserById(string id)
        {
            return this.authenticationDl.GetUserById(id);
        }

        /// <summary>
        /// The GetUserByUsername
        /// </summary>
        /// <param name="username">The username<see cref="string"/></param>
        /// <returns>The <see cref="ApplicationUser"/></returns>
        public ApplicationUser GetUserByUsername(string username)
        {
            return this.authenticationDl.GetUserByUsername(username);
        }

        /// <summary>
        /// The ValidatePassword
        /// </summary>
        /// <param name="username">The username<see cref="string"/></param>
        /// <param name="plainTextPassword">The plainTextPassword<see cref="string"/></param>
        /// <returns>The <see cref="bool"/></returns>
        public bool ValidatePassword(string username, string plainTextPassword)
        {
            return this.authenticationDl.ValidatePassword(username, plainTextPassword);
        }

        #endregion
    }

    #endregion
}
