using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Pages
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="Index" />
    /// </summary>
    public partial class Index
    {
        #region Methods

        /// <summary>
        /// Method that shows Info Alert
        /// </summary>
        /// <param name="firstRender">Se = a ture indica che è il primo rendering della pagina</param>
        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                // Write the logic for the first rendering of the app
            }
        }

        /// <summary>
        /// sagdd xfj
        /// </summary>
        /// <returns>Ruitorna un task</returns>
        protected override async Task OnInitializedAsync()
        {
        }

        #endregion
    }

    #endregion
}
