﻿using Microsoft.AspNetCore.Components;

using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Pages
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="Counter" />
    /// </summary>
    public partial class Counter : ComponentBase
    {
        #region Fields

        /// <summary>
        /// Defines the currentCount
        /// </summary>
        private int currentCount = 0;

        #endregion

        #region Methods

        /// <summary>
        /// The OnInitializedAsync
        /// </summary>
        /// <returns>The <see cref="Task"/></returns>
        protected override Task OnInitializedAsync()
        {
            return base.OnInitializedAsync();
        }

        /// <summary>
        /// The IncrementCount
        /// </summary>
        private void IncrementCount()
        {
            this.currentCount++;
        }

        #endregion
    }

    #endregion
}
