﻿using Blazorise;
using Blazorise.DataGrid;
using M2FBitbucketTestPWAClient.Log;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Pages
{
    /// <summary>
    /// Defines the <see cref="LocalLogs" />.
    /// </summary>
    public partial class LocalLogs : ComponentBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalLogs"/> class.
        /// </summary>
        public LocalLogs()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the LogsList.
        /// </summary>
        private IEnumerable<LocalLogDto> LogsList { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The OnAfterRenderAsync.
        /// </summary>
        /// <param name="firstRender">The firstRender<see cref="bool"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await this.Refresh();
            }
        }

        /// <summary>
        /// The OnInitializedAsync.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        protected override Task OnInitializedAsync()
        {
            return base.OnInitializedAsync();
        }

        private async Task Refresh()
        {
            await this.localMemoryLog.CleanOlderLog();

            ////bool bPresence = await this.localMemoryLog.GetPresenceLogOnLocalStorage();
            this.LogsList = await this.localMemoryLog.GetLogsList();

            this.StateHasChanged();
        }

        /// <summary>
        /// The ClearLocalStorage.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task ClearLocalStorage()
        {
            await this.localMemoryLog.ClearLocalStorage();
        }

        /// <summary>
        /// The RemoveLogs.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task RemoveLogs()
        {
            await this.localMemoryLog.RemoveAllLogs();

            await this.Refresh();
        }

        private void OnRowStyling(LocalLogDto log, DataGridRowStyling styling)
        {
            switch (log.Level)
            {
                case LocalLogDto.LevelEnum.FTL:
                    styling.Style = "color: violet;";
                    break;
                case LocalLogDto.LevelEnum.ERR:
                    styling.Style = "color: red;";
                    break;
                case LocalLogDto.LevelEnum.WRN:
                    styling.Style = "color: orange;";
                    break;
                case LocalLogDto.LevelEnum.INF:
                    styling.Style = "color: green;";
                    break;
                default:
                case LocalLogDto.LevelEnum.DBG:
                    styling.Style = "color: black;";
                    break;
            }
        }

        private void OnSelectedRowStyling(LocalLogDto log, DataGridRowStyling styling)
        {
            styling.Background = Background.Info;
        }

        #endregion
    }
}
