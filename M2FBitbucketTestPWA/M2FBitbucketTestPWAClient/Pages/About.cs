﻿using Blazorise;
using M2FDto;
using M2FDto.Constant;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Pages
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="About" />.
    /// </summary>
    public partial class About : ComponentBase
    {
        #region Fields

        /// <summary>
        /// Defines the errorModal.
        /// </summary>
        private Modal errorModal;

        /// <summary>
        /// Defines the modalMessage.
        /// </summary>
        private string modalMessage;

        /// <summary>
        /// Defines the tooltipCustomTemplate.
        /// </summary>
        private string tooltipCustomTemplate =
            "<div class='tooltip' role='tooltip'>" +
            "<div class='arrow'></div>" +
            "<div class='tooltip-inner tooltip-alignment-left'></div>" +
            "</div>";

        /// <summary>
        /// Defines the selectedTab.
        /// </summary>
        private string selectedTab = "general";

        #endregion

        #region Methods

        /// <summary>
        /// The OnAfterRenderAsync.
        /// </summary>
        /// <param name="firstRender">The firstRender<see cref="bool"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await this.JSRuntime.InvokeVoidAsync("addToolTips");

                await this.GetAboutInfoServer();
            }
        }

        /// <summary>
        /// The OnInitializedAsync.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        protected override Task OnInitializedAsync()
        {
            return base.OnInitializedAsync();
        }

        /// <summary>
        /// The GetAboutInfoServer.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task GetAboutInfoServer()
        {
            var httpClientAnonymous = this.httpClientFactory.CreateClient(HttpConstantDto.ChannelNameAnonymus);
            try
            {
                this.aboutInfoServer = await this.apiManager.CallApi<M2FAboutDtoServer>(httpClientAnonymous, ApiConstantDto.AboutApiController, HttpMethod.Get, string.Empty);
                this.StateHasChanged();
            }
            catch (Exception ex)
            {
                this.modalMessage = $"Il metodo per reperire le informazioni del Server ha restituito l'eccezione: {ex.Message}";
                await this.logger.Error(this.modalMessage);
                this.ShowErrorModal();
            }
        }

        /// <summary>
        /// The HideErrorModal.
        /// </summary>
        private void HideErrorModal()
        {
            this.errorModal.Hide();
        }

        /// <summary>
        /// The ShowErrorModal.
        /// </summary>
        private void ShowErrorModal()
        {
            this.errorModal.Show();
        }

        /// <summary>
        /// The OnSelectedTabChanged.
        /// </summary>
        /// <param name="name">The name<see cref="string"/>.</param>
        private void OnSelectedTabChanged(string name)
        {
            this.selectedTab = name;
        }

        #endregion
    }

    #endregion
}
