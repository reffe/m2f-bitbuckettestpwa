using Blazorise;

using M2FCommunicationContracts;
using M2FDto.Constant;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Pages
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="IntegrationTests" />
    /// </summary>
    public partial class IntegrationTests
    {
        #region Fields

        /// <summary>
        /// Defines the errorModal.
        /// </summary>
        private Modal errorModal;

        /// <summary>
        /// Defines the infoModal.
        /// </summary>
        private Modal infoModal;

        /// <summary>
        /// Defines the modalMessage.
        /// </summary>
        private string modalMessage;

        /// <summary>
        /// Defines the response.
        /// </summary>
        private M2FResponse response = new M2FResponse();

        /// <summary>
        /// Defines the testAifServiceResult.
        /// </summary>
        private Modal testAifServiceResult;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationTests"/> class.
        /// </summary>
        public IntegrationTests()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The HideErrorModal.
        /// </summary>
        private void HideErrorModal()
        {
            this.errorModal.Hide();
        }

        /// <summary>
        /// The HideInfoModal.
        /// </summary>
        private void HideInfoModal()
        {
            this.infoModal.Hide();
        }

        /// <summary>
        /// Method that allows the modal testAifServiceResult to be hidden.
        /// </summary>
        private void HideTestAifServiceResult()
        {
            this.testAifServiceResult.Hide();
        }

        /// <summary>
        /// The ShowErrorModal.
        /// </summary>
        private void ShowErrorModal()
        {
            this.errorModal.Show();
        }

        /// <summary>
        /// The ShowInfoModal.
        /// </summary>
        private void ShowInfoModal()
        {
            this.infoModal.Show();
        }

        /// <summary>
        /// Test aif service.
        /// </summary>
        /// <returns>Return a Task.</returns>
        private async Task TestAifService()
        {
            try
            {
                //// this.response = await this.httpClient.GetFromJsonAsync<M2FResponse>(ApiConstantDto.TestAifApiController);

                this.response = await this.apiManager.CallApi<M2FResponse>(this.httpClient, ApiConstantDto.TestAifApiController, HttpMethod.Get, string.Empty);

                this.testAifServiceResult.Show();
            }
            catch (System.Exception ex)
            {
                this.modalMessage = $"Il metodo per testare gli AIF sulle WebApi ha restituito l'eccezione: {ex.Message}";
                await this.logger.Error(this.modalMessage);
                this.ShowErrorModal();
            }
        }

        /// <summary>
        /// The TestWebApiMethodKo.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task TestMethodException()
        {
            try
            {
                string uri = ApiConstantDto.TestMethodExceptionApiController;
                ////await this.httpClient.GetFromJsonAsync<object>(uri);

                _ = await this.apiManager.CallApi<object>(this.httpClient, uri, HttpMethod.Get, string.Empty);
            }
            catch (System.Exception ex)
            {
                this.modalMessage = $"Il metodo per testare le eccezioni sulle WebApi ha restituito l'eccezione: {ex.Message}";
                await this.logger.Error(this.modalMessage);
                this.ShowErrorModal();
            }
        }

        /// <summary>
        /// The TestWebApiMethodOk.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task TestMethodOk()
        {
            try
            {
                string uri = ApiConstantDto.TestMethodOkApiController;
                ////await this.httpClient.GetFromJsonAsync<object>(uri);

                _ = await this.apiManager.CallApi<object>(this.httpClient, uri, HttpMethod.Get, string.Empty);

                this.modalMessage = $"Il metodo per testare le chiamate sulle WebApi � andato a buon fine.";
                this.ShowInfoModal();
            }
            catch (System.Exception ex)
            {
                this.modalMessage = $"Il metodo per testare le chiamate sulle WebApi ha restituito l'eccezione: {ex.Message}";
                await this.logger.Error(this.modalMessage);
                this.ShowErrorModal();
            }
        }

        /// <summary>
        /// The TestMethodServerLog.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task TestMethodServerLog()
        {
            try
            {
                await this.logger.TestServerLog("Questo � un test log sul Server!");

                this.modalMessage = $"Il metodo per testare i log dal client � andato a buon fine.";
                this.ShowInfoModal();
            }
            catch (System.Exception ex)
            {
                this.modalMessage = $"Il metodo per testare il log sul server ha restituito l'eccezione: {ex.Message}";
                await this.logger.Error(this.modalMessage);
                this.ShowErrorModal();
            }
        }

        /// <summary>
        /// The TestMethodLocalMemoryLog.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task TestMethodLocalMemoryLog()
        {
            try
            {
                await this.logger.TestLocalLog("Questo � un test log su Local Memory!");

                this.modalMessage = $"Il metodo per testare i log su Local Memory � andato a buon fine.";
                this.ShowInfoModal();
            }
            catch (System.Exception ex)
            {
                this.modalMessage = $"Il metodo per testare il log su local memory ha restituito l'eccezione: {ex.Message}";
                await this.logger.Error(this.modalMessage);
                this.ShowErrorModal();
            }
        }

        /// <summary>
        /// The TestMethodSendMail.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task TestMethodSendMail()
        {
            try
            {
                Dictionary<string, string> attach = new Dictionary<string, string>();

                var options = new JsonSerializerOptions(JsonSerializerDefaults.Web) { WriteIndented = true };

                attach.Add(nameof(this.aboutInfoClient), System.Text.Json.JsonSerializer.Serialize(this.aboutInfoClient, options));

                await this.mailManager.SendMail("Test Mail WASM", "Questa e una mail di test mandata da un client WASM!!!", attach);

                this.modalMessage = $"Il metodo per testare l'invio delle mail dal client � andato a buon fine.";
                this.ShowInfoModal();
            }
            catch (System.Exception ex)
            {
                this.modalMessage = $"Il metodo per testare la spedizione delle mail ha restituito l'eccezione: {ex.Message}";
                await this.logger.Error(this.modalMessage);
                this.ShowErrorModal();
            }
        }

        #endregion
    }

    #endregion
}
