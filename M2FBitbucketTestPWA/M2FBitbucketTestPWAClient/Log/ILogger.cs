﻿using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Log
{
    #region Interfaces

    /// <summary>
    /// Defines the <see cref="ILogger" />.
    /// </summary>
    public interface ILogger
    {
        #region Methods

        /// <summary>
        /// The Debug.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task Debug(string log);

        /// <summary>
        /// The Error.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task Error(string log);

        /// <summary>
        /// The Fatal.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task Fatal(string log);

        /// <summary>
        /// The Information.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task Information(string log);

        /// <summary>
        /// The Error.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task Warning(string log);

        /// <summary>
        /// The CleanOlderLog.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task CleanOlderLog();

        /// <summary>
        /// The CleanOlderLog.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task TestLocalLog(string log);

        /// <summary>
        /// The TestServerLog.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task TestServerLog(string log);

        #endregion
    }

    #endregion
}
