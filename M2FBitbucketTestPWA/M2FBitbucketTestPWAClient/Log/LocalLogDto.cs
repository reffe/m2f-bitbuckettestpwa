﻿using System;

namespace M2FBitbucketTestPWAClient.Log
{
    /// <summary>
    /// Defines the <see cref="LocalLogDto" />.
    /// </summary>
    public class LocalLogDto
    {
        #region Enums

        /// <summary>
        /// Defines the LevelEnum.
        /// </summary>
        public enum LevelEnum
        {
            /// <summary>
            /// Defines the DBG.
            /// </summary>
            DBG = 0,

            /// <summary>
            /// Defines the INF.
            /// </summary>
            INF,

            /// <summary>
            /// Defines the WRN.
            /// </summary>
            WRN,

            /// <summary>
            /// Defines the ERR.
            /// </summary>
            ERR,

            /// <summary>
            /// Defines the FTL.
            /// </summary>
            FTL,
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the Key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the Level.
        /// </summary>
        public LevelEnum Level { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        public string Log { get; set; }

        #endregion
    }
}
