﻿using Blazored.LocalStorage;
using M2FDto.Constant;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Log
{
    /// <summary>
    /// Defines the <see cref="Logger" />.
    /// </summary>
    public class Logger : ILogger
    {
        #region Fields

        /// <summary>
        /// Defines the httpClientFactory.
        /// </summary>
        private readonly IHttpClientFactory httpClientFactory;

        /// <summary>
        /// Defines the localMemoryLog.
        /// </summary>
        private readonly LocalMemoryLog localMemoryLog;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        /// <param name="httpClientFactory">The httpClient<see cref="IHttpClientFactory"/>.</param>
        /// <param name="localMemoryLog">The localStorage<see cref="ILocalStorageService"/>.</param>
        public Logger(IHttpClientFactory httpClientFactory, LocalMemoryLog localMemoryLog)
        {
            this.httpClientFactory = httpClientFactory;
            this.localMemoryLog = localMemoryLog;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The CleanOlderLog.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task CleanOlderLog()
        {
            await this.localMemoryLog.CleanOlderLog();
        }

        /// <summary>
        /// The Debug.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Debug(string log)
        {
            var bodyJson = System.Text.Json.JsonSerializer.Serialize(log);
            StringContent queryString = new StringContent(bodyJson, Encoding.UTF8, "application/json");
            var httpClient = this.httpClientFactory.CreateClient(HttpConstantDto.ChannelNameAnonymus);
            var response = await httpClient.PostAsync("api/Logger/Debug", queryString);

            if (!response.IsSuccessStatusCode)
            {
                await this.localMemoryLog.Debug(log);
            }
        }

        /// <summary>
        /// The Error.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Error(string log)
        {
            var bodyJson = System.Text.Json.JsonSerializer.Serialize(log);
            StringContent queryString = new StringContent(bodyJson, Encoding.UTF8, "application/json");
            var httpClient = this.httpClientFactory.CreateClient(HttpConstantDto.ChannelNameAnonymus);
            var response = await httpClient.PostAsync("api/Logger/Error", queryString);

            if (!response.IsSuccessStatusCode)
            {
                await this.localMemoryLog.Error(log);
            }
        }

        /// <summary>
        /// The Fatal.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Fatal(string log)
        {
            var bodyJson = System.Text.Json.JsonSerializer.Serialize(log);
            StringContent queryString = new StringContent(bodyJson, Encoding.UTF8, "application/json");
            var httpClient = this.httpClientFactory.CreateClient(HttpConstantDto.ChannelNameAnonymus);
            var response = await httpClient.PostAsync("api/Logger/Fatal", queryString);

            if (!response.IsSuccessStatusCode)
            {
                await this.localMemoryLog.Fatal(log);
            }
        }

        /// <summary>
        /// The Information.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Information(string log)
        {
            var bodyJson = System.Text.Json.JsonSerializer.Serialize(log);
            StringContent queryString = new StringContent(bodyJson, Encoding.UTF8, "application/json");
            var httpClient = this.httpClientFactory.CreateClient(HttpConstantDto.ChannelNameAnonymus);
            var response = await httpClient.PostAsync("api/Logger/Information", queryString);

            if (!response.IsSuccessStatusCode)
            {
                await this.localMemoryLog.Information(log);
            }
        }

        /// <summary>
        /// The CleanOlderLog.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task TestLocalLog(string log)
        {
            await this.localMemoryLog.Debug(log);
            await this.localMemoryLog.Information(log);
            await this.localMemoryLog.Warning(log);
            await this.localMemoryLog.Error(log);
            await this.localMemoryLog.Fatal(log);
        }

        /// <summary>
        /// The TestServerLog.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task TestServerLog(string log)
        {
            await this.Debug(log);
            await this.Information(log);
            await this.Warning(log);
            await this.Error(log);
            await this.Fatal(log);
        }

        /// <summary>
        /// The Error.
        /// </summary>
        /// <param name="log">The log<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Warning(string log)
        {
            var bodyJson = System.Text.Json.JsonSerializer.Serialize(log);
            StringContent queryString = new StringContent(bodyJson, Encoding.UTF8, "application/json");
            var httpClient = this.httpClientFactory.CreateClient(HttpConstantDto.ChannelNameAnonymus);
            var response = await httpClient.PostAsync("api/Logger/Warning", queryString);

            if (!response.IsSuccessStatusCode)
            {
                await this.localMemoryLog.Warning(log);
            }
        }

        #endregion
    }
}
