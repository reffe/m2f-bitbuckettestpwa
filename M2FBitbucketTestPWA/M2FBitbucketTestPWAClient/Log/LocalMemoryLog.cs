﻿using Blazored.LocalStorage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Log
{
    /// <summary>
    /// Defines the <see cref="LocalMemoryLog" />.
    /// </summary>
    public class LocalMemoryLog
    {
        #region Constants

        /// <summary>
        /// Defines the DayStore.
        /// </summary>
        private const int DayStore = 3;

        /// <summary>
        /// Defines the LogTag.
        /// </summary>
        private const string LogTag = "LOG";

        #endregion

        #region Fields

        /// <summary>
        /// Defines the localStorage.
        /// </summary>
        private readonly ILocalStorageService localStorage;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalMemoryLog"/> class.
        /// </summary>
        /// <param name="localStorage">The localStorage<see cref="ILocalStorageService"/>.</param>
        public LocalMemoryLog(ILocalStorageService localStorage)
        {
            this.localStorage = localStorage;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The RemoveName.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task CleanOlderLog()
        {
            List<string> keyToDelete = new List<string>();
            try
            {
                // Gurdo quanti elementi ci sono
                int length = await this.GetLocalStorageLength();

                // Ciclo su tuttti gli elementi
                for (int index = 0; index < length; ++index)
                {
                    string key = await this.localStorage.KeyAsync(index);

                    // Se la key inzioa con Log è di tipo log
                    if (key.StartsWith(LogTag))
                    {
                        LocalLogDto log = await this.GetLogFromKey(key);
                        var datediff = (DateTime.Now - log.Date).TotalDays;

                        if (log != null &&
                            (DateTime.Now - log.Date).TotalDays > DayStore)
                        {
                            // Li aggiungo alla lista che cancello in seguito (per non spostare gli indici)
                            keyToDelete.Add(key);
                        }
                    }
                }

                // Cancello tutti i log rilevati prima
                foreach (var key in keyToDelete)
                {
                    await this.localStorage.RemoveItemAsync(key);
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Cancella tutta la local Storage.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task ClearLocalStorage()
        {
            // Console.WriteLine("Calling Clear...");
            await this.localStorage.ClearAsync();
        }

        /// <summary>
        /// The Debug.
        /// </summary>
        /// <param name="logInput">The logInput<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Debug(string logInput)
        {
            string log = DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture) + $" [{LocalLogDto.LevelEnum.DBG.ToString()}] " + logInput;
            await this.localStorage.SetItemAsync(this.GenerateNewLogKey(), log);
        }

        /// <summary>
        /// The Error.
        /// </summary>
        /// <param name="logInput">The logInput<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Error(string logInput)
        {
            string log = DateTime.Now.ToString(/*"yyyy-MM-dd HH:mm:ss:fff",*/ System.Globalization.CultureInfo.InvariantCulture) + $" [{LocalLogDto.LevelEnum.ERR.ToString()}] " + logInput;
            await this.localStorage.SetItemAsync(this.GenerateNewLogKey(), log);
        }

        /// <summary>
        /// The Fatal.
        /// </summary>
        /// <param name="logInput">The logInput<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Fatal(string logInput)
        {
            string log = DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture) + $" [{LocalLogDto.LevelEnum.FTL.ToString()}] " + logInput;
            await this.localStorage.SetItemAsync(this.GenerateNewLogKey(), log);
        }

        /// <summary>
        /// Dice quanti elementi ci sono nella local storage.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task<int> GetLocalStorageLength()
        {
            return await this.localStorage.LengthAsync();
        }

        /// <summary>
        /// The GetLogsList.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task<IEnumerable<LocalLogDto>> GetLogsList()
        {
            List<LocalLogDto> logList = new List<LocalLogDto>();

            try
            {
                // Gurdo quanti elementi ci sono
                int length = await this.GetLocalStorageLength();

                // Ciclo su tuttti gli elementi
                for (int index = 0; index < length; ++index)
                {
                    string key = await this.localStorage.KeyAsync(index);

                    // Se la key inzioa con Log è di tipo log
                    if (key.StartsWith(LogTag))
                    {
                        LocalLogDto log = await this.GetLogFromKey(key);

                        if (log != null)
                        {
                            logList.Add(log);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            IEnumerable<LocalLogDto> ret = logList.OrderBy(log => log.Date);

            return ret;
        }

        /// <summary>
        /// The GetPresenceLogOnLocalStorage.
        /// </summary>
        /// <returns>The <see cref="Task{bool}"/>.</returns>
        public async Task<bool> GetPresenceLogOnLocalStorage()
        {
            try
            {
                // Gurdo quanti elementi ci sono
                int length = await this.GetLocalStorageLength();

                // Ciclo su tuttti gli elementi
                for (int index = 0; index < length; ++index)
                {
                    string key = await this.localStorage.KeyAsync(index);

                    // Se la key inzioa con Log è di tipo log
                    if (key.StartsWith(LogTag))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return false;
        }

        /// <summary>
        /// The Information.
        /// </summary>
        /// <param name="logInput">The logInput<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Information(string logInput)
        {
            string log = DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture) + $" [{LocalLogDto.LevelEnum.INF.ToString()}] " + logInput;
            await this.localStorage.SetItemAsync(this.GenerateNewLogKey(), log);
        }

        /// <summary>
        /// The RemoveName.
        /// </summary>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task RemoveAllLogs()
        {
            List<string> keyToDelete = new List<string>();
            try
            {
                // Gurdo quanti elementi ci sono
                int length = await this.GetLocalStorageLength();

                // Ciclo su tuttti gli elementi
                for (int index = 0; index < length; ++index)
                {
                    string key = await this.localStorage.KeyAsync(index);

                    // Se la key inzioa con Log è di tipo log
                    if (key.StartsWith(LogTag))
                    {
                        // Li aggiungo alla lista che cancello in seguito (per non spostare gli indici)
                        keyToDelete.Add(key);
                    }
                }

                // Cancello tutti i log rilevati prima
                foreach (var key in keyToDelete)
                {
                    await this.localStorage.RemoveItemAsync(key);
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// The Warning.
        /// </summary>
        /// <param name="logInput">The logInput<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Warning(string logInput)
        {
            string log = DateTime.Now.ToString(/*"yyyy-MM-dd HH:mm:ss:fff",*/ System.Globalization.CultureInfo.InvariantCulture) + $" [{LocalLogDto.LevelEnum.WRN.ToString()}] " + logInput;
            await this.localStorage.SetItemAsync(this.GenerateNewLogKey(), log);
        }

        /// <summary>
        /// The GenerateNewLogKey.
        /// </summary>
        /// <returns>The <see cref="string"/>.</returns>
        private string GenerateNewLogKey()
        {
            return LogTag + "_" + System.Guid.NewGuid();
        }

        /// <summary>
        /// The GetLogFromKey.
        /// </summary>
        /// <param name="key">The key<see cref="string"/>.</param>
        /// <returns>The <see cref="LocalLogDto"/>.</returns>
        private async Task<LocalLogDto> GetLogFromKey(string key)
        {
            LocalLogDto log = null;
            try
            {
                string strLog = await this.GetLogFromLocalStorage(key);

                var divider = strLog.IndexOf("[");

                log = new LocalLogDto()
                {
                    Key = key,
                    Date = Convert.ToDateTime(strLog.Substring(0, divider), CultureInfo.InvariantCulture),
                    Level = (LocalLogDto.LevelEnum)Enum.Parse(typeof(LocalLogDto.LevelEnum), strLog.Substring(divider + 1, 3), true),
                    Log = strLog.Substring(divider + 5),
                };
            }
            catch (Exception ex)
            {
            }

            return log;
        }

        /// <summary>
        /// The GetLogFromLocalStorage.
        /// </summary>
        /// <param name="key">The key<see cref="string"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task<string> GetLogFromLocalStorage(string key)
        {
            string ret = string.Empty;

            try
            {
                ret = await this.localStorage.GetItemAsync<string>(key);
            }
            catch (Exception ex)
            {
            }

            return ret;
        }

        #endregion
    }
}
