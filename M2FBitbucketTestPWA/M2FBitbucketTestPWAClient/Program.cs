using Blazored.LocalStorage;
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;

using M2FDto;
using M2FDto.Constant;
using M2FBitbucketTestPWAClient.Log;
using M2FBitbucketTestPWAClient.Mail;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="Program" />
    /// </summary>
    public class Program
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Program"/> class.
        /// </summary>
        protected Program()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The Main
        /// </summary>
        /// <param name="args">The args<see cref="T:string[]"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.RootComponents.Add<App>("#app");

            ConfigureServices(builder);

            await builder.Build().RunAsync();
        }

        /// <summary>
        /// The ConfigureServices
        /// </summary>
        /// <param name="builder">The builder<see cref="WebAssemblyHostBuilder"/></param>
        private static void ConfigureServices(WebAssemblyHostBuilder builder)
        {
            Assembly executingAsm = Assembly.GetExecutingAssembly();

            string softwareVersion = ((AssemblyInformationalVersionAttribute)Attribute.GetCustomAttribute(executingAsm, typeof(AssemblyInformationalVersionAttribute), false))?.InformationalVersion.ToString();

            builder.Services.AddHttpClient(HttpConstantDto.ChannelNameAnonymus, client =>
            {
                client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);
                client.DefaultRequestHeaders.Add(HttpConstantDto.DefaultHeaderValue, softwareVersion);
            });

            builder.Services
                .AddHttpClient(HttpConstantDto.ChannelNameAuthenticated, client =>
                {
                    client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);
                    client.DefaultRequestHeaders.Add(HttpConstantDto.DefaultHeaderValue, softwareVersion);
                })
                .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

            builder.Services
                .AddScoped(sp =>
                {
                    return
                    sp
                    .GetRequiredService<IHttpClientFactory>()
                    .CreateClient(HttpConstantDto.ChannelNameAuthenticated);
                });

            builder.Services.AddBlazoredLocalStorage();

            builder.Services
                .AddSingleton<AboutDto>(x => new AboutDto(Assembly.GetExecutingAssembly()));

            builder.Services
                .AddSingleton<M2FAboutDtoServer>();

            builder.Services
                 .AddScoped<LocalMemoryLog>();

            builder.Services
                 .AddScoped<ILogger, Logger>();

            builder.Services
                .AddScoped<ApiManager>();

            builder.Services
                 .AddScoped<IClientMailManager, ClientMailManager>();

            builder.Services
              .AddBlazorise(options =>
              {
                  options.ChangeTextOnKeyPress = true;
              })
              .AddBootstrapProviders()
              .AddFontAwesomeIcons();

            builder.Services
                .AddApiAuthorization();

            builder.Services
                .AddApiAuthorization()
                .AddAccountClaimsPrincipalFactory<CustomUserFactory>();
        }

        #endregion
    }

    #endregion
}
