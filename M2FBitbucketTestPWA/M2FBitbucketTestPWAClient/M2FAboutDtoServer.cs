﻿using M2FDto;

namespace M2FBitbucketTestPWAClient
{
    /// <summary>
    /// Defines the <see cref="M2FAboutDtoServer" />.
    /// </summary>
    public class M2FAboutDtoServer : AboutDto
    {
    }
}
