using M2FDto;
using M2FDto.About;
using M2FDto.Constant;
using M2FBitbucketTestPWAClient.Log;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="CustomUserFactory" />.
    /// </summary>
    public class CustomUserFactory : AccountClaimsPrincipalFactory<RemoteUserAccount>
    {
        #region Fields

        /// <summary>
        /// Defines the aboutDto.
        /// </summary>
        private readonly AboutDto aboutDto;

        /// <summary>
        /// Defines the aboutDto.
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomUserFactory"/> class.
        /// </summary>
        /// <param name="accessor">The accessor<see cref="IAccessTokenProviderAccessor"/>.</param>
        /// <param name="aboutDto">The about<see cref="AboutDto"/>.</param>
        /// 3/// <param name="logger">The logger<see cref="ILogger"/>.</param>
        public CustomUserFactory(IAccessTokenProviderAccessor accessor, AboutDto aboutDto, ILogger logger)
            : base(accessor)
        {
            this.aboutDto = aboutDto;
            this.logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The CreateUserAsync.
        /// </summary>
        /// <param name="account">The account<see cref="RemoteUserAccount"/>.</param>
        /// <param name="options">The options<see cref="RemoteAuthenticationUserOptions"/>.</param>
        /// <returns>The <see cref="ValueTask{ClaimsPrincipal}"/>.</returns>
        public async override ValueTask<ClaimsPrincipal> CreateUserAsync(RemoteUserAccount account, RemoteAuthenticationUserOptions options)
        {
            var user = await base.CreateUserAsync(account, options);

            var claimsIdentity = (ClaimsIdentity)user.Identity;

            if (account != null)
            {
                this.MapArrayClaimsToMultipleSeparateClaims(account, claimsIdentity);
            }

            // Pulisco qui i log locali perch� so ceh ci passo ogni volta che mi loggo
            this.logger.CleanOlderLog();

            return user;
        }

        /// <summary>
        /// The MapArrayClaimsToMultipleSeparateClaims.
        /// </summary>
        /// <param name="account">The account<see cref="RemoteUserAccount"/>.</param>
        /// <param name="claimsIdentity">The claimsIdentity<see cref="ClaimsIdentity"/>.</param>
        private void MapArrayClaimsToMultipleSeparateClaims(RemoteUserAccount account, ClaimsIdentity claimsIdentity)
        {
            this.aboutDto.LoggedUserInformation.MemberOf.Clear();
            this.aboutDto.LoggedUserInformation.Roles.Clear();

            List<RoleInfoDto> identityServerRoles = new List<RoleInfoDto>();

            foreach (var prop in account.AdditionalProperties)
            {
                var value = prop.Value;

                if (value is JsonElement element && element.ValueKind == JsonValueKind.Array)
                {
                    claimsIdentity.RemoveClaim(claimsIdentity.FindFirst(prop.Key));

                    var claims = element.EnumerateArray().Select(x => new Claim(prop.Key, x.ToString()));

                    claimsIdentity.AddClaims(claims);
                }
            }

            foreach (var item in claimsIdentity.Claims)
            {
                switch (item.Type.ToLower())
                {
                    case IdentityServerConstantDto.ClaimsRole:
                        {
                            var memberOf = new MemberOfInfoDto();
                            memberOf.GroupName = item.Value;
                            memberOf.DistinguishedName = item.Value;

                            if (this.aboutDto.LoggedUserInformation.MemberOf.FirstOrDefault(x => string.Compare(x.DistinguishedName, memberOf.DistinguishedName, true) == 0) == null)
                            {
                                this.aboutDto.LoggedUserInformation.MemberOf.Add(memberOf);
                            }
                        }

                        break;

                    case IdentityServerConstantDto.ClaimsIdentityServerRole:
                        {
                            string[] roles = item.Value.Split(",");
                            if (roles.Length > 0)
                            {
                                foreach (var r in roles)
                                {
                                    // mi ricavo l oggetto roleInfo da aggiungere ai ruoli
                                    var roleInfo = new RoleInfoDto();
                                    roleInfo.RoleName = r;

                                    if (this.aboutDto.LoggedUserInformation.Roles.FirstOrDefault(x => string.Compare(x.RoleName, roleInfo.RoleName, true) == 0) == null)
                                    {
                                        this.aboutDto.LoggedUserInformation.Roles.Add(roleInfo);
                                        identityServerRoles.Add(roleInfo);
                                    }
                                }
                            }
                        }

                        break;

                    case IdentityServerConstantDto.ClaimsEmplId:

                        this.aboutDto.LoggedUserInformation.EmplId = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsDallasKey:

                        this.aboutDto.LoggedUserInformation.DallasKey = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsName:

                        this.aboutDto.LoggedUserInformation.LoggedUser = item.Value;

                        break;
                }
            }

            // scorro tutti le identity server roles per toglierli dai member of
            foreach (var role in identityServerRoles)
            {
                MemberOfInfoDto memberOf = null;

                // controllo se nei member of c � gia un ruolo di quel tipo
                memberOf = this.aboutDto.LoggedUserInformation.MemberOf.FirstOrDefault(x => string.Compare(x.DistinguishedName, role.RoleName, true) == 0);
                if (memberOf != null)
                {
                    // se l ho trovato lo elimino
                    this.aboutDto.LoggedUserInformation.MemberOf.Remove(memberOf);
                }
            }

            // Riempo le informazioni utente nell about DTO
            this.FillLoggedUserInformationFromClaims(claimsIdentity);

            // Riempio le connection Info
            this.FillConnectionInfoInformationFromClaims(claimsIdentity);

            // Riempio le enviroment Info
            this.FillEnvironmentInfoInformationFromClaims(claimsIdentity);
        }

        /// <summary>
        /// The FillLoggedUserInformationFromClaims.
        /// </summary>
        /// <param name="claimsIdentity">The claimsIdentity<see cref="ClaimsIdentity"/>.</param>
        private void FillLoggedUserInformationFromClaims(ClaimsIdentity claimsIdentity)
        {
            foreach (var item in claimsIdentity.Claims)
            {
                switch (item.Type)
                {
                    case IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutDto.LoggedUserInformation.DistinguishedName):

                        this.aboutDto.LoggedUserInformation.DistinguishedName = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutDto.LoggedUserInformation.GivenName):

                        this.aboutDto.LoggedUserInformation.GivenName = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutDto.LoggedUserInformation.Mail):

                        this.aboutDto.LoggedUserInformation.Mail = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutDto.LoggedUserInformation.Name):

                        this.aboutDto.LoggedUserInformation.Name = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutDto.LoggedUserInformation.SAMAccountName):

                        this.aboutDto.LoggedUserInformation.SAMAccountName = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutDto.LoggedUserInformation.Sn):

                        this.aboutDto.LoggedUserInformation.Sn = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutDto.LoggedUserInformation.UserPrincipalName):

                        this.aboutDto.LoggedUserInformation.UserPrincipalName = item.Value;

                        break;
                }
            }
        }

        /// <summary>
        /// The FillConnectionInfoInformationFromClaims.
        /// </summary>
        /// <param name="claimsIdentity">The claimsIdentity<see cref="ClaimsIdentity"/>.</param>
        private void FillConnectionInfoInformationFromClaims(ClaimsIdentity claimsIdentity)
        {
            foreach (var item in claimsIdentity.Claims)
            {
                switch (item.Type)
                {
                    case IdentityServerConstantDto.ClaimsPrefixConnectionInfo + nameof(this.aboutDto.HttpContextConnectionInformation.LocalIpAddress):

                        this.aboutDto.HttpContextConnectionInformation.LocalIpAddress = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsPrefixConnectionInfo + nameof(this.aboutDto.HttpContextConnectionInformation.LocalPort):

                        this.aboutDto.HttpContextConnectionInformation.LocalPort = Convert.ToInt32(item.Value);

                        break;
                    case IdentityServerConstantDto.ClaimsPrefixConnectionInfo + nameof(this.aboutDto.HttpContextConnectionInformation.RemoteIpAddress):

                        this.aboutDto.HttpContextConnectionInformation.RemoteIpAddress = item.Value;

                        break;

                    case IdentityServerConstantDto.ClaimsPrefixConnectionInfo + nameof(this.aboutDto.HttpContextConnectionInformation.RemotePort):

                        this.aboutDto.HttpContextConnectionInformation.RemotePort = Convert.ToInt32(item.Value);

                        break;
                }
            }
        }

        /// <summary>
        /// The FillEnvironmentInfoInformationFromClaims.
        /// </summary>
        /// <param name="claimsIdentity">The claimsIdentity<see cref="ClaimsIdentity"/>.</param>
        private void FillEnvironmentInfoInformationFromClaims(ClaimsIdentity claimsIdentity)
        {
            foreach (var item in claimsIdentity.Claims)
            {
                switch (item.Type)
                {
                    case IdentityServerConstantDto.ClaimsPrefixEnvInfo + nameof(this.aboutDto.EnvironmentInformation.MachineName):

                        this.aboutDto.EnvironmentInformation.MachineName = item.Value;

                        break;
                }
            }
        }

        #endregion
    }

    #endregion
}
