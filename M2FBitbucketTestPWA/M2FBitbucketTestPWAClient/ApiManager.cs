﻿using M2FBitbucketTestPWAClient.Log;
using Microsoft.AspNetCore.Components;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient
{
    /// <summary>
    /// Defines the <see cref="ApiManager" />.
    /// </summary>
    public class ApiManager
    {
        #region Fields

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Defines the navigation.
        /// </summary>
        private readonly NavigationManager navigation;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiManager"/> class.
        /// </summary>
        /// <param name="navigation">The navigation<see cref="NavigationManager"/>.</param>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        public ApiManager(NavigationManager navigation, ILogger logger)
        {
            this.navigation = navigation;
            this.logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The ManageHttpResponse.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="httpClient">The httpClient<see cref="HttpClient"/>.</param>
        /// <param name="uri">The uri<see cref="string"/>.</param>
        /// <param name="method">The method<see cref="HttpMethod"/>.</param>
        /// <param name="body">The body<see cref="string"/>.</param>
        /// <returns>The <see cref="T"/>.</returns>
        public async Task<T> CallApi<T>(HttpClient httpClient, string uri, HttpMethod method, string body)
        {
            HttpResponseMessage httpResponse = null;

            T retValue = default(T);

            try
            {
                if (method == HttpMethod.Get)
                {
                    httpResponse = await httpClient.GetAsync(uri);
                    if (this.ManageHttpResponse(httpResponse))
                    {
                        retValue = await httpResponse.Content.ReadFromJsonAsync<T>();
                    }
                }
                else if (method == HttpMethod.Post)
                {
                    StringContent queryString = new StringContent(body, Encoding.UTF8, "application/json");
                    httpResponse = await httpClient.PostAsync(uri, queryString);
                    if (this.ManageHttpResponse(httpResponse))
                    {
                        retValue = await httpResponse.Content.ReadFromJsonAsync<T>();
                    }
                }
                else if (method == HttpMethod.Delete)
                {
                    httpResponse = await httpClient.DeleteAsync(uri);
                    if (this.ManageHttpResponse(httpResponse))
                    {
                    }
                }
                else if (method == HttpMethod.Put)
                {
                    StringContent queryString = new StringContent(body, Encoding.UTF8, "application/json");
                    httpResponse = await httpClient.PutAsync(uri, queryString);
                    if (this.ManageHttpResponse(httpResponse))
                    {
                    }
                }
            }
            catch (Exception exc)
            {
                if (httpResponse != null)
                {
                    if (!this.ManageHttpExceptionResponse(httpResponse.StatusCode))
                    {
                        return retValue;
                    }
                }

                throw;
            }

            return retValue;
        }

        /// <summary>
        /// The ManageHttpResponse.
        /// </summary>
        /// <param name="httpResponse">The httpStatusCode<see cref="HttpResponseMessage"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        private bool ManageHttpResponse(HttpResponseMessage httpResponse)
        {
            if (httpResponse.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                throw new Exception(httpResponse.ReasonPhrase);
            }
        }

        /// <summary>
        /// The ManageHttpResponse.
        /// </summary>
        /// <param name="httpStatusCode">The httpStatusCode<see cref="HttpStatusCode"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        private bool ManageHttpExceptionResponse(HttpStatusCode httpStatusCode)
        {
            if (httpStatusCode == HttpStatusCode.Forbidden)
            {
                this.navigation.NavigateTo("403Forbidden");
                return false;
            }

            return true;
        }

        #endregion
    }
}
