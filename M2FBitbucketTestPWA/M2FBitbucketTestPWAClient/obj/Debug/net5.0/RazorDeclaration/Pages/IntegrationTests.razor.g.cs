// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace M2FBitbucketTestPWAClient.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using M2FBitbucketTestPWAClient;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using M2FBitbucketTestPWAClient.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using M2FBitbucketTestPWAClient.Log;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using M2FBitbucketTestPWAClient.Mail;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Blazorise;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Blazorise.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Blazorise.DataGrid;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using M2FCommunicationContracts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\_Imports.razor"
using Blazored.LocalStorage;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\src\m2f-bitbuckettestpwa\M2FBitbucketTestPWA\M2FBitbucketTestPWAClient\Pages\IntegrationTests.razor"
using M2FDto;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/integrationTests")]
    public partial class IntegrationTests : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private AboutDto aboutInfoClient { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IClientMailManager mailManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ILogger logger { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ApiManager apiManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient httpClient { get; set; }
    }
}
#pragma warning restore 1591
