﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Mail
{
    #region Interfaces

    /// <summary>
    /// Defines the <see cref="IClientMailManager" />.
    /// </summary>
    public interface IClientMailManager
    {
        #region Methods

        /// <summary>
        /// The SendMail.
        /// </summary>
        /// <param name="subject">The subject<see cref="string"/>.</param>
        /// <param name="body">The body<see cref="string"/>.</param>
        /// <param name="attachments">The attachments<see cref="List{string}"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task SendMail(string subject, string body, Dictionary<string, string> attachments);

        /// <summary>
        /// The TrySendMail.
        /// </summary>
        /// <param name="subject">The subject<see cref="string"/>.</param>
        /// <param name="body">The body<see cref="string"/>.</param>
        /// <param name="attachments">The attachments<see cref="List{string}"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task TrySendMail(string subject, string body, Dictionary<string, string> attachments);

        #endregion
    }

    #endregion
}
