﻿using M2FDto;
using M2FDto.Constant;
using M2FBitbucketTestPWAClient.Log;
using M2FBitbucketTestPWADto.Mail;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAClient.Mail
{
    /// <summary>
    /// Defines the <see cref="ClientMailManager" />.
    /// </summary>
    public class ClientMailManager : IClientMailManager
    {
        #region Constants

        /// <summary>
        /// Defines the Uri.
        /// </summary>
        private const string Uri = "api/mail/SendMail";

        #endregion

        #region Fields

        /// <summary>
        /// Defines the aboutInfo.
        /// </summary>
        private readonly AboutDto aboutInfo;

        /// <summary>
        /// Defines the apiManager.
        /// </summary>
        private readonly ApiManager apiManager;

        /// <summary>
        /// Defines the httpClientFactory.
        /// </summary>
        private readonly IHttpClientFactory httpClientFactory;

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientMailManager"/> class.
        /// </summary>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/>.</param>
        /// <param name="apiManager">The apiManager<see cref="ApiManager"/>.</param>
        /// <param name="httpClientFactory">The httpClientFactory<see cref="IHttpClientFactory"/>.</param>
        public ClientMailManager(ILogger logger, AboutDto aboutInfo, ApiManager apiManager, IHttpClientFactory httpClientFactory)
        {
            this.apiManager = apiManager;
            this.aboutInfo = aboutInfo;
            this.logger = logger;
            this.httpClientFactory = httpClientFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The SendMail.
        /// </summary>
        /// <param name="subject">The subject<see cref="string"/>.</param>
        /// <param name="body">The body<see cref="string"/>.</param>
        /// <param name="attachments">The attachments<see cref="List{string}"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task SendMail(string subject, string body, Dictionary<string, string> attachments)
        {
            string solutionName = this.aboutInfo.AssemblyInformation.Product;
            MailDto mail = new MailDto()
            {
                FullName = $"NoReply - {solutionName}",
                MailAddress = $"noReply.{solutionName}@merlo.com",
                MailSubject = subject,
                EventName = "CLIENT_MAIL",
                MailBody = body,
                Attachments = attachments,
            };

            var httpClientAnonymous = this.httpClientFactory.CreateClient(HttpConstantDto.ChannelNameAnonymus);

            var serializedMail = System.Text.Json.JsonSerializer.Serialize(mail);

            _ = await this.apiManager.CallApi<object>(httpClientAnonymous, Uri, HttpMethod.Post, serializedMail);
        }

        /// <summary>
        /// The SendMail.
        /// </summary>
        /// <param name="subject">The subject<see cref="string"/>.</param>
        /// <param name="body">The body<see cref="string"/>.</param>
        /// <param name="attachments">The attachments<see cref="List{string}"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task TrySendMail(string subject, string body, Dictionary<string, string> attachments)
        {
            try
            {
                await this.SendMail(subject, body, attachments);
            }
            catch (System.Exception ex)
            {
                await this.logger.Error($"Error Sending Mail From Client: {ex.Message}");
            }
        }

        #endregion
    }
}
