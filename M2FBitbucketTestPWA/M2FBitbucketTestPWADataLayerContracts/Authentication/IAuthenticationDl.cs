using System.Collections.Generic;

using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Custom;
using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard;

using Microsoft.AspNetCore.Identity;

namespace M2FBitbucketTestPWADataLayerContracts.Authentication
{
    #region Interfaces

    /// <summary>
    /// Defines the <see cref="IAuthenticationDl" />
    /// </summary>
    public interface IAuthenticationDl
    {
        #region Methods

        /// <summary>
        /// The GetRolesByUserId
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="IEnumerable{Role}"/></returns>
        IEnumerable<IdentityRole> GetRolesByUserId(string id);

        /// <summary>
        /// The GetUserById
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="ApplicationUser"/></returns>
        ApplicationUser GetUserById(string id);

        /// <summary>
        /// The GetUserByUsername
        /// </summary>
        /// <param name="username">The username<see cref="string"/></param>
        /// <returns>The <see cref="ApplicationUser"/></returns>
        ApplicationUser GetUserByUsername(string username);

        /// <summary>
        /// The ValidatePassword
        /// </summary>
        /// <param name="username">The username<see cref="string"/></param>
        /// <param name="plainTextPassword">The plainTextPassword<see cref="string"/></param>
        /// <returns>The <see cref="bool"/></returns>
        bool ValidatePassword(string username, string plainTextPassword);

        #endregion
    }

    #endregion
}
