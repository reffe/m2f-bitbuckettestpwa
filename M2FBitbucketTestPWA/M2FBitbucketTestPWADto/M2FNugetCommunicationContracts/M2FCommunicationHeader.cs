﻿using System;

namespace M2FCommunicationContracts
{
    /// <summary>
    /// DTO used as abstract class for the DTOs used in each communication that pass throught the M2F Communication Infrastructure.
    /// </summary>
    public abstract class M2FCommunicationHeader
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Action
        /// The Web API action called by <see cref="SystemFrom"/>..
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Gets or sets the CommMode
        /// Communication modality used.
        /// 1 = Sync
        /// 2 = Async..
        /// </summary>
        public int CommMode { get; set; } = 1;

        /// <summary>
        /// Gets or sets the CommTime1
        /// Time when the communication starts...
        /// </summary>
        public DateTime CommTime1 { get; set; }

        /// <summary>
        /// Gets or sets the CommTime2
        /// Time when the communication is received by ADAPTERTO...
        /// </summary>
        public DateTime CommTime2 { get; set; }

        /// <summary>
        /// Gets or sets the Controller
        /// The Web API controller called by <see cref="SystemFrom"/>..
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsRequest
        /// If TRUE indicates that the communication have to be intended as a request...
        /// </summary>
        public bool IsRequest { get; set; }

        /// <summary>
        /// Gets or sets the MessageGuid
        /// Unique identifier for the communication between 2 endpoints...
        /// </summary>
        public string MessageGuid { get; set; }

        /// <summary>
        /// Gets or sets the SystemFrom
        /// The SENDER of the communication...
        /// </summary>
        public string SystemFrom { get; set; }

        /// <summary>
        /// Gets or sets the SystemTo
        /// THE RECIPIENT  of the communication...
        /// </summary>
        public string SystemTo { get; set; }

        #endregion
    }
}
