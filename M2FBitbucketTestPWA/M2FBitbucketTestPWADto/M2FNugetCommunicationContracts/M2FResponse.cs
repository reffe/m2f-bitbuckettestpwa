﻿using System;
using System.Xml.Serialization;

namespace M2FCommunicationContracts
{
    /// <summary>
    /// DTO used in order to standardize the strucutre of the response in each communication that pass throught the M2F Communication Infrastructure.
    /// </summary>
    [XmlRoot(ElementName = "root")]
    public class M2FResponse : M2FCommunicationHeader
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Code
        /// A numeric value that is correlated to a specific event/message.
        /// For example: in case of success the value is 0, in case of error the value is a negative integer...
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Gets or sets the Description
        /// Custom message...
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Object
        /// This property is used to exchange custom data structure...
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Status
        /// If TRUE indicates that the communication is in SUCCESS or WARNING state.
        /// If FALSE indicates that the communication is in ERROR state...
        /// </summary>
        public bool Status { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The CreateErrorResponse.
        /// </summary>
        /// <param name="messageGuid">The messageGuid<see cref="string"/>.</param>
        /// <param name="commMode">The commMode<see cref="int"/>.</param>
        /// <param name="controller">The controller<see cref="string"/>.</param>
        /// <param name="action">The action<see cref="string"/>.</param>
        /// <param name="systemFrom">The systemFrom<see cref="string"/>.</param>
        /// <param name="systemTo">The systemTo<see cref="string"/>.</param>
        /// <param name="code">The code<see cref="int"/>.</param>
        /// <param name="description">The description<see cref="string"/>.</param>
        /// <param name="strObject">The strObject<see cref="string"/>.</param>
        /// <returns>The <see cref="M2FResponse"/>.</returns>
        public static M2FResponse CreateErrorResponse(
            string messageGuid,
            int commMode,
            string controller,
            string action,
            string systemFrom,
            string systemTo,
            int code,
            string description,
            string strObject)
        {
            var response = new M2FResponse();

            response.MessageGuid = messageGuid;
            response.CommMode = commMode;
            response.CommTime1 = DateTime.Now;
            response.CommTime2 = DateTime.Now;
            response.Controller = controller;
            response.Action = action;
            response.SystemFrom = systemFrom;
            response.SystemTo = systemTo;

            response.Status = false;
            response.Code = code;
            response.Description = description;
            response.Object = strObject;

            return response;
        }

        /// <summary>
        /// The CreateSuccessResponse.
        /// </summary>
        /// <param name="messageGuid">The messageGuid<see cref="string"/>.</param>
        /// <param name="commMode">The commMode<see cref="int"/>.</param>
        /// <param name="controller">The controller<see cref="string"/>.</param>
        /// <param name="action">The action<see cref="string"/>.</param>
        /// <param name="systemFrom">The systemFrom<see cref="string"/>.</param>
        /// <param name="systemTo">The systemTo<see cref="string"/>.</param>
        /// <returns>The <see cref="M2FResponse"/>.</returns>
        public static M2FResponse CreateSuccessResponse(string messageGuid, int commMode, string controller, string action, string systemFrom, string systemTo)
        {
            var response = new M2FResponse();

            response.MessageGuid = messageGuid;
            response.CommMode = commMode;
            response.CommTime1 = DateTime.Now;
            response.CommTime2 = DateTime.Now;
            response.Controller = controller;
            response.Action = action;
            response.SystemFrom = systemFrom;
            response.SystemTo = systemTo;

            response.Status = true;
            response.Code = 0;
            response.Description = string.Empty;
            response.Object = string.Empty;

            return response;
        }

        /// <summary>
        /// The CreateWarningResponse.
        /// </summary>
        /// <param name="messageGuid">The messageGuid<see cref="string"/>.</param>
        /// <param name="commMode">The commMode<see cref="int"/>.</param>
        /// <param name="controller">The controller<see cref="string"/>.</param>
        /// <param name="action">The action<see cref="string"/>.</param>
        /// <param name="systemFrom">The systemFrom<see cref="string"/>.</param>
        /// <param name="systemTo">The systemTo<see cref="string"/>.</param>
        /// <param name="code">The code<see cref="int"/>.</param>
        /// <param name="description">The description<see cref="string"/>.</param>
        /// <param name="strObject">The strObject<see cref="string"/>.</param>
        /// <returns>The <see cref="M2FResponse"/>.</returns>
        public static M2FResponse CreateWarningResponse(
            string messageGuid,
            int commMode,
            string controller,
            string action,
            string systemFrom,
            string systemTo,
            int code,
            string description,
            string strObject)
        {
            var response = new M2FResponse();

            response.MessageGuid = messageGuid;
            response.CommMode = commMode;
            response.CommTime1 = DateTime.Now;
            response.CommTime2 = DateTime.Now;
            response.Controller = controller;
            response.Action = action;
            response.SystemFrom = systemFrom;
            response.SystemTo = systemTo;

            response.Status = true;
            response.Code = code;
            response.Description = description;
            response.Object = strObject;

            return response;
        }

        #endregion
    }
}
