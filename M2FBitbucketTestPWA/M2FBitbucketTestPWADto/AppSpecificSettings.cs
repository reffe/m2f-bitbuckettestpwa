namespace M2FBitbucketTestPWADto
{
    /// <summary>
    /// Defines the <see cref="AppSpecificSettings" />.
    /// La classe è utilizzata come contenitore dei parametri specifici per l'applicazione.
    /// Tali parametri sono caricati dall'appsettings.json.
    /// </summary>
    public class AppSpecificSettings
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppSpecificSettings"/> class.
        /// </summary>
        public AppSpecificSettings()
        {
        }

        #endregion
    }
}
