﻿using System.Collections.Generic;

namespace M2FBitbucketTestPWADto.Mail
{
    /// <summary>
    /// Defines the <see cref="MailDto" />.
    /// </summary>
    public class MailDto
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MailDto"/> class.
        /// </summary>
        public MailDto()
        {
            this.Attachments = new Dictionary<string, string>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Attachments.
        /// </summary>
        public Dictionary<string, string> Attachments { get; set; }

        /// <summary>
        /// Gets or sets the EventName.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the FullName.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the MailAddress.
        /// </summary>
        public string MailAddress { get; set; }

        /// <summary>
        /// Gets or sets the MailBody.
        /// </summary>
        public string MailBody { get; set; }

        /// <summary>
        /// Gets or sets the MailSubject.
        /// </summary>
        public string MailSubject { get; set; }

        #endregion
    }
}
