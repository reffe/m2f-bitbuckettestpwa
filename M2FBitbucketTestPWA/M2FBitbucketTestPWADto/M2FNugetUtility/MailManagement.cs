﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System.Collections.Generic;
using System.IO;

namespace M2FUtility
{
    /// <summary>
    /// Defines the <see cref="MailManagement" />.
    /// </summary>
    public class MailManagement
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MailManagement"/> class.
        /// </summary>
        public MailManagement()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The SendMail.
        /// </summary>
        /// <param name="fullname">The fullname<see cref="string"/>.</param>
        /// <param name="mailAddress">The mailAddress<see cref="string"/>.</param>
        /// <param name="mailSubject">The mailSubject<see cref="string"/>.</param>
        /// <param name="mailBody">The response<see cref="string"/>.</param>
        /// <param name="attachments">The attachments<see cref="Dictionary{string, string}"/>.</param>
        /// <param name="attachmentsPath">The attachmentsPath<see cref="T: List{string}"/>.</param>
        /// <param name="receiversTo">The receiversTo<see cref="List{string}"/>.</param>
        /// <param name="receiversCc">The receiversCc<see cref="List{string}"/>.</param>
        public void SendMail(
            string fullname,
            string mailAddress,
            string mailSubject,
            string mailBody,
            Dictionary<string, string> attachments,
            List<string> attachmentsPath,
            List<string> receiversTo,
            List<string> receiversCc)
        {
            var messageToSend = new MimeMessage
            {
                Sender = new MailboxAddress(fullname, mailAddress),
                Subject = mailSubject,
            };

            foreach (string receiver in receiversTo)
            {
                messageToSend.To.Add(new MailboxAddress(receiver));
            }

            foreach (string receiverCC in receiversCc)
            {
                messageToSend.Cc.Add(new MailboxAddress(receiverCC));
            }

            messageToSend.From.Add(new MailboxAddress(fullname, mailAddress));

            // now create the multipart/mixed container to hold the message text and the
            // image attachment
            var multipart = new Multipart("mixed");

            // Allegati in memoria virtuale
            foreach (var dic in attachments)
            {
                MemoryStream memoryStream = (MemoryStream)GenerateStreamFromString(dic.Value);

                var attachment = new MimePart("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    Content = new MimeContent(memoryStream),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = $"{dic.Key}.json",
                };
                multipart.Add(attachment);
            }

            // Allegati su file su disco
            foreach (string path in attachmentsPath)
            {
                var attachment = new MimePart("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    Content = new MimeContent(File.OpenRead(path)),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = Path.GetFileName(path),
                };
                multipart.Add(attachment);
            }

            var body = new TextPart("plain")
            {
                Text = mailBody,
            };

            multipart.Add(body);

            messageToSend.Body = multipart;

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.Connect("172.24.2.130", 25, SecureSocketOptions.None);
                client.Capabilities &= ~SmtpCapabilities.Pipelining;
                client.Send(messageToSend);
                client.Disconnect(true);
            }
        }

        /// <summary>
        /// The GenerateStreamFromString.
        /// </summary>
        /// <param name="s">The s<see cref="string"/>.</param>
        /// <returns>The <see cref="Stream"/>.</returns>
        private static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        #endregion
    }
}
