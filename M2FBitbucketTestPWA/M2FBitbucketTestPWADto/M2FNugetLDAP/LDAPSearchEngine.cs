﻿using LdapForNet;
using M2FDto;
using M2FDto.About;
using System.Linq;
using System.Text.RegularExpressions;
using static LdapForNet.Native.Native;

namespace M2FLDAPSearchEngine
{
    /// <summary>
    /// Defines the <see cref="LDAPSearchEngine" />.
    /// </summary>
    public class LDAPSearchEngine
    {
        #region Constants

        /// <summary>
        /// Defines the ADPASSWORD  .
        /// </summary>
        private const string ADPASSWORD = "bu1ld_adm1n";

        /// <summary>
        /// Defines the ADPATH.
        /// </summary>
        private const string ADPATH = "LDAP://merlo.local";

        /// <summary>
        /// Defines the ADUSERNAME.
        /// </summary>
        private const string ADUSERNAME = "build_admin";

        /// <summary>
        /// Defines the ROOTDN.
        /// </summary>
        private const string ROOTDN = "dc=merlo,dc=local";

        #endregion

        #region Fields

        /// <summary>
        /// Defines the aboutInfo.
        /// </summary>
        private readonly AboutDto aboutInfo;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LDAPSearchEngine"/> class.
        /// </summary>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/>.</param>
        public LDAPSearchEngine(AboutDto aboutInfo)
        {
            this.aboutInfo = aboutInfo;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The GetAUser.
        /// </summary>
        /// <param name="userName">The userName<see cref="string"/>.</param>
        /// <returns>The <see cref="AboutDto"/>.</returns>
        public AboutDto GetUserInformationFromActiveDirectory(string userName)
        {
            using (var cn = new LdapConnection())
            {
                // Apro la connessione
                cn.Connect(ADPATH);

                // Mi autentico
                cn.Bind(LdapAuthType.Negotiate, new LdapCredential
                {
                    UserName = ADUSERNAME,
                    Password = ADPASSWORD,
                });

                // Setto il filtro dell utente
                var filter = $"(&(objectCategory=person)(objectClass=user)(sAMAccountName={userName.Split("\\").LastOrDefault()}))";

                // Ricavo tutte le entries
                var entries = cn.Search(ROOTDN, filter);

                // Controllo che ci sia una risposta
                if (entries.Count > 0)
                {
                    // ciclo tra tutte le entry
                    foreach (var entry in entries[0].DirectoryAttributes)
                    {
                        if (entry.Name == "distinguishedName")
                        {
                            this.aboutInfo.LoggedUserInformation.DistinguishedName =
                           entry.GetValues<string>()?.Count() > 0 ?
                           entry.GetValues<string>()?.FirstOrDefault<string>() :
                           string.Empty;
                        }

                        if (entry.Name == "givenName")
                        {
                            this.aboutInfo.LoggedUserInformation.GivenName =
                           entry.GetValues<string>()?.Count() > 0 ?
                           entry.GetValues<string>()?.FirstOrDefault<string>() :
                           string.Empty;
                        }

                        if (entry.Name == "mail")
                        {
                            this.aboutInfo.LoggedUserInformation.Mail =
                           entry.GetValues<string>()?.Count() > 0 ?
                           entry.GetValues<string>()?.FirstOrDefault<string>() :
                           string.Empty;
                        }

                        if (entry.Name == "name")
                        {
                            this.aboutInfo.LoggedUserInformation.Name =
                           entry.GetValues<string>()?.Count() > 0 ?
                           entry.GetValues<string>()?.FirstOrDefault<string>() :
                           string.Empty;
                        }

                        if (entry.Name == "sAMAccountName")
                        {
                            this.aboutInfo.LoggedUserInformation.SAMAccountName =
                           entry.GetValues<string>()?.Count() > 0 ?
                           entry.GetValues<string>()?.FirstOrDefault<string>() :
                           string.Empty;
                        }

                        if (entry.Name == "sn")
                        {
                            this.aboutInfo.LoggedUserInformation.Sn =
                           entry.GetValues<string>()?.Count() > 0 ?
                           entry.GetValues<string>()?.FirstOrDefault<string>() :
                           string.Empty;
                        }

                        if (entry.Name == "userPrincipalName")
                        {
                            this.aboutInfo.LoggedUserInformation.UserPrincipalName =
                           entry.GetValues<string>()?.Count() > 0 ?
                           entry.GetValues<string>()?.FirstOrDefault<string>() :
                           string.Empty;
                        }

                        if (entry.Name == "memberOf")
                        {
                            // Regex per recuperare il nome del gruppo di AD da una stringa che rispetta il pattern del distinguishedName (CN=.....)
                            // Il valore del primo CN del distinguishedName è il nome del gruppo di AD.
                            var regex = new Regex(@"(?<=^CN=)[a-zA-Z0-9_\s-]*(?=,)", RegexOptions.IgnoreCase);

                            this.aboutInfo.LoggedUserInformation.MemberOf =
                            entry.GetValues<string>()?
                            .Select(x =>
                            {
                                return
                                new MemberOfInfoDto()
                                {
                                    DistinguishedName = x,
                                    GroupName = regex.Match(x).Value,
                                };
                            })
                            .ToList();
                        }
                    }
                }
                else
                {
                    throw new System.Exception($"User '{userName.Split("\\").LastOrDefault()}' not found in Active Directory. LDAP Query: {filter}");
                }
            }

            return this.aboutInfo;
        }

        #endregion
    }
}
