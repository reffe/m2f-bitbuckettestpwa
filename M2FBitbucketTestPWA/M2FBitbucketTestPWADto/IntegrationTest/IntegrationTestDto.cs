using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace M2FBitbucketTestPWADto.IntegrationTest
{
    /// <summary>
    /// Defines the <see cref="IntegrationTestDto" />.
    /// </summary>
    [XmlRoot(ElementName = "root")]
    public class IntegrationTestDto
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationTestDto"/> class.
        /// </summary>
        public IntegrationTestDto()
        {
            this.MessageGuid = Guid.NewGuid();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the MessageGuid.
        /// </summary>
        public Guid MessageGuid { get; set; }

        /// <summary>
        /// Gets or sets the Project.
        /// </summary>
        public string Project { get; set; }

        /// <summary>
        /// Gets or sets the TestDateTime.
        /// </summary>
        public DateTime TestDateTime { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The serializeAxAifEntryNumRefRangeDatesDto.
        /// </summary>
        /// <returns>The <see cref="string"/>.</returns>
        public string SerializeIntegrationTestDto()
        {
            string xmlValue = string.Empty;
            StringWriter stringWriter = new StringWriter();
            XmlSerializer serializer = new XmlSerializer(typeof(IntegrationTestDto));

            XmlRootAttribute xmlRootAttr = new XmlRootAttribute();
            xmlRootAttr.ElementName = "root";
            xmlRootAttr.IsNullable = true;

            XmlSerializer serializerResponse = new XmlSerializer(typeof(IntegrationTestDto), xmlRootAttr);

            using (var writer = XmlWriter.Create(stringWriter))
            {
                try
                {
                    serializer.Serialize(writer, this);

                    xmlValue = stringWriter.ToString();
                }
                catch (Exception ex)
                {
                    xmlValue = ex.ToString();
                }
            }

            return xmlValue;
        }

        #endregion
    }
}
