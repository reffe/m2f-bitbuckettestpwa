﻿using M2FDto.About;
using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace M2FDto
{
    /// <summary>
    /// Defines the <see cref="AboutDto" />.
    /// </summary>
    public class AboutDto
    {
        #region Fields

        /// <summary>
        /// Defines the asm.
        /// </summary>
        private Assembly asm;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutDto"/> class.
        /// </summary>
        public AboutDto()
        {
            this.asm = null;

            this.AssemblyInformation = new AssemblyInfoDto();

            this.DevOpsInformation = new DevOpsInfoDto();

            this.InstallationPackageInformation = new InstallationPackageInfoDto();

            this.EnvironmentInformation = new EnvironmentInfoDto();

            this.RuntimeInformation = new RuntimeInfoDto();

            this.SourceCodeRepositoryInformation = new SourceCodeRepositoryDto();

            this.LoggedUserInformation = new LoggedUserInfo();

            this.HttpContextConnectionInformation = new HttpContextConnectionInfo();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutDto"/> class.
        /// </summary>
        /// <param name="executingAssembly">The executingAssembly<see cref="Assembly"/>.</param>
        public AboutDto(Assembly executingAssembly)
        {
            this.asm = executingAssembly;

            this.AssemblyInformation = new AssemblyInfoDto();

            this.DevOpsInformation = new DevOpsInfoDto();

            this.InstallationPackageInformation = new InstallationPackageInfoDto();

            this.EnvironmentInformation = new EnvironmentInfoDto();

            this.RuntimeInformation = new RuntimeInfoDto();

            this.SourceCodeRepositoryInformation = new SourceCodeRepositoryDto();

            this.LoggedUserInformation = new LoggedUserInfo();

            this.HttpContextConnectionInformation = new HttpContextConnectionInfo();

            this.GetAllInformation();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the AssemblyInformation
        /// Gets the AssemblyInformation.
        /// </summary>
        public AssemblyInfoDto AssemblyInformation { get; set; }

        /// <summary>
        /// Gets or sets the DevOpsInformation
        /// Gets the DevOpsInformation.
        /// </summary>
        public DevOpsInfoDto DevOpsInformation { get; set; }

        /// <summary>
        /// Gets or sets the EnvironmentInformation
        /// Gets the EnvironmentInformation.
        /// </summary>
        public EnvironmentInfoDto EnvironmentInformation { get; set; }

        /// <summary>
        /// Gets or sets the HttpContextConnectionInformation
        /// Gets the HttpContextConnectionInformation.
        /// </summary>
        public HttpContextConnectionInfo HttpContextConnectionInformation { get; set; }

        /// <summary>
        /// Gets or sets the InstallationPackageInformation
        /// Gets the InstallationPackageInformation.
        /// </summary>
        public InstallationPackageInfoDto InstallationPackageInformation { get; set; }

        /// <summary>
        /// Gets or sets the LoggedUserInformation
        /// Gets the LoggedUserInformation.
        /// </summary>
        public LoggedUserInfo LoggedUserInformation { get; set; }

        /// <summary>
        /// Gets or sets the RuntimeInformation
        /// Gets the RuntimeInformation.
        /// </summary>
        public RuntimeInfoDto RuntimeInformation { get; set; }

        /// <summary>
        /// Gets or sets the SourceCodeRepositoryInformation
        /// Gets the SourceCodeRepositoryInformation.
        /// </summary>
        public SourceCodeRepositoryDto SourceCodeRepositoryInformation { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The SetAssembly.
        /// </summary>
        /// <param name="executingAssembly">The executingAssembly<see cref="Assembly"/>.</param>
        public void SetAssembly(Assembly executingAssembly)
        {
            this.asm = executingAssembly;

            this.GetAllInformation();
        }

        /// <summary>
        /// The ComposeUrl.
        /// </summary>
        /// <returns>The <see cref="string"/>.</returns>
        private string ComposeUrl()
        {
            string repositoryName = string.Empty;

            string repositoryUrl = this.GetRepositoryUrlFromAssemblyTitle();

            Regex regex = new Regex(@"(?<=\/tfs\/merlo\/software\/_git\/)[a-zA-Z0-9-]+", RegexOptions.IgnoreCase);
            if (!string.IsNullOrEmpty(repositoryUrl))
            {
                Match match = regex.Match(repositoryUrl);
                repositoryName = match.Value;
            }

            string originCommit = this.GetTokenFromAssemblyTitle("ORIGIN COMMIT");

            Uri uri = new Uri($"http://srvtfs:8080/tfs/merlo/software/_git/{repositoryName}/commit/{originCommit}");

            return uri.ToString();
        }

        /// <summary>
        /// The GetAllInformation.
        /// </summary>
        private void GetAllInformation()
        {
            this.GetAssemblyInformation();

            this.GetDevOpsInformation();

            this.GetInstallationPackageInformation();

            this.GetRuntimeInformation();

            this.GetEnvironmentInformation();

            this.GetSourceCodeRepositoryInformation();
        }

        /// <summary>
        /// The GetAssemblyInformation.
        /// </summary>
        private void GetAssemblyInformation()
        {
            if (this.asm != null)
            {
                this.AssemblyInformation.Title = ((AssemblyTitleAttribute)Attribute.GetCustomAttribute(this.asm, typeof(AssemblyTitleAttribute), false))?.Title;
                this.AssemblyInformation.Company = ((AssemblyCompanyAttribute)Attribute.GetCustomAttribute(this.asm, typeof(AssemblyCompanyAttribute)))?.Company;
                this.AssemblyInformation.Copyright = ((AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(this.asm, typeof(AssemblyCopyrightAttribute), false))?.Copyright;
                this.AssemblyInformation.Configuration = ((AssemblyConfigurationAttribute)Attribute.GetCustomAttribute(this.asm, typeof(AssemblyConfigurationAttribute), false))?.Configuration;
                this.AssemblyInformation.Product = ((AssemblyProductAttribute)Attribute.GetCustomAttribute(this.asm, typeof(AssemblyProductAttribute), false))?.Product;
                this.AssemblyInformation.Trademark = ((AssemblyTrademarkAttribute)Attribute.GetCustomAttribute(this.asm, typeof(AssemblyTrademarkAttribute), false))?.Trademark;
                this.AssemblyInformation.Culture = this.asm.GetName().CultureName.ToString();
                this.AssemblyInformation.InformationalVersion = ((AssemblyInformationalVersionAttribute)Attribute.GetCustomAttribute(this.asm, typeof(AssemblyInformationalVersionAttribute), false))?.InformationalVersion.ToString();
                this.AssemblyInformation.Version = this.asm.GetName().Version.ToString();
            }
        }

        /// <summary>
        /// The GetTokenFromAssemblyTitle.
        /// </summary>
        /// <returns>The <see cref="string"/>.</returns>
        private string GetBuildDateFromAssemblyTitle()
        {
            Regex regex = new Regex(@"BUILD DATE: [0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{7}", RegexOptions.IgnoreCase);

            if (this.AssemblyInformation != null
                && !string.IsNullOrWhiteSpace(this.AssemblyInformation.Title))
            {
                Match match = regex.Match(this.AssemblyInformation.Title);
                if (match.Success)
                {
                    return match.Value.Replace("BUILD DATE: ", string.Empty);
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// The GetTokensFromAssemblyTitle.
        /// </summary>
        private void GetDevOpsInformation()
        {
            this.DevOpsInformation.BuildDate = this.GetBuildDateFromAssemblyTitle();
            this.DevOpsInformation.BuildServer = this.GetTokenFromAssemblyTitle("BUILD MACHINE");
            this.DevOpsInformation.BuildUser = this.GetTokenFromAssemblyTitle("BUILD USER");
            this.DevOpsInformation.Officiality = this.GetTokenFromAssemblyTitle("OFFICIALITY");
            this.DevOpsInformation.PipelineName = this.GetTokenFromAssemblyTitle("PIPELINE NAME");

            string pipelineCounterStringValue = this.GetTokenFromAssemblyTitle("PIPELINE COUNTER");

            if (!string.IsNullOrWhiteSpace(pipelineCounterStringValue))
            {
                this.DevOpsInformation.PipelineCounter = Convert.ToInt32(pipelineCounterStringValue);
            }

            this.DevOpsInformation.PipelineLabel = this.GetTokenFromAssemblyTitle("PIPELINE LABEL");
            this.DevOpsInformation.PipelineTriggerUser = this.GetTokenFromAssemblyTitle("PIPELINE TRIGGER USER");

            string materialHasChangedStringValue = this.GetTokenFromAssemblyTitle("MATERIAL HAS CHANGED");

            if (!string.IsNullOrWhiteSpace(materialHasChangedStringValue))
            {
                this.DevOpsInformation.MaterialHasChanged = Convert.ToBoolean(materialHasChangedStringValue);
            }
        }

        /// <summary>
        /// The GetRuntimeInformation.
        /// </summary>
        private void GetEnvironmentInformation()
        {
            this.EnvironmentInformation.CommandLine = Environment.CommandLine;
            this.EnvironmentInformation.CurrentDirectory = Environment.CurrentDirectory;
            this.EnvironmentInformation.CurrentManagedThreadId = Environment.CurrentManagedThreadId;
            this.EnvironmentInformation.Is64BitOperatingSystem = Environment.Is64BitOperatingSystem;
            this.EnvironmentInformation.Is64BitProcess = Environment.Is64BitProcess;
            this.EnvironmentInformation.MachineName = Environment.MachineName;
            this.EnvironmentInformation.OSVersion = Environment.OSVersion.ToString();
            this.EnvironmentInformation.ProcessorCount = Environment.ProcessorCount;
            this.EnvironmentInformation.SystemDirectory = Environment.SystemDirectory;
            this.EnvironmentInformation.SystemPageSize = Environment.SystemPageSize;
            this.EnvironmentInformation.UserDomainName = Environment.UserDomainName;
            this.EnvironmentInformation.UserInteractive = Environment.UserInteractive;
            this.EnvironmentInformation.UserName = Environment.UserName;
            this.EnvironmentInformation.Version = Environment.Version.ToString();
            this.EnvironmentInformation.WorkingSet = Environment.WorkingSet;
        }

        /// <summary>
        /// The GetInstallationPackageInformation.
        /// </summary>
        private void GetInstallationPackageInformation()
        {
            this.InstallationPackageInformation.InstallationPackageBasePath = this.SourceCodeRepositoryInformation.BranchName = this.GetTokenFromAssemblyTitle("INSTALLATION PACKAGE BASE PATH");
        }

        /// <summary>
        /// The GetTokenFromAssemblyTitle.
        /// </summary>
        /// <returns>The <see cref="string"/>.</returns>
        private string GetRepositoryUrlFromAssemblyTitle()
        {
            Regex regex = new Regex(@"REPOSITORY URL: http:\/\/[a-zA-Z0-9]+:[0-9]+\/tfs\/merlo\/software\/_git\/[a-zA-Z0-9-]+", RegexOptions.IgnoreCase);

            if (this.AssemblyInformation != null
                && !string.IsNullOrWhiteSpace(this.AssemblyInformation.Title))
            {
                Match match = regex.Match(this.AssemblyInformation.Title);
                if (match.Success)
                {
                    return match.Value;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// The GetRuntimeInformation.
        /// </summary>
        private void GetRuntimeInformation()
        {
            string operatingSystem = string.Empty;

            this.RuntimeInformation.FrameworkDescription = System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription;
            this.RuntimeInformation.OSArchitecture = System.Runtime.InteropServices.RuntimeInformation.OSArchitecture.ToString();
            this.RuntimeInformation.OSDescription = System.Runtime.InteropServices.RuntimeInformation.OSDescription;
            this.RuntimeInformation.ProcessArchitecture = System.Runtime.InteropServices.RuntimeInformation.ProcessArchitecture.ToString();

            if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.FreeBSD))
            {
                operatingSystem = System.Runtime.InteropServices.OSPlatform.FreeBSD.ToString();
            }
            else if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.Linux))
            {
                operatingSystem = System.Runtime.InteropServices.OSPlatform.Linux.ToString();
            }
            else if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.OSX))
            {
                operatingSystem = System.Runtime.InteropServices.OSPlatform.OSX.ToString();
            }
            else if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.Windows))
            {
                operatingSystem = System.Runtime.InteropServices.OSPlatform.Windows.ToString();
            }
            else
            {
                operatingSystem = "UNKNOWN";
            }

            this.RuntimeInformation.OS = operatingSystem;
        }

        /// <summary>
        /// The GetSourceCodeRepositoryInformation.
        /// </summary>
        private void GetSourceCodeRepositoryInformation()
        {
            this.SourceCodeRepositoryInformation.RepositoryUrl = this.GetRepositoryUrlFromAssemblyTitle().Replace("REPOSITORY URL: ", string.Empty);
            this.SourceCodeRepositoryInformation.BranchName = this.GetTokenFromAssemblyTitle("BRANCH NAME");
            this.SourceCodeRepositoryInformation.OriginCommit = this.GetTokenFromAssemblyTitle("ORIGIN COMMIT");
            this.SourceCodeRepositoryInformation.LocalCommit = this.GetTokenFromAssemblyTitle("LOCAL COMMIT");
            this.SourceCodeRepositoryInformation.LastOriginCommit = this.ComposeUrl();
        }

        /// <summary>
        /// The GetTokenFromAssemblyTitle.
        /// </summary>
        /// <param name="key">The key<see cref="string"/>.</param>
        /// <returns>The <see cref="string"/>.</returns>
        private string GetTokenFromAssemblyTitle(string key)
        {
            Regex regex = new Regex(@"[\|]*[\s]*" + key + @"[\s]*:[\s]*[a-zA-Z0-9_\-\s\\\.]+[\s]*[\|]*", RegexOptions.IgnoreCase);

            if (this.AssemblyInformation != null
                && !string.IsNullOrWhiteSpace(this.AssemblyInformation.Title))
            {
                Match match = regex.Match(this.AssemblyInformation.Title);
                if (match.Success)
                {
                    return match.Value.Replace("|", string.Empty).Replace(":", string.Empty).Replace(key, string.Empty).Trim();
                }
            }

            return string.Empty;
        }

        #endregion
    }
}
