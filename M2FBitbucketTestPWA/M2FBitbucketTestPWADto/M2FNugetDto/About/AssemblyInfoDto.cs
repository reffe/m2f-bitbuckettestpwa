﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="AssemblyInfoDto" />.
    /// </summary>
    public class AssemblyInfoDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Company
        /// Gets the Company.
        /// </summary>
        public string Company { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Configuration
        /// Gets the Configuration.
        /// </summary>
        public string Configuration { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Copyright
        /// Gets the Copyright.
        /// </summary>
        public string Copyright { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Culture
        /// Gets the Culture.
        /// </summary>
        public string Culture { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the InformationalVersion
        /// Gets the InformationalVersion.
        /// </summary>
        public string InformationalVersion { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Product
        /// Gets the Product.
        /// </summary>
        public string Product { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Title
        /// Gets the Title.
        /// </summary>
        public string Title { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Trademark
        /// Gets the Trademark.
        /// </summary>
        public string Trademark { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Version
        /// Gets the Version.
        /// </summary>
        public string Version { get; set; } = string.Empty;

        #endregion
    }
}
