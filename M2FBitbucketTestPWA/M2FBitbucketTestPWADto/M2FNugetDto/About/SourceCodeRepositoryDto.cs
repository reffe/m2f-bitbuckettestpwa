﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="SourceCodeRepositoryDto" />.
    /// </summary>
    public class SourceCodeRepositoryDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the BranchName
        /// Gets the BranchName.
        /// </summary>
        public string BranchName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the LastOriginCommit
        /// Gets the LastOriginCommit.
        /// </summary>
        public string LastOriginCommit { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the LocalCommit
        /// Gets the LocalCommit.
        /// </summary>
        public string LocalCommit { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the OriginCommit
        /// Gets the OriginCommit.
        /// </summary>
        public string OriginCommit { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the RepositoryUrl
        /// Gets the RepositoryUrl.
        /// </summary>
        public string RepositoryUrl { get; set; } = string.Empty;

        #endregion
    }
}
