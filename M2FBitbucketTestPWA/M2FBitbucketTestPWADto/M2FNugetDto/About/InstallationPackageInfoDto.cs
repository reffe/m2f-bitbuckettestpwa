﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="InstallationPackageInfoDto" />.
    /// </summary>
    public class InstallationPackageInfoDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the InstallationPackageBasePath
        /// Gets the InstallationPackageBasePath.
        /// </summary>
        public string InstallationPackageBasePath { get; set; } = string.Empty;

        #endregion
    }
}
