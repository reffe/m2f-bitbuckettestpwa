﻿using System.Collections.Generic;

namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="LoggedUserInfo" />.
    /// </summary>
    public class LoggedUserInfo
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggedUserInfo"/> class.
        /// </summary>
        public LoggedUserInfo()
        {
            this.MemberOf = new List<MemberOfInfoDto>();
            this.Roles = new List<RoleInfoDto>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the DallasKey.
        /// </summary>
        public string DallasKey { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the DistinguishedName.
        /// </summary>
        public string DistinguishedName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the EmplId.
        /// </summary>
        public string EmplId { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the GivenName.
        /// </summary>
        public string GivenName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the LoggedUser.
        /// </summary>
        public string LoggedUser { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Mail.
        /// </summary>
        public string Mail { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the MemberOf.
        /// </summary>
        public List<MemberOfInfoDto> MemberOf { get; set; }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Roles.
        /// </summary>
        public List<RoleInfoDto> Roles { get; set; }

        /// <summary>
        /// Gets or sets the SAMAccountName.
        /// </summary>
        public string SAMAccountName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Sn.
        /// </summary>
        public string Sn { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the UserPrincipalName.
        /// </summary>
        public string UserPrincipalName { get; set; } = string.Empty;

        #endregion
    }
}
