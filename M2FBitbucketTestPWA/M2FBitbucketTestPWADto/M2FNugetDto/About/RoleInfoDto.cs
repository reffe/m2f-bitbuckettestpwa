﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="RoleInfoDto" />.
    /// </summary>
    public class RoleInfoDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the RoleName.
        /// </summary>
        public string RoleName { get; set; } = string.Empty;

        #endregion
    }
}
