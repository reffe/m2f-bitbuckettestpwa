﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="RuntimeInfoDto" />.
    /// </summary>
    public class RuntimeInfoDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the FrameworkDescription.
        /// </summary>
        public string FrameworkDescription { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the OS.
        /// </summary>
        public string OS { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the OSArchitecture.
        /// </summary>
        public string OSArchitecture { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the OSDescription.
        /// </summary>
        public string OSDescription { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the ProcessArchitecture.
        /// </summary>
        public string ProcessArchitecture { get; set; } = string.Empty;

        #endregion
    }
}
