﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="DevOpsInfoDto" />.
    /// </summary>
    public class DevOpsInfoDto
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DevOpsInfoDto"/> class.
        /// </summary>
        public DevOpsInfoDto()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the BuildDate
        /// Gets the BuildDate.
        /// </summary>
        public string BuildDate { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the BuildServer
        /// Gets the BuildServer.
        /// </summary>
        public string BuildServer { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the BuildUser
        /// Gets the BuildUser.
        /// </summary>
        public string BuildUser { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets a value indicating whether MaterialHasChanged
        /// Gets the MaterialHasChanged.
        /// </summary>
        public bool MaterialHasChanged { get; set; }

        /// <summary>
        /// Gets or sets the Officiality
        /// Gets the Officiality.
        /// </summary>
        public string Officiality { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the PipelineCounter
        /// Gets the PipelineCounter.
        /// </summary>
        public int PipelineCounter { get; set; }

        /// <summary>
        /// Gets or sets the PipelineLabel
        /// Gets the PipelineLabel.
        /// </summary>
        public string PipelineLabel { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the PipelineName
        /// Gets the PipelineName.
        /// </summary>
        public string PipelineName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the PipelineTriggerUser
        /// Gets the PipelineTriggerUser.
        /// </summary>
        public string PipelineTriggerUser { get; set; } = string.Empty;

        #endregion
    }
}
