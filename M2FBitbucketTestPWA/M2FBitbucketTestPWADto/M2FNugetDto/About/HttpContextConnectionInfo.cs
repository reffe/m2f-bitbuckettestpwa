﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="HttpContextConnectionInfo" />.
    /// </summary>
    public class HttpContextConnectionInfo
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ClientCertificate.
        /// </summary>
        public string ClientCertificate { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        public string Id { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the LocalIpAddress.
        /// </summary>
        public string LocalIpAddress { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the LocalPort.
        /// </summary>
        public int LocalPort { get; set; }

        /// <summary>
        /// Gets or sets the RemoteIpAddress.
        /// </summary>
        public string RemoteIpAddress { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the RemotePort.
        /// </summary>
        public int RemotePort { get; set; }

        #endregion
    }
}
