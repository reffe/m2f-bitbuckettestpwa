﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="MemberOfInfoDto" />.
    /// </summary>
    public class MemberOfInfoDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the DistinguishedName.
        /// </summary>
        public string DistinguishedName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the GroupName.
        /// </summary>
        public string GroupName { get; set; } = string.Empty;

        #endregion
    }
}
