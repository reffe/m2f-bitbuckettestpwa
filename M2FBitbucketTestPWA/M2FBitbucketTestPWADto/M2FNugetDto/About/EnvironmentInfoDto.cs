﻿namespace M2FDto.About
{
    /// <summary>
    /// Defines the <see cref="EnvironmentInfoDto" />.
    /// </summary>
    public class EnvironmentInfoDto
    {
        #region Properties

        /// <summary>
        /// Gets or sets the CommandLine.
        /// </summary>
        public string CommandLine { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the CurrentDirectory.
        /// </summary>
        public string CurrentDirectory { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the CurrentManagedThreadId.
        /// </summary>
        public int CurrentManagedThreadId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Is64BitOperatingSystem.
        /// </summary>
        public bool Is64BitOperatingSystem { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Is64BitProcess.
        /// </summary>
        public bool Is64BitProcess { get; set; }

        /// <summary>
        /// Gets or sets the MachineName.
        /// </summary>
        public string MachineName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the OSVersion.
        /// </summary>
        public string OSVersion { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the ProcessorCount.
        /// </summary>
        public int ProcessorCount { get; set; }

        /// <summary>
        /// Gets or sets the SystemDirectory.
        /// </summary>
        public string SystemDirectory { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the SystemPageSize.
        /// </summary>
        public int SystemPageSize { get; set; }

        /// <summary>
        /// Gets or sets the UserDomainName.
        /// </summary>
        public string UserDomainName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets a value indicating whether UserInteractive.
        /// </summary>
        public bool UserInteractive { get; set; }

        /// <summary>
        /// Gets or sets the UserName.
        /// </summary>
        public string UserName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the Version.
        /// </summary>
        public string Version { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the WorkingSet.
        /// </summary>
        public long WorkingSet { get; set; }

        #endregion
    }
}
