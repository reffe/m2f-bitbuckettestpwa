﻿namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="Ax2009AifServicesSettings" />.
    /// </summary>
    public class Ax2009AifServicesSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets the BindingName.
        /// </summary>
        public string BindingName { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the ServiceName.
        /// </summary>
        public string ServiceName { get; set; }

        #endregion
    }
}
