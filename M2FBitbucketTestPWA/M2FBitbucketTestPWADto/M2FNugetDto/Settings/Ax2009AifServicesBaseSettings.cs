﻿namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="Ax2009AifServicesBaseSettings" />.
    /// </summary>
    public class Ax2009AifServicesBaseSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets the BaseUrl.
        /// </summary>
        public string BaseUrl { get; set; }

        #endregion
    }
}
