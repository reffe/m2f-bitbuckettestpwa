﻿namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="SharedFolderSettings" />.
    /// </summary>
    public class SharedFolderSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets the SharedFolderName.
        /// </summary>
        public string SharedFolderName { get; set; }

        /// <summary>
        /// Gets or sets the SharedFolderPath.
        /// </summary>
        public string SharedFolderPath { get; set; }

        #endregion
    }
}
