﻿namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="AuthenticationSettings" />.
    /// </summary>
    public class AuthenticationSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets the LoginTypology.
        /// </summary>
        public string LoginTypology { get; set; }

        #endregion
    }
}
