﻿namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="HttpSettings" />.
    /// </summary>
    public class HttpSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets the BaseUrl of the .NET Core APP.
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Gets or sets the HttpPort used by .NET Core APP.
        /// </summary>
        public int HttpPort { get; set; }

        /// <summary>
        /// Gets or sets the HttpsPort .NET Core APP.
        /// </summary>
        public int HttpsPort { get; set; }

        #endregion
    }
}
