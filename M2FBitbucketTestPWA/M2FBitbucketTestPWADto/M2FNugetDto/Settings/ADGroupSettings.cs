﻿namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="ADGroupSettings" />.
    /// </summary>
    public class ADGroupSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ADGroupName.
        /// </summary>
        public string ADGroupName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsIT.
        /// </summary>
        public bool IsIT { get; set; }

        #endregion
    }
}
