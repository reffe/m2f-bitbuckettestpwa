﻿using System.Collections.Generic;

namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="NotificationSettings" />.
    /// </summary>
    public class NotificationSettings
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationSettings"/> class.
        /// </summary>
        public NotificationSettings()
        {
            this.NotificationEventSettings = new List<NotificationEventSettings>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the NotificationEventSettings.
        /// </summary>
        public List<NotificationEventSettings> NotificationEventSettings { get; set; }

        #endregion
    }
}
