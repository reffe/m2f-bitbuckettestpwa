﻿namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="ConnectionSettings" />.
    /// </summary>
    public class ConnectionSettings
    {
        #region Properties

        /// <summary>
        /// Gets or sets the CommandTimeout.
        /// </summary>
        public int CommandTimeout { get; set; }

        /// <summary>
        /// Gets or sets the ConnectionName.
        /// </summary>
        public string ConnectionName { get; set; }

        /// <summary>
        /// Gets or sets the ConnectionString.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        public string Description { get; set; }

        #endregion
    }
}
