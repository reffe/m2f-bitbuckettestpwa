﻿using System.Collections.Generic;

namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="NotificationEventSettings" />.
    /// </summary>
    public class NotificationEventSettings
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationEventSettings"/> class.
        /// </summary>
        public NotificationEventSettings()
        {
            this.Attachments = new List<string>();
            this.Cc = new List<string>();
            this.To = new List<string>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Attachments.
        /// </summary>
        public List<string> Attachments { get; set; }

        /// <summary>
        /// Gets or sets the Cc.
        /// </summary>
        public List<string> Cc { get; set; }

        /// <summary>
        /// Gets or sets the EventDescription.
        /// </summary>
        public string EventDescription { get; set; }

        /// <summary>
        /// Gets or sets the EventName.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the To.
        /// </summary>
        public List<string> To { get; set; }

        #endregion
    }
}
