﻿using System.Collections.Generic;

namespace M2FDto.Settings
{
    /// <summary>
    /// Defines the <see cref="SharedFoldersSettings" />.
    /// </summary>
    public class SharedFoldersSettings
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SharedFoldersSettings"/> class.
        /// </summary>
        public SharedFoldersSettings()
        {
            this.SharedFolders = new List<SharedFolderSettings>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the SharedFolders.
        /// </summary>
        public List<SharedFolderSettings> SharedFolders { get; set; }

        #endregion
    }
}
