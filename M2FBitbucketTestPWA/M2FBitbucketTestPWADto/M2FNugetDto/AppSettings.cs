﻿using M2FDto.Settings;
using System.Collections.Generic;

namespace M2FDto
{
    /// <summary>
    /// Defines the <see cref="AppSettings" />.
    /// </summary>
    /// <typeparam name="TAppSpecificSettings">.</typeparam>
    public class AppSettings<TAppSpecificSettings>
        where TAppSpecificSettings : class, new()
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppSettings{TAppSpecificSettings}"/> class.
        /// </summary>
        public AppSettings()
        {
            this.AppSpecificSettings = new TAppSpecificSettings();
            this.AuthenticationSettings = new AuthenticationSettings();
            this.Ax2009AifServicesBaseSettings = new Ax2009AifServicesBaseSettings();
            this.Ax2009AifServicesSettings = new List<Ax2009AifServicesSettings>();
            this.DatabaseConnectionsSettings = new List<ConnectionSettings>();
            this.HttpSettings = new HttpSettings();
            this.NotificationSettings = new NotificationSettings();
            this.SharedFoldersSettings = new SharedFoldersSettings();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the AppSpecificSettings.
        /// </summary>
        public TAppSpecificSettings AppSpecificSettings { get; set; }

        /// <summary>
        /// Gets or sets the AuthenticationSettings.
        /// </summary>
        public AuthenticationSettings AuthenticationSettings { get; set; }

        /// <summary>
        /// Gets or sets the Ax2009AifServicesBaseSettings.
        /// </summary>
        public Ax2009AifServicesBaseSettings Ax2009AifServicesBaseSettings { get; set; }

        /// <summary>
        /// Gets or sets the Ax2009AifServicesSettings.
        /// </summary>
        public List<Ax2009AifServicesSettings> Ax2009AifServicesSettings { get; set; }

        /// <summary>
        /// Gets or sets the DatabaseConnectionsSettings.
        /// </summary>
        public List<ConnectionSettings> DatabaseConnectionsSettings { get; set; }

        /// <summary>
        /// Gets or sets the HttpSettings.
        /// </summary>
        public HttpSettings HttpSettings { get; set; }

        /// <summary>
        /// Gets or sets the NotificationSettings.
        /// </summary>
        public NotificationSettings NotificationSettings { get; set; }

        /// <summary>
        /// Gets or sets the SharedFoldersSettings.
        /// </summary>
        public SharedFoldersSettings SharedFoldersSettings { get; set; }

        #endregion
    }
}
