﻿namespace M2FDto.Constant
{
    /// <summary>
    /// Defines the <see cref="GeneralConstantDto" />.
    /// </summary>
    public static class GeneralConstantDto
    {
        #region Constants

        /// <summary>
        /// Defines the AppSettingFileName.
        /// </summary>
        public const string AppSettingFileName = "appsettings.json";

        /// <summary>
        /// Defines the DBDboSchema.
        /// </summary>
        public const string DBDboSchema = "dbo";

        /// <summary>
        /// Defines the ErrorPage.
        /// </summary>
        public const string ErrorPage = "/Error";

        #endregion
    }
}
