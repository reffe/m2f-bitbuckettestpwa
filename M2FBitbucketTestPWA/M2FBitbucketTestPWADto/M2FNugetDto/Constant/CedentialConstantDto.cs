﻿namespace M2FDto.Constant
{
    /// <summary>
    /// Defines the <see cref="CedentialConstantDto" />.
    /// </summary>
    public static class CedentialConstantDto
    {
        #region Constants

        /// <summary>
        /// Defines the ClientSuffixStringInWcfClientName.
        /// </summary>
        public const string ClientSuffixStringInWcfClientName = "client";

        /// <summary>
        /// Defines the NetworkCredentialDomain.
        /// </summary>
        public const string NetworkCredentialDomain = "merlont";

        /// <summary>
        /// Defines the NetworkCredentialPassword.
        /// </summary>
        public const string NetworkCredentialPassword = "axsvcxyz09";

        /// <summary>
        /// Defines the NetworkCredentialUsername.
        /// </summary>
        public const string NetworkCredentialUsername = "axsvc";

        /// <summary>
        /// Defines the TestFileExtension.
        /// </summary>
        public const string TestFileExtension = "txt";

        /// <summary>
        /// Defines the TestFileName.
        /// </summary>
        public const string TestFileName = "IntegrationTestFile_";

        #endregion
    }
}
