﻿namespace M2FDto.Constant
{
    /// <summary>
    /// Defines the <see cref="IdentityServerConstantDto" />.
    /// </summary>
    public static class IdentityServerConstantDto
    {
        #region Constants

        /// <summary>
        /// Defines the AuthorizationRoleAdmin.
        /// </summary>
        public const string AuthorizationRoleAdmin = "Domain Admins, CED, IT-web, Administrator";

        /// <summary>
        /// Defines the AuthorizationRoleTester.
        /// </summary>
        public const string AuthorizationRoleTester = "Domain Admins, CED, IT-web, Administrator, Tester";

        /// <summary>
        /// Defines the ClaimsDallasKey.
        /// </summary>
        public const string ClaimsDallasKey = "dallaskey";

        /// <summary>
        /// Defines the ClaimsEmplId.
        /// </summary>
        public const string ClaimsEmplId = "emplid";

        /// <summary>
        /// Defines the ClaimsIdentityServerRole.
        /// </summary>
        public const string ClaimsIdentityServerRole = "identityserverrole";

        /// <summary>
        /// Defines the ClaimsName.
        /// </summary>
        public const string ClaimsName = "name";

        /// <summary>
        /// Defines the ClaimsPrefixConnectionInfo.
        /// </summary>
        public const string ClaimsPrefixConnectionInfo = "ConnectionInfo_";

        /// <summary>
        /// Defines the ClaimsPrefixEnvInfo.
        /// </summary>
        public const string ClaimsPrefixEnvInfo = "EnvironmentInfo_";

        /// <summary>
        /// Defines the ClaimsPrefixLDAP.
        /// </summary>
        public const string ClaimsPrefixLDAP = "LDAP_";

        /// <summary>
        /// Defines the ClaimsRole.
        /// </summary>
        public const string ClaimsRole = "role";

        /// <summary>
        /// Defines the LogInTypePin.
        /// </summary>
        public const string LogInTypePin = "LOGIN_WITH_PIN";

        /// <summary>
        /// Defines the LogInTypeStandard.
        /// </summary>
        public const string LogInTypeStandard = "STANDARD_LOGIN";

        #endregion
    }
}
