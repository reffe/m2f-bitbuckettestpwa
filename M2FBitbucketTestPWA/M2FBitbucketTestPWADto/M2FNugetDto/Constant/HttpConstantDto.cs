﻿namespace M2FDto.Constant
{
    /// <summary>
    /// Defines the <see cref="HttpConstantDto" />.
    /// </summary>
    public static class HttpConstantDto
    {
        #region Constants

        /// <summary>
        /// Defines the ChannelNameAnonymus.
        /// </summary>
        public const string ChannelNameAnonymus = "BlazorWasmApp.AnonymousAPI";

        /// <summary>
        /// Defines the ChannelNameAuthenticated.
        /// </summary>
        public const string ChannelNameAuthenticated = "BlazorWasmApp.AuthenticatedAPI";

        /// <summary>
        /// Defines the DefaultHeaderValue.
        /// </summary>
        public const string DefaultHeaderValue = "version";

        #endregion
    }
}
