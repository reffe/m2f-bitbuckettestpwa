﻿namespace M2FDto.Constant
{
    /// <summary>
    /// Defines the <see cref="ApiConstantDto" />.
    /// </summary>
    public static class ApiConstantDto
    {
        #region Constants

        /// <summary>
        /// Defines the AboutApiController.
        /// </summary>
        public const string AboutApiController = "api/About/GetInfo";

        /// <summary>
        /// Defines the IntegrationTestBaseApiController.
        /// </summary>
        public const string IntegrationTestBaseApiController = "api/integrationtest";

        /// <summary>
        /// Defines the TestAifApiController.
        /// </summary>
        public const string TestAifApiController = IntegrationTestBaseApiController + "/TestAifService";

        /// <summary>
        /// Defines the TestMethodExceptionApiController.
        /// </summary>
        public const string TestMethodExceptionApiController = IntegrationTestBaseApiController + "/TestMethodException";

        /// <summary>
        /// Defines the TestMethodOkApiController.
        /// </summary>
        public const string TestMethodOkApiController = IntegrationTestBaseApiController + "/TestMethodOk";

        #endregion
    }
}
