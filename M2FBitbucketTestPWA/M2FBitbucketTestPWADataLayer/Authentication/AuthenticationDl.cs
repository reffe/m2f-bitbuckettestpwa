using M2FDto;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using Serilog;

using System;
using System.Collections.Generic;
using System.Linq;

using M2FBitbucketTestPWADataLayer.DbContexts.IdentityServer.Custom;
using M2FBitbucketTestPWADataLayer.DbContexts.IdentityServer.Standard;

using M2FBitbucketTestPWADataLayerContracts.Authentication;

using M2FBitbucketTestPWADto;

using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Custom;
using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard;
using Microsoft.AspNetCore.Identity;

namespace M2FBitbucketTestPWADataLayer.Authentication
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="AuthenticationDl" />
    /// </summary>
    public class AuthenticationDl : IAuthenticationDl
    {
        #region Fields

        /// <summary>
        /// Defines the aboutInfo
        /// </summary>
        private readonly AboutDto aboutInfo;

        /// <summary>
        /// Defines the appSettings
        /// </summary>
        private readonly IOptions<AppSettings<AppSpecificSettings>> appSettings;

        /// <summary>
        /// Defines the identityServerCustomDbCtx
        /// </summary>
        private readonly IdentityServerCustomDbContext identityServerCustomDbCtx;

        /// <summary>
        /// Defines the identityServerStandardDbCtx
        /// </summary>
        private readonly IdentityServerStandardDbContext identityServerStandardDbCtx;

        /// <summary>
        /// Defines the logger
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationDl"/> class.
        /// </summary>
        /// <param name="appSettings">The appSettings<see cref="IOptions{AppSettings{AppSpecificSettings}}"/></param>
        /// <param name="logger">The logger<see cref="ILogger"/></param>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/></param>
        /// <param name="identityServerStandardDbCtx">The identityServerStandardCtx<see cref="IdentityServerStandardDbContext"/></param>
        /// <param name="identityServerCustomDbCtx">The identityServerCustomCtx<see cref="IdentityServerCustomDbContext"/></param>
        public AuthenticationDl(
            IOptions<AppSettings<AppSpecificSettings>> appSettings,
            ILogger logger,
            AboutDto aboutInfo,
            IdentityServerStandardDbContext identityServerStandardDbCtx,
            IdentityServerCustomDbContext identityServerCustomDbCtx)
        {
            this.appSettings = appSettings;
            this.logger = logger;
            this.aboutInfo = aboutInfo;
            this.identityServerStandardDbCtx = identityServerStandardDbCtx;
            this.identityServerCustomDbCtx = identityServerCustomDbCtx;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The GetRolesByUserId
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="List{Role}"/></returns>
        public IEnumerable<IdentityRole> GetRolesByUserId(string id)
        {
            IEnumerable<IdentityRole> resStandardDbCtx = null;
            IEnumerable<IdentityRole> resCustomDbCtx = null;
            List<IdentityRole> res = new List<IdentityRole>();

            if (!string.IsNullOrWhiteSpace(id))
            {
                try
                {
                    // Ricavo il RUOLO di default a livello di APPLICAZIONE-UFFICIO
                    // nello schema [dbo] del database di Identity Server
                    resStandardDbCtx =
                        this.identityServerStandardDbCtx
                        .M2FBusinessUnitUsers
                        .Where(x => x.UserId.Equals(id))
                        .Join(
                            this.identityServerStandardDbCtx.M2FApplicationBusinessUnitRoles,
                            (businessUnitUsr) => businessUnitUsr.M2FBusinessUnitsId,
                            (businessUnitRole) => businessUnitRole.M2FBusinessUnitsId,
                            (usrRole, role) => role)
                        .Where(x => x.M2FApplications.Name.Equals("M2FBitbucketTestPWA") || x.M2FApplications.Name.Equals("ALL APPLICATIONS"))
                        .Include(x => x.Role)
                        .Select(x => x.Role)
                        .ToList();

                    // Ricavo il RUOLO specifico dell'utente a livello di APPLICAZIONE
                    // nello schema [M2FBitbucketTestPWA] del database di Identity Server
                    resCustomDbCtx =
                        this.identityServerCustomDbCtx
                        .UserRoles
                        .Where(x => x.UserId.Equals(id))
                        .Include(x => x.Role)
                        .Select(x => x.Role)
                        .ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            res.AddRange(resStandardDbCtx);
            res.AddRange(resCustomDbCtx);

            return res;
        }

        /// <summary>
        /// The GetUserById
        /// </summary>
        /// <param name="id">The id<see cref="string"/></param>
        /// <returns>The <see cref="ApplicationUser"/></returns>
        public ApplicationUser GetUserById(string id)
        {
            var user = this.identityServerStandardDbCtx.Users.Where(u => u.Id.Equals(id)).FirstOrDefault();

            return user;
        }

        /// <summary>
        /// The GetUserByUsername
        /// </summary>
        /// <param name="username">The username<see cref="string"/></param>
        /// <returns>The <see cref="ApplicationUser"/></returns>
        public ApplicationUser GetUserByUsername(string username)
        {
            var user = this.identityServerStandardDbCtx.Users.Where(u => string.Equals(u.Email, username)).FirstOrDefault();

            return user;
        }

        /// <summary>
        /// The ValidatePassword
        /// </summary>
        /// <param name="username">The username<see cref="string"/></param>
        /// <param name="plainTextPassword">The plainTextPassword<see cref="string"/></param>
        /// <returns>The <see cref="bool"/></returns>
        public bool ValidatePassword(string username, string plainTextPassword)
        {
            var user = this.identityServerStandardDbCtx.Users.Where(u => string.Equals(u.Email, username)).FirstOrDefault();

            if (user == null)
            {
                return false;
            }

            if (string.Equals(plainTextPassword, user.PasswordHash))
            {
                return true;
            }

            return false;
        }

        #endregion
    }

    #endregion
}
