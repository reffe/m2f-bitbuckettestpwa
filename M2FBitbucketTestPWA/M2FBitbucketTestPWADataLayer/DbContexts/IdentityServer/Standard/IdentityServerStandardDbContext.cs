using IdentityServer4.EntityFramework.Options;

using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard;

using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace M2FBitbucketTestPWADataLayer.DbContexts.IdentityServer.Standard
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="IdentityServerStandardDbContext" />
    /// </summary>
    public class IdentityServerStandardDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityServerStandardDbContext"/> class.
        /// </summary>
        /// <param name="options">The options<see cref="DbContextOptions"/></param>
        /// <param name="operationalStoreOptions">The operationalStoreOptions<see cref="IOptions{OperationalStoreOptions}"/></param>
        public IdentityServerStandardDbContext(
            DbContextOptions<IdentityServerStandardDbContext> options,
            IOptions<OperationalStoreOptions> operationalStoreOptions)
            : base(options, operationalStoreOptions)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the M2FBusinessUnitRoles
        /// </summary>
        public DbSet<M2FApplicationBusinessUnitRoles> M2FApplicationBusinessUnitRoles { get; set; }

        /// <summary>
        /// Gets or sets the M2FBusinessUnits
        /// </summary>
        public DbSet<M2FBusinessUnits> M2FBusinessUnits { get; set; }

        /// <summary>
        /// Gets or sets the M2FBusinessUnitUsers
        /// </summary>
        public DbSet<M2FBusinessUnitUsers> M2FBusinessUnitUsers { get; set; }

        /// <summary>
        /// Gets or sets the M2FCompanies
        /// </summary>
        public DbSet<M2FCompanies> M2FCompanies { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The OnModelCreating
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            this.ConfigureEntityM2FApplications(builder);

            this.ConfigureEntityM2FCompanies(builder);

            this.ConfigureEntityM2FBusinessUnits(builder);

            this.ConfigureEntityM2FBusinessUnitRoles(builder);

            this.ConfigureEntityM2FBusinessUnitUsers(builder);
        }

        /// <summary>
        /// The ConfigureEntityM2FApplications
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        private void ConfigureEntityM2FApplications(ModelBuilder builder)
        {
            builder
                .Entity<M2FApplications>()
                .HasKey(x => new { x.Id });
        }

        /// <summary>
        /// The ConfigureEntityM2FBusinessUnitRoles
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        private void ConfigureEntityM2FBusinessUnitRoles(ModelBuilder builder)
        {
            builder
                .Entity<M2FApplicationBusinessUnitRoles>()
                .HasOne(x => x.M2FApplications);

            builder
                .Entity<M2FApplicationBusinessUnitRoles>()
                .HasOne(x => x.M2FBusinessUnits);

            builder
                .Entity<M2FApplicationBusinessUnitRoles>()
                .HasOne(x => x.Role);

            builder
                .Entity<M2FApplicationBusinessUnitRoles>()
                .HasKey(x => new { x.M2FApplicationsId, x.M2FBusinessUnitsId, x.RoleId });
        }

        /// <summary>
        /// The ConfigureEntityM2FBusinessUnits
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        private void ConfigureEntityM2FBusinessUnits(ModelBuilder builder)
        {
            builder
                .Entity<M2FBusinessUnits>()
                .HasOne(x => x.M2FCompanies);

            builder
                .Entity<M2FBusinessUnits>()
                .HasKey(x => new { x.Id });
        }

        /// <summary>
        /// The ConfigureEntityM2FBusinessUnitUsers
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        private void ConfigureEntityM2FBusinessUnitUsers(ModelBuilder builder)
        {
            builder
                .Entity<M2FBusinessUnitUsers>()
                .HasOne(x => x.M2FBusinessUnits);

            builder
                .Entity<M2FBusinessUnitUsers>()
                .HasOne(x => x.User);

            builder
                .Entity<M2FBusinessUnitUsers>()
                .HasKey(x => new { x.M2FBusinessUnitsId, x.UserId });
        }

        /// <summary>
        /// The ConfigureEntityM2FCompanies
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        private void ConfigureEntityM2FCompanies(ModelBuilder builder)
        {
            builder
                .Entity<M2FCompanies>()
                .HasKey(x => new { x.Id });
        }

        #endregion
    }

    #endregion
}
