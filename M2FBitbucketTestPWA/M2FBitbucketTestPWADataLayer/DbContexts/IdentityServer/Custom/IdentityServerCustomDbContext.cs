using IdentityServer4.EntityFramework.Options;

using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Custom;
using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard;

namespace M2FBitbucketTestPWADataLayer.DbContexts.IdentityServer.Custom
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="IdentityServerCustomDbContext" />
    /// </summary>
    public class IdentityServerCustomDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityServerCustomDbContext"/> class.
        /// </summary>
        /// <param name="options">The options<see cref="DbContextOptions"/></param>
        /// <param name="operationalStoreOptions">The operationalStoreOptions<see cref="IOptions{OperationalStoreOptions}"/></param>
        public IdentityServerCustomDbContext(
            DbContextOptions<IdentityServerCustomDbContext> options,
            IOptions<OperationalStoreOptions> operationalStoreOptions)
            : base(options, operationalStoreOptions)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Roles
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Gets or sets the UserRoles
        /// </summary>
        public DbSet<UserRole> UserRoles { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The OnModelCreating
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // You can globally assign schema here
            builder.HasDefaultSchema("M2FBitbucketTestPWA");

            this.ConfigureEntityRole(builder);

            this.ConfigureEntityUserRole(builder);
        }

        /// <summary>
        /// The ConfigureEntityRole
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        private void ConfigureEntityRole(ModelBuilder builder)
        {
            // E' necessario effettuare l'ignore dell'entity base IdentityUserRole per evitare l'errore a run-time
            // "System.Data.SqlClient.SqlException: Invalid column name 'Discriminator'"
            // Questo accade perchè l'entity UserRole eredita dall'entity IdentityUserRole<string>
            builder.Ignore<IdentityUserRole<string>>();

            // Configurazione dell'entity UserRole relativa all'applicazione M2FBitbucketTestPWA
            builder
                .Entity<UserRole>()
                .ToTable("UserRoles", "M2FBitbucketTestPWA")
                .HasOne<Role>(x => x.Role);

            builder
                .Entity<UserRole>()
                .HasKey(x => new { x.RoleId, x.UserId });
        }

        /// <summary>
        /// The ConfigureEntityUserRole
        /// </summary>
        /// <param name="builder">The builder<see cref="ModelBuilder"/></param>
        private void ConfigureEntityUserRole(ModelBuilder builder)
        {
            // E' necessario effettuare l'ignore dell'entity base IdentityUserRole per evitare l'errore a run-time
            // "System.Data.SqlClient.SqlException: Invalid column name 'Discriminator'"
            // Questo accade perchè l'entity Role eredita dall'entity IdentityRole
            builder.Ignore<IdentityRole>();

            // Configurazione dell'entity Role relativa all'applicazione M2FBitbucketTestPWA
            builder
                .Entity<Role>()
                .ToTable("Roles", "M2FBitbucketTestPWA")
                .HasMany<UserRole>(x => x.UserRoles);

            builder
                .Entity<Role>()
                .HasKey(x => x.Id);
        }

        #endregion
    }

    #endregion
}
