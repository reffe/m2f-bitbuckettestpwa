using Microsoft.EntityFrameworkCore;

namespace M2FBitbucketTestPWADataLayer.DbContexts
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="ApplicationsDataDbContext" />
    /// </summary>
    public class ApplicationsDataDbContext : DbContext
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationsDataDbContext"/> class.
        /// </summary>
        /// <param name="options">The options<see cref="DbContextOptions{AppDataDbContext}"/>.</param>
        public ApplicationsDataDbContext(DbContextOptions<ApplicationsDataDbContext> options)
           : base(options)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The OnModelCreating.
        /// </summary>
        /// <param name="modelBuilder">The modelBuilder<see cref="ModelBuilder"/>.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        #endregion
    }

    #endregion
}
