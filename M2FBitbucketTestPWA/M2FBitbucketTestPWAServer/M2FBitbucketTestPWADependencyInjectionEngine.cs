using Microsoft.Extensions.DependencyInjection;

namespace M2FBitbucketTestPWAServer
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="M2FBitbucketTestPWADependencyInjectionEngine" />
    /// </summary>
    internal class M2FBitbucketTestPWADependencyInjectionEngine
    {
        #region Methods

        /// <summary>
        /// Metodo utilizzato per registrare nel DI container i business logic services.
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        internal void ConfigureBusinessLogicServices(IServiceCollection services)
        {
        }

        /// <summary>
        /// Metodo utilizzato per registrare nel DI container i data layer services.
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        internal void ConfigureDataLayerServices(IServiceCollection services)
        {
        }

        /// <summary>
        /// The ConfigureOtherServices
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        internal void ConfigureOtherServices(IServiceCollection services)
        {
        }

        #endregion
    }

    #endregion
}
