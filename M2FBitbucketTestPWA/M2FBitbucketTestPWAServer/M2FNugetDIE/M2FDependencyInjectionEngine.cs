﻿using M2FDto;
using M2FDto.Settings;
using M2FLDAPSearchEngine;
using M2FUtility;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace M2FDependencyInjectionEngine
{
    /// <summary>
    /// Defines the <see cref="M2FDependencyInjectionEngine" />.
    /// </summary>
    public class M2FDependencyInjectionEngine
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="M2FDependencyInjectionEngine"/> class.
        /// </summary>
        /// <param name="configuration">The configuration<see cref="IConfiguration"/>.</param>
        /// <param name="dbContextTypes">The dbContextTypes<see cref="List{Type}"/>.</param>
        public M2FDependencyInjectionEngine(IConfiguration configuration, List<Type> dbContextTypes)
        {
            this.Configuration = configuration;
            this.DbContextTypes = dbContextTypes;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Configuration.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Gets the DbContextTypes.
        /// </summary>
        public List<Type> DbContextTypes { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Configura una singleton per il Dto <see cref="M2FDto.AppSettings"/>.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <param name="services">The services<see cref="IServiceCollection"/>.</param>
        /// <param name="appSpecificSettings">The appSpecificSettings<see cref="TAppSpecificSettings"/>.</param>
        public void ConfigureAppSettings<TAppSpecificSettings>(IServiceCollection services, TAppSpecificSettings appSpecificSettings)
            where TAppSpecificSettings : class, new()
        {
            // Configurazione dell'Options pattern.
            // Per maggiori dettagli: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/options?view=aspnetcore-3.1
            services.Configure((Action<AppSettings<TAppSpecificSettings>>)(options =>
            {
                if (appSpecificSettings != null)
                {
                    options.AppSpecificSettings = appSpecificSettings;
                }

                this.ConfigureDbConnections(options);

                this.ConfigureHttpSettings(options);

                this.ConfigureSharedFoldersSettings(options);

                this.ConfigureAx2009AifServices(options);

                this.ConfigureAuthenticationSettings(options);

                this.ConfigureNotificationSettings(options);
            }));
        }

        /// <summary>
        /// Configurazione dei DbContexts <see cref="Startup.dbContextTypes"/> e delle relative stringhe di connesione caricate dinamicamente da appSettings.json.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <param name="services">The services<see cref="IServiceCollection"/>.</param>
        /// <param name="singleton">The forService<see cref="bool"/>.</param>
        public void ConfigureEntityFramework<TAppSpecificSettings>(IServiceCollection services, bool singleton = false)
            where TAppSpecificSettings : class, new()
        {
            // Recupero dall'appSettings.Json la lista di connessioni ai Db.
            // Necessario come parametro di input per le chiamate al metodo this.ConfigureEntityFrameworkConnections(...)
            // ...
            // In questo fase dello Startup.cs non è possibile utilizzare la lista 'Connections' del DTO singleton 'M2FDto.AppSettings',
            // questo perchè nonostante il Dto 'M2FDto.AppSettings' sia già stato configurato tramite IOption pattern nel metodo this.ConfigureAppSettings()
            // la lambda 'services.Configure<AppSettings>(options => ...)' non è ancora stata eseguita.
            // Questo perchè non è ancora stata richiesta/creata una classe, registrata nel DI Container,
            // che nel costruttore abbia tra i parametri di injection 'IOptions<AppSettings> appSettings'.
            List<ConnectionSettings> dBonnections = new List<ConnectionSettings>();
            dBonnections = this.GetDbConnectionsFromAppSettingsJson<TAppSpecificSettings>();

            this.ConfigureEntityFrameworkDbContexts(services, dBonnections, singleton);
        }

        /// <summary>
        /// Configura nel DI Container i servizi standard delle applicazioni M2F.
        /// <para />
        /// Esempio:
        /// <see cref="AboutDto"/>, <see cref="LDAPSearchEngine"/>, .
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/>.</param>
        /// <param name="asmCaller">The asmCaller<see cref="Assembly"/>.</param>
        /// <param name="forWinService">The forService<see cref="bool"/>.</param>
        public void ConfigureStandardServices(IServiceCollection services, Assembly asmCaller, bool forWinService = false)
        {
            // se è per windows service non posso fare AddScoped ma Singleton
            if (forWinService)
            {
                services.AddSingleton<AboutDto>(provider => { return new AboutDto(asmCaller); });
            }
            else
            {
                services.AddScoped<AboutDto>(provider => { return new AboutDto(asmCaller); });
            }

            // Engine utilizzato per la connesione ed esecuzione di query in Active Direcotry tramite LDAP
            services.AddTransient<LDAPSearchEngine>();

            // Engine utilizzato per MailManagment per iniare mail di errore
            services.AddTransient<MailManagement>();
        }

        /// <summary>
        /// The ConfigureAuthenticationSettings.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <param name="options">The options<see cref="AppSettings"/>.</param>
        private void ConfigureAuthenticationSettings<TAppSpecificSettings>(AppSettings<TAppSpecificSettings> options)
            where TAppSpecificSettings : class, new()
        {
            options.AuthenticationSettings.LoginTypology =
                this.Configuration
                .GetSection(nameof(AuthenticationSettings))
                .GetSection(nameof(AuthenticationSettings.LoginTypology))
                .Value;
        }

        /// <summary>
        /// The ConfigureAx2009AifServices.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <param name="options">The options<see cref="AppSettings"/>.</param>
        private void ConfigureAx2009AifServices<TAppSpecificSettings>(AppSettings<TAppSpecificSettings> options)
            where TAppSpecificSettings : class, new()
        {
            // Impostazione dei parametri relativi alla sezione "Ax2009AifServicesBaseSettings" del json
            options.Ax2009AifServicesBaseSettings.BaseUrl =
                this.Configuration
                .GetSection(nameof(Ax2009AifServicesBaseSettings))
                .GetSection(nameof(Ax2009AifServicesBaseSettings.BaseUrl))
                .Value;

            // Impostazione dei servizi AIF di AX2009 presenti nel json
            var ax2009AifServicesChildren = this.Configuration.GetSection(nameof(Ax2009AifServicesSettings)).GetChildren();

            foreach (var aifServiceSection in ax2009AifServicesChildren)
            {
                Ax2009AifServicesSettings newAifService = new Ax2009AifServicesSettings();

                newAifService.BindingName = aifServiceSection.GetSection(nameof(Ax2009AifServicesSettings.BindingName)).Value;
                newAifService.ServiceName = aifServiceSection.GetSection(nameof(Ax2009AifServicesSettings.ServiceName)).Value;
                newAifService.Description = aifServiceSection.GetSection(nameof(Ax2009AifServicesSettings.Description)).Value;

                options.Ax2009AifServicesSettings.Add(newAifService);
            }
        }

        /// <summary>
        /// The ConfigureDbConnections.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <param name="options">The options<see cref="AppSettings"/>.</param>
        private void ConfigureDbConnections<TAppSpecificSettings>(AppSettings<TAppSpecificSettings> options)
            where TAppSpecificSettings : class, new()
        {
            // Impostazione delle connections strings presenti nel json
            var connectionStringsChildren = this.Configuration.GetSection(nameof(AppSettings<TAppSpecificSettings>.DatabaseConnectionsSettings)).GetChildren();

            foreach (var connSection in connectionStringsChildren)
            {
                ConnectionSettings newConnection = new ConnectionSettings();

                newConnection.CommandTimeout = Convert.ToInt32(connSection.GetSection(nameof(ConnectionSettings.CommandTimeout)).Value);
                newConnection.ConnectionName = connSection.GetSection(nameof(ConnectionSettings.ConnectionName)).Value;
                newConnection.ConnectionString = connSection.GetSection(nameof(ConnectionSettings.ConnectionString)).Value;
                newConnection.Description = connSection.GetSection(nameof(ConnectionSettings.Description)).Value;

                options.DatabaseConnectionsSettings.Add(newConnection);
            }
        }

        /// <summary>
        /// Configura il <see cref="DbContext"/> passato in input come typeparameter.
        /// Il valore di nameof(TDbContext) deve essere una cn.ConnectionName presdente tra le varie connessioni.
        /// In caso contrario dBonnections.Single(...) restiruirà un'eccezione.
        /// </summary>
        /// <typeparam name="TDbContext">DbContext che si vuole aggiungere al DI Container opportunamente configurato con i relativi parametri da appSettings.</typeparam>
        /// <param name="services">The services<see cref="IServiceCollection"/>.</param>
        /// <param name="dBonnections">The dBonnections<see cref="List{ConnectionSettings}"/>Lista di tutte le Db connections presenti nell'appSettings.json.</param>
        /// <param name="singleton">The singleton<see cref="bool"/>.</param>
        private void ConfigureEntityFrameworkConnections<TDbContext>(IServiceCollection services, List<ConnectionSettings> dBonnections, bool singleton)
            where TDbContext : DbContext
        {
            // Recupero l'oggetto ConnectionSettings per TDbContext
            var connectionSettingsForTDbContext = dBonnections.Single(cn => cn.ConnectionName.ToLower() == typeof(TDbContext).Name.ToLower());

            ServiceLifetime serviceLifetime = ServiceLifetime.Transient;

            if (singleton)
            {
                serviceLifetime = ServiceLifetime.Singleton;
            }

            // Configuro TDbContext con i parametri di connectionSettingsForTDbContext e lo aggiungo al DI Container
            services.AddDbContext<TDbContext>(
                optionsDb =>
                {
                    optionsDb.UseSqlServer(connectionSettingsForTDbContext.ConnectionString, x =>
                    {
                        // Imposto il CommandTimeout in base al valore letto da appSettings.json.
                        // Converto il numero letto in secondi.
                        // Nel file appSettings il valore di CommandTimeout deve essere espresso come numero di minuti.
                        x.CommandTimeout((int)TimeSpan.FromMinutes(connectionSettingsForTDbContext.CommandTimeout).TotalSeconds);
                    });
                }, serviceLifetime);
        }

        /// <summary>
        /// Configura nel DI Container i DbContext presenti nella lista <see cref="Startup.dbContextTypes"/>.
        /// Per ogni DbContext presente nella lista <see cref="Startup.dbContextTypes"/> deve esistere nell'appSettings.json
        /// una connessione con ConnectionName uguale al nome del DbContext.
        /// In caso contrario verrà sollevata un'eccezione.
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/>.</param>
        /// <param name="dBonnections">The dBonnections<see cref="List{ConnectionSettings}"/>.</param>
        /// <param name="singleton">The singleton<see cref="bool"/>.</param>
        private void ConfigureEntityFrameworkDbContexts(IServiceCollection services, List<ConnectionSettings> dBonnections, bool singleton)
        {
            // Array di parametri da passare in input in fase di chiamata del generic method.
            // L'ordine di inserimento dei parametri è importante perchè deve rispettare l'ordine dei parametri della firma del generic method.
            var genircMethodInputParameters = new object[] { services, dBonnections, singleton };

            // Tramite reflection cerco il generic method this.ConfigureEntityFrameworkConnections<TDbContext>(...)
            // E' un metodo private di istanza e di conseguenza devo usare BindingFlags.NonPublic | BindingFlags.Instance
            MethodInfo method =
                typeof(M2FDependencyInjectionEngine).GetMethod(
                    nameof(M2FDependencyInjectionEngine.ConfigureEntityFrameworkConnections),
                    BindingFlags.NonPublic | BindingFlags.Instance);

            // Per ogni DbContext contenuto nella lista this.DbContextTypes creo un'istanza del generic method e la eseguo
            // in modo tale da configurare il DbContext in fase di valutazione
            this.DbContextTypes.ForEach(dbctxType =>
            {
                // Creo un'istanza del generic method this.ConfigureEntityFrameworkConnections<TDbContext>(...)
                MethodInfo generic = method.MakeGenericMethod(dbctxType);

                // Eseguo il generic method passando in input l'array di parametri
                // ...
                //      generic.Invoke(this, genericMethodInputParameters)
                // esegue il metodo
                //      ConfigureEntityFrameworkConnections<TDbContext>(IServiceCollection services, List<ConnectionSettings> dBonnections)
                generic.Invoke(this, genircMethodInputParameters);
            });
        }

        /// <summary>
        /// The ConfigureHttpSettings.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <param name="options">The options<see cref="AppSettings"/>.</param>
        private void ConfigureHttpSettings<TAppSpecificSettings>(AppSettings<TAppSpecificSettings> options)
            where TAppSpecificSettings : class, new()
        {
            // Impostazione dei parametri relativi alla sezione "HttpSettings" del json.
            // HttpSettings.BaseUrl
            options.HttpSettings.BaseUrl =
                this.Configuration
                .GetSection(nameof(HttpSettings))
                .GetSection(nameof(HttpSettings.BaseUrl))
                .Value;

            // HttpSettings.HttpPort
            options.HttpSettings.HttpPort =
                Convert.ToInt32(
                    this.Configuration
                    .GetSection(nameof(HttpSettings))
                    .GetSection(nameof(HttpSettings.HttpPort))
                    .Value);

            // HttpSettings.HttpsPort
            options.HttpSettings.HttpsPort =
                Convert.ToInt32(
                    this.Configuration
                    .GetSection(nameof(HttpSettings))
                    .GetSection(nameof(HttpSettings.HttpsPort))
                    .Value);
        }

        /// <summary>
        /// The ConfigureNotificationSettings.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <param name="options">The options<see cref="AppSettings"/>.</param>
        private void ConfigureNotificationSettings<TAppSpecificSettings>(AppSettings<TAppSpecificSettings> options)
            where TAppSpecificSettings : class, new()
        {
            // Impostazione dei parametri relativi alla sezione "NotificationSettings" del json
            var notificationSettingsSection = this.Configuration.GetSection(nameof(NotificationSettings)).GetChildren();

            foreach (var notificationEventSetting in notificationSettingsSection)
            {
                NotificationEventSettings newNotificationEventSetting = new NotificationEventSettings();

                newNotificationEventSetting.EventName = notificationEventSetting.GetSection(nameof(NotificationEventSettings.EventName)).Value;
                newNotificationEventSetting.EventDescription = notificationEventSetting.GetSection(nameof(NotificationEventSettings.EventDescription)).Value;

                foreach (var item in notificationEventSetting.GetSection(nameof(NotificationEventSettings.To)).GetChildren())
                {
                    newNotificationEventSetting.To.Add(item.Value);
                }

                foreach (var item in notificationEventSetting.GetSection(nameof(NotificationEventSettings.Cc)).GetChildren())
                {
                    newNotificationEventSetting.Cc.Add(item.Value);
                }

                foreach (var item in notificationEventSetting.GetSection(nameof(NotificationEventSettings.Attachments)).GetChildren())
                {
                    newNotificationEventSetting.Attachments.Add(item.Value);
                }

                options.NotificationSettings.NotificationEventSettings.Add(newNotificationEventSetting);
            }
        }

        /// <summary>
        /// The ConfigureSharedFoldersSettings.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <param name="options">The options<see cref="AppSettings"/>.</param>
        private void ConfigureSharedFoldersSettings<TAppSpecificSettings>(AppSettings<TAppSpecificSettings> options)
            where TAppSpecificSettings : class, new()
        {
            // Impostazione dei parametri relativi alla sezione "SharedFoldersSettings" del json
            var sharedFoldersSettingsChildren = this.Configuration.GetSection(nameof(AppSettings<TAppSpecificSettings>.SharedFoldersSettings)).GetChildren();

            foreach (var sharedFolderSection in sharedFoldersSettingsChildren)
            {
                SharedFolderSettings newSharedFolder = new SharedFolderSettings();

                newSharedFolder.SharedFolderName = sharedFolderSection.GetSection(nameof(SharedFolderSettings.SharedFolderName)).Value;
                newSharedFolder.SharedFolderPath = sharedFolderSection.GetSection(nameof(SharedFolderSettings.SharedFolderPath)).Value;

                options.SharedFoldersSettings.SharedFolders.Add(newSharedFolder);
            }
        }

        /// <summary>
        /// Carica la lista delle connessioni al Db da appSettings.json.
        /// </summary>
        /// <typeparam name="TAppSpecificSettings">Typeparameter.</typeparam>
        /// <returns>The <see cref="List{ConnectionSettings}"/>.</returns>
        private List<ConnectionSettings> GetDbConnectionsFromAppSettingsJson<TAppSpecificSettings>()
            where TAppSpecificSettings : class, new()
        {
            List<ConnectionSettings> dBonnections = new List<ConnectionSettings>();

            // Impostazione delle connections strings presenti nel json
            var connectionStringsChildren = this.Configuration.GetSection(nameof(AppSettings<TAppSpecificSettings>.DatabaseConnectionsSettings)).GetChildren();

            foreach (var connSection in connectionStringsChildren)
            {
                ConnectionSettings newConnection = new ConnectionSettings();

                newConnection.CommandTimeout = Convert.ToInt32(connSection.GetSection(nameof(ConnectionSettings.CommandTimeout)).Value);
                newConnection.ConnectionName = connSection.GetSection(nameof(ConnectionSettings.ConnectionName)).Value;
                newConnection.ConnectionString = connSection.GetSection(nameof(ConnectionSettings.ConnectionString)).Value;
                newConnection.Description = connSection.GetSection(nameof(ConnectionSettings.Description)).Value;

                dBonnections.Add(newConnection);
            }

            return dBonnections;
        }

        #endregion
    }
}
