using M2FDto;
using M2FDto.Constant;
using M2FDto.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

using Serilog;

using System;
using System.Net;

namespace M2FBitbucketTestPWAServer
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="Program" />
    /// </summary>
    public class Program
    {
        #region Constants

        /// <summary>
        /// Defines the M2FCert
        /// </summary>
        private const string M2FCert = nameof(M2FCert);

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Program"/> class.
        /// </summary>
        protected Program()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the port used for Http communication.
        /// </summary>
        public static string PortHttp { get; set; }

        /// <summary>
        /// Gets or sets the port used for Https communication.
        /// </summary>
        public static string PortHttps { get; set; }

        /// <summary>
        /// Gets or sets the AppSettingsJsonName
        /// </summary>
        private static string AppSettingsJsonName { get; set; } = GeneralConstantDto.AppSettingFileName;

        #endregion

        #region Methods

        /// <summary>
        /// The CreateHostBuilder.
        /// </summary>
        /// <param name="args">The args<see cref="T:string[]"/>.</param>
        /// <returns>The <see cref="IHostBuilder"/>.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>

            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    string dotNetEnvironmentName = string.Empty;

#if !DEBUG
                    dotNetEnvironmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

                    switch (dotNetEnvironmentName)
                    {
                        case "DEV":
                        case "TEST":
                        case "PROD":
                            AppSettingsJsonName = $"appsettings.{dotNetEnvironmentName}.json";
                            break;

                        default:
                            throw new ArgumentException($"Wrong EnvironmentName: '{dotNetEnvironmentName}'");
                            break;
                    }
#endif

                    var config = new ConfigurationBuilder().AddJsonFile(AppSettingsJsonName, false, true).Build();

                    PortHttp = config.GetValue<string>($"{nameof(HttpSettings)}:{nameof(HttpSettings.HttpPort)}");
                    PortHttps = config.GetValue<string>($"{nameof(HttpSettings)}:{nameof(HttpSettings.HttpsPort)}");

                    webBuilder
                        .ConfigureAppConfiguration((context, config) =>
                        {
                            config.AddJsonFile(AppSettingsJsonName);
                        })
                        .UseSerilog((context, configuration) =>
                        {
                            configuration.ReadFrom.Configuration(context.Configuration);
                        })
                        .ConfigureKestrel(options =>
                        {
                            int port = Convert.ToInt32(PortHttps);

                            var pfxFilePath = $@"{Environment.CurrentDirectory}\{M2FCert}.pfx";

                            // The password you specified when exporting the PFX file using OpenSSL.
                            // This would normally be stored in configuration or an environment variable.
                            // I've hard-coded it here just to make it easier to see what's going on.
                            var pfxPassword = M2FCert;

                            options.Listen(IPAddress.Any, port, listenOptions =>
                            {
                                // Enable support for HTTP1 and HTTP2 (required if you want to host gRPC endpoints)
                                listenOptions.Protocols = HttpProtocols.Http1AndHttp2;

                                // Configure Kestrel to use a certificate from a local .PFX file for hosting HTTPS
                                listenOptions.UseHttps(pfxFilePath, pfxPassword);
                            });

                            options.ConfigureHttpsDefaults(o => o.ClientCertificateMode = ClientCertificateMode.RequireCertificate);
                        })
                        .UseStartup<Startup>()
                        .UseConfiguration(config)
                        .UseUrls(
                            "https://0.0.0.0:" + PortHttps,
                            "http://0.0.0.0:" + PortHttp);
                });

        /// <summary>
        /// The Main
        /// </summary>
        /// <param name="args">The args<see cref="T:string[]"/></param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

#endregion
    }

#endregion
}
