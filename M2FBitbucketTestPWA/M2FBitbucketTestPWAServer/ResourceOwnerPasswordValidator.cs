using IdentityServer4.Models;
using IdentityServer4.Validation;

using M2FBitbucketTestPWABusinessLogicContracts.Authentication;

using System.Threading.Tasks;

namespace M2FBitbucketTestPWAServer
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="ResourceOwnerPasswordValidator" />
    /// </summary>
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        #region Fields

        /// <summary>
        /// Defines the authenticationBl
        /// </summary>
        private readonly IAuthenticationBl authenticationBl;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceOwnerPasswordValidator"/> class.
        /// </summary>
        /// <param name="authenticationBl">The authenticationRepository<see cref="IAuthenticationBl"/></param>
        public ResourceOwnerPasswordValidator(IAuthenticationBl authenticationBl)
        {
            this.authenticationBl = authenticationBl;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The ValidateAsync
        /// </summary>
        /// <param name="context">The context<see cref="ResourceOwnerPasswordValidationContext"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            if (this.authenticationBl.ValidatePassword(context.UserName, context.Password))
            {
                context.Result = new GrantValidationResult(this.authenticationBl.GetUserByUsername(context.UserName).Id, "password", null, "local", null);

                return Task.FromResult(context.Result);
            }

            context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "The username or password are wrong.", null);

            return Task.FromResult(context.Result);
        }

        #endregion
    }

    #endregion
}
