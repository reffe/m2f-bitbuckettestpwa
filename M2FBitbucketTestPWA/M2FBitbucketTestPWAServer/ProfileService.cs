using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;

using M2FDto;
using M2FDto.About;
using M2FDto.Constant;
using M2FBitbucketTestPWABusinessLogicContracts.Authentication;

using M2FBitbucketTestPWADto;

using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

using Serilog;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAServer
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="ProfileService" />
    /// </summary>
    public class ProfileService : IProfileService
    {
        #region Fields

        /// <summary>
        /// Defines the aboutInfo
        /// </summary>
        private readonly AboutDto aboutInfo;

        /// <summary>
        /// Defines the appSettings
        /// </summary>
        private readonly IOptions<AppSettings<AppSpecificSettings>> appSettings;

        /// <summary>
        /// Defines the authenticationBl
        /// </summary>
        private readonly IAuthenticationBl authenticationBl;

        /// <summary>
        /// Defines the claimsFactory
        /// </summary>
        private readonly IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory;

        /// <summary>
        /// Defines the logger
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Defines the userManager
        /// </summary>
        private readonly UserManager<ApplicationUser> userManager;

        /// <summary>
        /// Defines the httpContextAccessor
        /// </summary>
        private readonly IHttpContextAccessor httpContextAccessor;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileService"/> class.
        /// </summary>
        /// <param name="appSettings">The appSettings<see cref="IOptions{AppSettings{AppSpecificSettings}}"/></param>
        /// <param name="logger">The logger<see cref="ILogger"/></param>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/></param>
        /// <param name="userManager">The userManager<see cref="UserManager{ApplicationUser}"/></param>
        /// <param name="claimsFactory">The claimsFactory<see cref="IUserClaimsPrincipalFactory{ApplicationUser}"/></param>
        /// <param name="authenticationBl">The authenticationRepository<see cref="IAuthenticationBl"/></param>
        /// <param name="httpContextAccessor">The httpContextAccessor<see cref="IHttpContextAccessor"/></param>
        public ProfileService(
            IOptions<AppSettings<AppSpecificSettings>> appSettings,
            ILogger logger,
            AboutDto aboutInfo,
            UserManager<ApplicationUser> userManager,
            IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory,
            IAuthenticationBl authenticationBl,
            IHttpContextAccessor httpContextAccessor)
        {
            this.appSettings = appSettings;
            this.logger = logger;
            this.aboutInfo = aboutInfo;
            this.userManager = userManager;
            this.claimsFactory = claimsFactory;
            this.authenticationBl = authenticationBl;
            this.httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The GetProfileDataAsync
        /// </summary>
        /// <param name="context">The context<see cref="ProfileDataRequestContext"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();

            ////ApplicationUser user = await this.userManager.FindByIdAsync(sub);
            ApplicationUser user = this.authenticationBl.GetUserById(sub);

            ClaimsPrincipal principal = await this.claimsFactory.CreateAsync(user);

            List<Claim> claims = principal.Claims.ToList();
            claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();

            this.AddCustomUserClaims(user, claims);

            this.AddRoleClaimsFromIdentityServerDb(user, claims);

            this.AddUserInfoClaimsFromActiveDirectory(user, claims);

            this.AddConnectionInfoClaims(claims);

            this.AddEnvironmentInfoClaims(claims);

            context.IssuedClaims = claims;
        }

        /// <summary>
        /// The IsActiveAsync
        /// </summary>
        /// <param name="context">The context<see cref="IsActiveContext"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();

            ////var ApplicationUser = await this.userManager.FindByIdAsync(sub);
            ApplicationUser user = this.authenticationBl.GetUserById(sub);

            context.IsActive = user != null;
        }

        /// <summary>
        /// The AddCustomUserClaims
        /// </summary>
        /// <param name="user">The user<see cref="ApplicationUser"/></param>
        /// <param name="claims">The claims<see cref="List{Claim}"/></param>
        private void AddCustomUserClaims(ApplicationUser user, List<Claim> claims)
        {
            // Add custom claims in token here based on user properties or any other source
            //
            // Aggiungo la DallasKey tra i Claims (...attestazioni...) dell'utente loggato.
            // Il valore della DallasKey verrà caricato dalla tabella [dbo].[AspeNetUsers] del database IdentityDB.
            // Non tutti gli utenti mappati nella tabella [dbo].[AspeNetUsers] hanno il campo DallaKey valorizzato, in questo caso si tratterà o di
            // utenze tecniche oppure di utenze create appositamente per favorire l'accesso a fornitori esterni.
            claims.Add(new Claim(IdentityServerConstantDto.ClaimsDallasKey, !string.IsNullOrWhiteSpace(user.DallasKey) ? user.DallasKey : string.Empty));

            // Aggiungo il Claim relativo alla proprietà EmplId
            claims.Add(new Claim(IdentityServerConstantDto.ClaimsEmplId, !string.IsNullOrWhiteSpace(user.EmplId) ? user.EmplId : string.Empty));

            // Aggiungo il Claim relativo alla proprietà IsDomainUser
            claims.Add(new Claim(nameof(ApplicationUser.IsDomainUser), Convert.ToString(Convert.ToInt16(user.IsDomainUser))));

            // Aggiungo il Claim relativo alla proprietà IsTechnicalUser
            claims.Add(new Claim(nameof(ApplicationUser.IsTechnicalUser), Convert.ToString(Convert.ToInt16(user.IsTechnicalUser))));
        }

        /// <summary>
        /// The AddRoleClaimsFromIdentityServerDb
        /// </summary>
        /// <param name="user">The user<see cref="ApplicationUser"/></param>
        /// <param name="claims">The claims<see cref="List{Claim}"/></param>
        private void AddRoleClaimsFromIdentityServerDb(ApplicationUser user, List<Claim> claims)
        {
            foreach (var r in this.authenticationBl.GetRolesByUserId(user.Id))
            {
                claims.Add(new Claim(IdentityServerConstantDto.ClaimsRole, r.Name));

                var roleInfo = new RoleInfoDto();
                roleInfo.RoleName = r.Name;

                this.aboutInfo.LoggedUserInformation.Roles.Add(roleInfo);
            }

            // Insieme di tutti i ruoli di Identity Server
            string roleNames = string.Empty;

            // Indica se è la prima volta nel ciclo for
            bool firstTime = true;

            foreach (var r in this.authenticationBl.GetRolesByUserId(user.Id))
            {
                // Se non è la prima volta gli aggiungo la virgola come separatore
                if (!firstTime)
                {
                    roleNames += ",";
                }

                roleNames += r.Name;
                firstTime = false;

                var roleInfo = new RoleInfoDto();
                roleInfo.RoleName = r.Name;

                this.aboutInfo.LoggedUserInformation.Roles.Add(roleInfo);
            }

            if (!string.IsNullOrWhiteSpace(roleNames))
            {
                claims.Add(new Claim(IdentityServerConstantDto.ClaimsIdentityServerRole, roleNames));
            }
        }

        /// <summary>
        /// The AddClaimsFromActiveDirectory
        /// </summary>
        /// <param name="user">The user<see cref="ApplicationUser"/></param>
        /// <param name="claims">The claims<see cref="List{Claim}"/></param>
        private void AddUserInfoClaimsFromActiveDirectory(ApplicationUser user, List<Claim> claims)
        {
            if (user.IsDomainUser)
            {
                var ldapSearchEngine = new M2FLDAPSearchEngine.LDAPSearchEngine(this.aboutInfo);
                ldapSearchEngine.GetUserInformationFromActiveDirectory(user.UserName);

                // Aggiungo le informazioni utente recuperate da LDAP
                this.AddCustomUserClaimsFromActiveDirectory(claims);

                // Aggiungo le informazioni dei ruoli memberof recuperate da LDAP
                this.AddRoleClaimsFromActiveDirectory(claims);
            }
        }

        /// <summary>
        /// The AddCustomUserClaimsFromActiveDirectory
        /// </summary>
        /// <param name="claims">The claims<see cref="List{Claim}"/></param>
        private void AddCustomUserClaimsFromActiveDirectory(List<Claim> claims)
        {
            // Add custom claims in token here based on user properties or any other source
            //
            // Aggiungo le proprietà prelevate da active directory tra i Claims (...attestazioni...) dell'utente loggato.
            // Non tutti gli utenti mappati nella tabella [dbo].[AspeNetUsers] saranno user domain, in questo caso non saranno valorizzati.
            claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutInfo.LoggedUserInformation.DistinguishedName), !string.IsNullOrWhiteSpace(this.aboutInfo.LoggedUserInformation.DistinguishedName) ? this.aboutInfo.LoggedUserInformation.DistinguishedName : string.Empty));

            claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutInfo.LoggedUserInformation.GivenName), !string.IsNullOrWhiteSpace(this.aboutInfo.LoggedUserInformation.GivenName) ? this.aboutInfo.LoggedUserInformation.GivenName : string.Empty));

            claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutInfo.LoggedUserInformation.Mail), !string.IsNullOrWhiteSpace(this.aboutInfo.LoggedUserInformation.Mail) ? this.aboutInfo.LoggedUserInformation.Mail : string.Empty));

            claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutInfo.LoggedUserInformation.Name), !string.IsNullOrWhiteSpace(this.aboutInfo.LoggedUserInformation.Name) ? this.aboutInfo.LoggedUserInformation.Name : string.Empty));

            claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutInfo.LoggedUserInformation.SAMAccountName), !string.IsNullOrWhiteSpace(this.aboutInfo.LoggedUserInformation.SAMAccountName) ? this.aboutInfo.LoggedUserInformation.SAMAccountName : string.Empty));

            claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutInfo.LoggedUserInformation.Sn), !string.IsNullOrWhiteSpace(this.aboutInfo.LoggedUserInformation.Sn) ? this.aboutInfo.LoggedUserInformation.Sn : string.Empty));

            claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixLDAP + nameof(this.aboutInfo.LoggedUserInformation.UserPrincipalName), !string.IsNullOrWhiteSpace(this.aboutInfo.LoggedUserInformation.UserPrincipalName) ? this.aboutInfo.LoggedUserInformation.UserPrincipalName : string.Empty));
        }

        /// <summary>
        /// The AddRoleClaimsFromActiveDirectory
        /// </summary>
        /// <param name="claims">The claims<see cref="List{Claim}"/></param>
        private void AddRoleClaimsFromActiveDirectory(List<Claim> claims)
        {
            foreach (var item in this.aboutInfo.LoggedUserInformation.MemberOf)
            {
                claims.Add(new Claim(IdentityServerConstantDto.ClaimsRole, item.GroupName));
            }
        }

        /// <summary>
        /// The AddConnectionInfoClaims
        /// </summary>
        /// <param name="claims">The claims<see cref="List{Claim}"/></param>
        private void AddConnectionInfoClaims(List<Claim> claims)
        {
            // Add custom claims in token here based on user properties or any other source
            //
            // Aggiungo le proprietà prelevate dalla connection info tra i Claims (...attestazioni...) dell'utente loggato.
            if (this.httpContextAccessor.HttpContext != null)
            {
                // Valorizzo i dati del httpContextAccessor
                this.aboutInfo.HttpContextConnectionInformation.ClientCertificate = this.httpContextAccessor.HttpContext.Connection.ClientCertificate?.ToString();
                this.aboutInfo.HttpContextConnectionInformation.Id = this.httpContextAccessor.HttpContext.Connection.Id;
                this.aboutInfo.HttpContextConnectionInformation.LocalIpAddress = this.httpContextAccessor.HttpContext.Connection.LocalIpAddress.ToString();
                this.aboutInfo.HttpContextConnectionInformation.LocalPort = this.httpContextAccessor.HttpContext.Connection.LocalPort;
                this.aboutInfo.HttpContextConnectionInformation.RemoteIpAddress = this.httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                this.aboutInfo.HttpContextConnectionInformation.RemotePort = this.httpContextAccessor.HttpContext.Connection.RemotePort;

                // ATTENZIONE Inverto il locale con il remoto per il client
                claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixConnectionInfo + nameof(this.aboutInfo.HttpContextConnectionInformation.RemoteIpAddress), !string.IsNullOrWhiteSpace(this.aboutInfo.HttpContextConnectionInformation.LocalIpAddress) ? this.aboutInfo.HttpContextConnectionInformation.LocalIpAddress : string.Empty));

                claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixConnectionInfo + nameof(this.aboutInfo.HttpContextConnectionInformation.RemotePort), !string.IsNullOrWhiteSpace(this.aboutInfo.HttpContextConnectionInformation.LocalPort.ToString()) ? this.aboutInfo.HttpContextConnectionInformation.LocalPort.ToString() : string.Empty));

                claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixConnectionInfo + nameof(this.aboutInfo.HttpContextConnectionInformation.LocalIpAddress), !string.IsNullOrWhiteSpace(this.aboutInfo.HttpContextConnectionInformation.RemoteIpAddress) ? this.aboutInfo.HttpContextConnectionInformation.RemoteIpAddress : string.Empty));

                claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixConnectionInfo + nameof(this.aboutInfo.HttpContextConnectionInformation.LocalPort), !string.IsNullOrWhiteSpace(this.aboutInfo.HttpContextConnectionInformation.RemotePort.ToString()) ? this.aboutInfo.HttpContextConnectionInformation.RemotePort.ToString() : string.Empty));
            }
        }

        /// <summary>
        /// The AddEnvironmentInfoClaims
        /// </summary>
        /// <param name="claims">The claims<see cref="List{Claim}"/></param>
        private void AddEnvironmentInfoClaims(List<Claim> claims)
        {
            // Add custom claims in token here based on user properties or any other source
            //
            // Aggiungo le proprietà prelevate dalla connection info tra i Claims (...attestazioni...) dell'utente loggato.
            claims.Add(new Claim(IdentityServerConstantDto.ClaimsPrefixEnvInfo + nameof(this.aboutInfo.EnvironmentInformation.MachineName), !string.IsNullOrWhiteSpace(this.aboutInfo.EnvironmentInformation.MachineName) ? this.aboutInfo.EnvironmentInformation.MachineName : string.Empty));
        }

        #endregion
    }

    #endregion
}
