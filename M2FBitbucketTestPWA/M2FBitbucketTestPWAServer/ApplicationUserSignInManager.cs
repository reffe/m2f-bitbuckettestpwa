using M2FBitbucketTestPWADataLayer.DbContexts.IdentityServer.Custom;
using M2FBitbucketTestPWADataLayer.DbContexts.IdentityServer.Standard;

using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;
using System.Threading.Tasks;

namespace M2FBitbucketTestPWAServer
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="ApplicationUserSignInManager" />
    /// </summary>
    public class ApplicationUserSignInManager : SignInManager<ApplicationUser>
    {
        #region Fields

        /// <summary>
        /// Defines the httpContextAccessor
        /// </summary>
        private readonly IHttpContextAccessor httpContextAccessor;

        /// <summary>
        /// Defines the identityServerCustomDbCtx
        /// </summary>
        private readonly IdentityServerCustomDbContext identityServerCustomDbCtx;

        /// <summary>
        /// Defines the identityServerStandardDbCtx
        /// </summary>
        private readonly IdentityServerStandardDbContext identityServerStandardDbCtx;

        /// <summary>
        /// Defines the userManager
        /// </summary>
        private readonly UserManager<ApplicationUser> userManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationUserSignInManager"/> class.
        /// </summary>
        /// <param name="userManager">The userManager<see cref="UserManager{ApplicationUser}"/></param>
        /// <param name="contextAccessor">The contextAccessor<see cref="IHttpContextAccessor"/></param>
        /// <param name="claimsFactory">The claimsFactory<see cref="IUserClaimsPrincipalFactory{ApplicationUser}"/></param>
        /// <param name="optionsAccessor">The optionsAccessor<see cref="IOptions{IdentityOptions}"/></param>
        /// <param name="logger">The logger<see cref="ILogger{SignInManager{ApplicationUser}}"/></param>
        /// <param name="identityServerCustomDbCtx">The identityServerCustomDbCtx<see cref="IdentityServerCustomDbContext"/></param>
        /// <param name="identityServerStandardDbCtx">The identityServerStandardDbCtx<see cref="IdentityServerStandardDbContext"/></param>
        /// <param name="schemeProvider">The schemeProvider<see cref="IAuthenticationSchemeProvider"/></param>
        public ApplicationUserSignInManager(
            UserManager<ApplicationUser> userManager,
            IHttpContextAccessor contextAccessor,
            IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory,
            IOptions<IdentityOptions> optionsAccessor,
            ILogger<SignInManager<ApplicationUser>> logger,
            IdentityServerCustomDbContext identityServerCustomDbCtx,
            IdentityServerStandardDbContext identityServerStandardDbCtx,
            IAuthenticationSchemeProvider schemeProvider)
            : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemeProvider, null)
        {
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            this.httpContextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(contextAccessor));
            this.identityServerCustomDbCtx = identityServerCustomDbCtx ?? throw new ArgumentNullException(nameof(identityServerCustomDbCtx));
            this.identityServerStandardDbCtx = identityServerStandardDbCtx ?? throw new ArgumentNullException(nameof(identityServerStandardDbCtx));
        }

        #endregion

        #region Methods

        /// <summary>
        /// The PasswordSignInAsync
        /// </summary>
        /// <param name="userName">The userName<see cref="string"/></param>
        /// <param name="password">The password<see cref="string"/></param>
        /// <param name="isPersistent">The isPersistent<see cref="bool"/></param>
        /// <param name="lockoutOnFailure">The lockoutOnFailure<see cref="bool"/></param>
        /// <returns>The <see cref="Task{SignInResult}"/></returns>
        public override Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
        {
            return base.PasswordSignInAsync(userName, password, isPersistent, lockoutOnFailure);
        }

        #endregion
    }

    #endregion
}
