using M2FDto;
using M2FDto.Constant;
using M2FBitbucketTestPWADto;

using M2FBitbucketTestPWAServer.Areas.Identity.Pages.Account;

using System.ComponentModel.DataAnnotations;

namespace M2FBitbucketTestPWAServer.Attributes
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="RequiredIfAttribute" />
    /// </summary>
    public class RequiredIfAttribute : ValidationAttribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RequiredIfAttribute"/> class.
        /// </summary>
        /// <param name="propertyName">The propertyName<see cref="string"/></param>
        /// <param name="errorMessage">The errorMessage<see cref="string"/></param>
        public RequiredIfAttribute(string propertyName, string errorMessage = "")
        {
            this.PropertyName = propertyName;
            this.ErrorMessage = errorMessage;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the PropertyName
        /// </summary>
        public string PropertyName { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The IsValid
        /// </summary>
        /// <param name="value">The value<see cref="object"/></param>
        /// <param name="validationContext">The validationContext<see cref="ValidationContext"/></param>
        /// <returns>The <see cref="ValidationResult"/></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var instance = validationContext.ObjectInstance;
            var type = instance.GetType();

            // Ricavo il valore della proprietà AppSettings definita nella classe InputModel
            var appSettingsFieldValue = (AppSettings<AppSpecificSettings>)type.GetProperty("appSettings").GetValue(instance);

            // Ricavo il valore della proprietà in fase di validazione, cioè la proprietà che ha l'attributo [Required]
            string proprtyvalue = (string)type.GetProperty(this.PropertyName).GetValue(instance, null);

            // Se il tipo di autenticazione NON è un tipo di autenticazione Standard (Form-Based => PIN)
            // Allora devo controllare che:
            // - il campo PIN sia correttamente valorizzato
            // - il campo USERNAME può essere null / empty
            // - il campo PASSWORD può essere null / empty
            if (string.Compare(appSettingsFieldValue.AuthenticationSettings.LoginTypology, IdentityServerConstantDto.LogInTypePin, true) == 0 &&
                this.PropertyName.ToUpper().Equals(nameof(LoginModel.InputModel.Pin).ToUpper()) &&
                string.IsNullOrWhiteSpace(proprtyvalue))
            {
                return new ValidationResult(this.ErrorMessage);
            }

            // Se il tipo di autenticazione è un tipo di autenticazione Standard (Form-Based => Username + Password)
            // Allora devo controllare che:
            // - il campo PIN può essere null / empty
            // - il campo USERNAME sia correttamente valorizzato
            // - il campo PASSWORD sia correttamente valorizzato
            if (string.Compare(appSettingsFieldValue.AuthenticationSettings.LoginTypology, IdentityServerConstantDto.LogInTypeStandard, true) == 0 &&
                (
                    this.PropertyName.ToUpper().Equals(nameof(LoginModel.InputModel.Username).ToUpper()) ||
                    this.PropertyName.ToUpper().Equals(nameof(LoginModel.InputModel.Password).ToUpper())) &&
                string.IsNullOrWhiteSpace(proprtyvalue))
            {
                return new ValidationResult(this.ErrorMessage);
            }

            return ValidationResult.Success;
        }

        #endregion
    }

    #endregion
}
