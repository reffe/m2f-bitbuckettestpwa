using IdentityServer4.Services;
using IdentityServer4.Validation;

using M2FControllers.Middlewares;

using M2FDto;
using M2FDto.Constant;
using M2FDto.Settings;
using M2FBitbucketTestPWABusinessLogic.Authentication;

using M2FBitbucketTestPWABusinessLogicContracts.Authentication;

using M2FBitbucketTestPWADataLayer.Authentication;
using M2FBitbucketTestPWADataLayer.DbContexts;
using M2FBitbucketTestPWADataLayer.DbContexts.IdentityServer.Custom;
using M2FBitbucketTestPWADataLayer.DbContexts.IdentityServer.Standard;

using M2FBitbucketTestPWADataLayerContracts.Authentication;

using M2FBitbucketTestPWADto;

using M2FBitbucketTestPWAEntities.Entities.IdentityServer.Standard;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Certificate;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace M2FBitbucketTestPWAServer
{
    #region Classes

    /// <summary>
    /// Defines the <see cref="Startup" />
    /// </summary>
    public class Startup
    {
        #region Constants

        /// <summary>
        /// Defines the M2FCert
        /// </summary>
        private const string M2FCert = nameof(M2FCert);

        #endregion

        #region Fields

        /// <summary>
        /// Defines the dbContextTypes
        /// </summary>
        private readonly List<Type> dbContextTypes = new List<Type>()
        {
            typeof(AxDbContext),
            typeof(ApplicationsDataDbContext),
        };

        /// <summary>
        /// Defines the m2FDependencyInjectionEngine
        /// </summary>
        private readonly M2FDependencyInjectionEngine.M2FDependencyInjectionEngine m2FDependencyInjectionEngine;

        /// <summary>
        /// Defines the m2FBitbucketTestPWADependencyInjectionEngine
        /// </summary>
        private readonly M2FBitbucketTestPWADependencyInjectionEngine m2FBitbucketTestPWADependencyInjectionEngine;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration<see cref="IConfiguration"/></param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;

            // Creo un'istanza della classe M2FDependencyInjectionEngine
            // Questa classe � utilizzata per effettuare la configurazione di diversi servizi.
            this.m2FDependencyInjectionEngine = new M2FDependencyInjectionEngine.M2FDependencyInjectionEngine(this.Configuration, this.dbContextTypes);

            this.m2FBitbucketTestPWADependencyInjectionEngine = new M2FBitbucketTestPWADependencyInjectionEngine();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The Configure
        /// </summary>
        /// <param name="app">The app<see cref="IApplicationBuilder"/></param>
        /// <param name="env">The env<see cref="IWebHostEnvironment"/></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // L'istruzione sottostante � stata commentata in quanto va in conflitto con app.UseExceptionHandler("/Error")
                //
                ////app.UseDeveloperExceptionPage();

                app.UseWebAssemblyDebugging();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "M2FBitbucketTestPWAServer v1"));
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // When an unhandled exception occurs, it will redirect to a controller action method decorated with the Route[�/error�] attribute
            // This exception handling works with code throwed exception (not with 401, 404 and others excpetion related to permission or resources)
            app.UseExceptionHandler(GeneralConstantDto.ErrorPage);

            //// app.UseHttpsRedirection();

            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();

            // Middleware to log request and response
            app.UseMiddleware<RequestResponseLoggingMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
        }

        /// <summary>
        /// The ConfigureServices
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "M2FBitbucketTestPWAServer", Version = "v1" });
            });

            this.ConfigureIdentityServerDbContextxs(services);

            services
                .AddDefaultIdentity<ApplicationUser>(options =>
                {
                    options.SignIn.RequireConfirmedAccount = false;
                    options.SignIn.RequireConfirmedEmail = false;
                    options.SignIn.RequireConfirmedPhoneNumber = false;
                    options.Lockout.AllowedForNewUsers = false;
                })
                .AddRoles<IdentityRole>()
                .AddSignInManager<ApplicationUserSignInManager>()
                .AddEntityFrameworkStores<IdentityServerStandardDbContext>();

            this.ConfigureAuthenticationAndIdentityServer(services, false);

            services.AddControllersWithViews();
            services.AddRazorPages();

            // Configurazione dei servizi:
            // - standard per le applicazioni M2F
            // - specifici di questa applicazione
            this.CustomConfiguration(services);
        }

        /// <summary>
        /// Metodo utilizzato per caricare dall'appsettings.json i parametri con tenuti nella sezione <see cref="AppSpecificSettings"/>
        /// </summary>
        /// <returns>Restituisce un'istanza configurata della classe <see cref="AppSpecificSettings"/></returns>
        private AppSpecificSettings ConfigureAppSpecificSettings()
        {
            var appSpecificSettings = new AppSpecificSettings();

            ////appSpecificSettings.RicardoEnvironmentLabel =
            ////    this.Configuration
            ////    .GetSection(nameof(AppSpecificSettings))
            ////    .GetSection(nameof(AppSpecificSettings.RicardoEnvironmentLabel))
            ////    .Value;

            ////appSpecificSettings.NumberOfElementsPerMessage =
            ////    Convert.ToInt32(
            ////        this.Configuration
            ////        .GetSection(nameof(AppSpecificSettings))
            ////        .GetSection(nameof(AppSpecificSettings.NumberOfElementsPerMessage))
            ////        .Value);

            return appSpecificSettings;
        }

        /// <summary>
        /// Metodo utilizzato per configurare Authentication ed IdentityServerJwt.
        /// Tramite il parametro "addCertificate" � anche possibile scegliere se utilizzare il certificato oppure no.
        /// Se si sceglie di utilizzare il certificato allora saranno necessarie delle configurazioni aggiuntive per i componenti
        /// <see cref="System.Net.Http.HttpClient"/> utilizzati per le chiamate ai controller Api esposti dal backend.
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        /// <param name="addCertificate">
        /// <para />
        /// Se addCertificate = false non verr� richiesto il certificato per le chiamate effettuate da HttpClient verso i controller Api esposti dal backend.
        /// <para />
        /// Se addCertificate = true per i componenti HttpClient che effettuano le chiamate ai controller Api esposti dal backend
        /// sar� necessario configurare ed utilizzare il certificato, altrimenti le chiamate effettuate andranno in 401 Unauthorized.
        /// </param>
        private void ConfigureAuthenticationAndIdentityServer(IServiceCollection services, bool addCertificate)
        {
            X509Certificate2 crt = this.LoadM2FCertificate();

            if (!addCertificate)
            {
                services
                .AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, IdentityServerStandardDbContext>(options =>
                {
                    options.IdentityResources["openid"].UserClaims.Add("name");
                    options.ApiResources.Single().UserClaims.Add("name");

                    options.IdentityResources["openid"].UserClaims.Add("role");
                    options.ApiResources.Single().UserClaims.Add("role");
                });

                services
                    .AddAuthentication()
                    .AddIdentityServerJwt();
            }
            else
            {
                services
                .AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, IdentityServerStandardDbContext>(options =>
                {
                    options.IdentityResources["openid"].UserClaims.Add("name");
                    options.ApiResources.Single().UserClaims.Add("name");

                    options.IdentityResources["openid"].UserClaims.Add("role");
                    options.ApiResources.Single().UserClaims.Add("role");
                })
                .AddSigningCredential(crt);

                services
                    .AddAuthentication(CertificateAuthenticationDefaults.AuthenticationScheme)
                    .AddCertificate()

                    // Adding an ICertificateValidationCache results in certificate auth caching the results.
                    // The default implementation uses a memory cache.
                    .AddCertificateCache()
                    .AddIdentityServerJwt();
            }

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("role");

            services
                .AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>()
                .AddTransient<IProfileService, ProfileService>();

            services
                .AddTransient<IAuthenticationBl, AuthenticationBl>()
                .AddTransient<IAuthenticationDl, AuthenticationDl>();
        }

        /// <summary>
        /// The ConfigureIdentityServerDbContextxs
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        private void ConfigureIdentityServerDbContextxs(IServiceCollection services)
        {
            List<ConnectionSettings> dbConnections = new List<ConnectionSettings>();

            var connectionStringsChildren = this.Configuration.GetSection(nameof(AppSettings<AppSpecificSettings>.DatabaseConnectionsSettings)).GetChildren();

            foreach (var connSection in connectionStringsChildren)
            {
                ConnectionSettings newConnection = new ConnectionSettings();

                newConnection.CommandTimeout = Convert.ToInt32(connSection.GetSection(nameof(ConnectionSettings.CommandTimeout)).Value);
                newConnection.ConnectionName = connSection.GetSection(nameof(ConnectionSettings.ConnectionName)).Value;
                newConnection.ConnectionString = connSection.GetSection(nameof(ConnectionSettings.ConnectionString)).Value;
                newConnection.Description = connSection.GetSection(nameof(ConnectionSettings.Description)).Value;

                dbConnections.Add(newConnection);
            }

            // Seleziono la connessione relativa al dbContext "IdentityServerStandardDbContext"
            var identityServerStandardDbCopnnection = dbConnections.FirstOrDefault(x => x.ConnectionName.Equals(nameof(IdentityServerStandardDbContext)));

            services.AddDbContext<IdentityServerStandardDbContext>(
                optionsDb =>
                {
                    optionsDb.UseSqlServer(identityServerStandardDbCopnnection.ConnectionString, x =>
                    {
                        // Imposto il CommandTimeout in base al valore letto da appSettings.json.
                        // Converto il numero letto in secondi.
                        // Nel file appSettings il valore di CommandTimeout deve essere espresso come numero di minuti.
                        x.CommandTimeout((int)TimeSpan.FromMinutes(identityServerStandardDbCopnnection.CommandTimeout).TotalSeconds);
                    });
                }, ServiceLifetime.Transient);

            // Seleziono la connessione relativa al dbContext "IdentityServerCustomDbContext"
            var identityServerCustomDbCopnnection = dbConnections.FirstOrDefault(x => x.ConnectionName.Equals(nameof(IdentityServerCustomDbContext)));

            services.AddDbContext<IdentityServerCustomDbContext>(
                optionsDb =>
                {
                    optionsDb.UseSqlServer(identityServerCustomDbCopnnection.ConnectionString, x =>
                    {
                        // Imposto il CommandTimeout in base al valore letto da appSettings.json.
                        // Converto il numero letto in secondi.
                        // Nel file appSettings il valore di CommandTimeout deve essere espresso come numero di minuti.
                        x.CommandTimeout((int)TimeSpan.FromMinutes(identityServerCustomDbCopnnection.CommandTimeout).TotalSeconds);
                    });
                }, ServiceLifetime.Transient);
        }

        /// <summary>
        /// Metodo utilizzato per configurare: <para />
        /// - servizi standard per le applicazioni M2F <para />
        /// - servizi custom di questa applicazione
        /// </summary>
        /// <param name="services">The services<see cref="IServiceCollection"/></param>
        private void CustomConfiguration(IServiceCollection services)
        {
            // Creo un'stanza della classe AppSpecificSettings configurata con i relativi parametri caricati dal file appsettings.json
            AppSpecificSettings appSpecificSettings = this.ConfigureAppSpecificSettings();

            // Configuro:
            //
            // - la classe AppSettings caricando i parametri dal file appsettings.json
            // - la propriet� AppSettings.AppSpecificSettings verr� valorizzata con l'istanza della classe AppSpecificSettings precedentemente configurata
            // - il IOptions pattern: IOptions<AppSettings>
            this.m2FDependencyInjectionEngine.ConfigureAppSettings<AppSpecificSettings>(services, appSpecificSettings);

            // Configuro Entity Framework e carico le varie stringhe di connessione
            this.m2FDependencyInjectionEngine.ConfigureEntityFramework<AppSpecificSettings>(services);

            // Configuro i servizi standard per le applicazioni M2F:
            // - AuthenticationStateProvider
            // - AppSettingsManager
            // - AboutDto
            // - LDAPSearchEngine
            // - MailManagement
            this.m2FDependencyInjectionEngine.ConfigureStandardServices(services, Assembly.GetExecutingAssembly());

            // Add Business Logic services
            this.m2FBitbucketTestPWADependencyInjectionEngine.ConfigureBusinessLogicServices(services);

            // Add Data Layer services
            this.m2FBitbucketTestPWADependencyInjectionEngine.ConfigureDataLayerServices(services);

            // Add other services, mandatory for the application life-cycle
            this.m2FBitbucketTestPWADependencyInjectionEngine.ConfigureOtherServices(services);
        }

        /// <summary>
        /// The LoadM2FCertificate
        /// </summary>
        /// <returns>The <see cref="X509Certificate2"/></returns>
        private X509Certificate2 LoadM2FCertificate()
        {
            string pfxFilePath = $@"{Environment.CurrentDirectory}\{M2FCert}.pfx";

            // The password you specified when exporting the PFX file using OpenSSL.
            // This would normally be stored in configuration or an environment variable.
            // I've hard-coded it here just to make it easier to see what's going on.
            string pfxPassword = M2FCert;

            X509Certificate2 m2fCertificatePfx = new X509Certificate2(pfxFilePath, pfxPassword);

            return m2fCertificatePfx;
        }

        #endregion
    }

    #endregion
}
