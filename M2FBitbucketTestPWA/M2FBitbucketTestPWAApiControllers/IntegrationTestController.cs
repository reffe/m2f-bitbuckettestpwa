using M2FBitbucketTestPWADto;
using M2FBitbucketTestPWADto.IntegrationTest;
using M2FCommunicationContracts;
using M2FControllers;
using M2FDto;
using M2FDto.Constant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MrlM2fIntegrationTest;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Xml;
using System.Xml.Serialization;

namespace M2FBitbucketTestPWAApiControllers
{
    /// <summary>
    /// Defines the <see cref="IntegrationTestController" />.
    /// </summary>
    [Authorize(Roles = IdentityServerConstantDto.AuthorizationRoleAdmin)]
    public partial class IntegrationTestController : BaseController
    {
        #region Constants

        /// <summary>
        /// Defines the TestFileMessage.
        /// </summary>
        private const string TestFileMessage = "Congratulations! The integration test file has successfully been created!";

        /// <summary>
        /// Defines the TestFileMessageAfterAifService.
        /// </summary>
        private const string TestFileMessageAfterAifService = "Congratulations! The integration test file has successfully been created! Message returned by called aif service: ";

        /// <summary>
        /// Defines the TestFileMessageAfterDbConnection.
        /// </summary>
        private const string TestFileMessageAfterDbConnection = "Congratulations! The integration test file has successfully been created! Message returned by called db connection: ";

        #endregion

        #region Fields

        /// <summary>
        /// Defines the aboutInfo.
        /// </summary>
        private readonly AboutDto aboutInfo;

        /// <summary>
        /// Defines the appSettings.
        /// </summary>
        private readonly AppSettings<AppSpecificSettings> appSettings;

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationTestController"/> class.
        /// </summary>
        /// <param name="appSettings">The appSettings<see cref="IOptions{AppSettings}"/>.</param>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/>.</param>
        public IntegrationTestController(
            IOptions<AppSettings<AppSpecificSettings>> appSettings,
            ILogger logger,
            AboutDto aboutInfo)
        {
            this.appSettings = appSettings.Value;
            this.logger = logger;
            this.aboutInfo = aboutInfo;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The TestAifService.
        /// </summary>
        /// <returns>The <see cref="M2FResponse"/>.</returns>
        [HttpGet]
        public M2FResponse TestAifService()
        {
            M2FResponse m2fResp = null;
            string res = string.Empty;
            string xmlValue = string.Empty;
            IntegrationTestDto testDto = new IntegrationTestDto();

            MRL_M2F_IntegrationTest_SrvClient client = this.InitAxServiceConnection();

            try
            {
                testDto.Project = Assembly.GetExecutingAssembly().FullName;
                testDto.TestDateTime = DateTime.Now;

                xmlValue = testDto.SerializeIntegrationTestDto();

                res = client.testAifService(xmlValue);
                m2fResp = this.DeserializeAifServiceResultString(res);
            }
            catch (Exception ex)
            {
                m2fResp = new M2FResponse();
                m2fResp.Status = true;
                m2fResp.Code = -666;
                m2fResp.Object = ex.ToString();
            }

            return m2fResp;
        }

        /// <summary>
        /// The ThrowingExceptionMethod.
        /// </summary>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpGet]
        public ActionResult TestMethodException()
        {
            // viene intercettata dal global exception handling del middleware asp net core
            throw new NullReferenceException("Null reference exception - Test");

            return this.Problem();
        }

        /// <summary>
        /// The TestMethod.
        /// </summary>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpGet]
        public ActionResult TestMethodOk()
        {
            return this.Ok(int.MaxValue);
        }

        /// <summary>
        /// The DeserializeM2FResponseString.
        /// </summary>
        /// <param name="axAifServiceResult">The axAifServiceResult<see cref="string"/>.</param>
        /// <returns>The <see cref="M2FResponse"/>.</returns>
        private M2FResponse DeserializeAifServiceResultString(string axAifServiceResult)
        {
            M2FResponse m2fRespAxAifService;

            XmlRootAttribute xRootResponse = new XmlRootAttribute();
            xRootResponse.ElementName = "root";
            xRootResponse.IsNullable = true;

            XmlSerializer serializerResponse = new XmlSerializer(typeof(M2FResponse), xRootResponse);

            using (var readerResp = XmlReader.Create(new StringReader(axAifServiceResult)))
            {
                m2fRespAxAifService = serializerResponse.Deserialize(readerResp, string.Empty) as M2FResponse;
            }

            return m2fRespAxAifService;
        }

        /// <summary>
        /// The InitAxServiceConnection.
        /// </summary>
        /// <returns>The <see cref="MRL_M2F_IntegrationTest_SrvClient"/>.</returns>
        private MRL_M2F_IntegrationTest_SrvClient InitAxServiceConnection()
        {
            var binding = new BasicHttpBinding();

            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;

            // L'impostazione del ProxyCredentialType sembra non essere necessaria.
            // Per questo motivo il codice è stato commentato.

            ////basicHttpBinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.Ntlm;

            binding.OpenTimeout = new TimeSpan(0, 10, 0);
            binding.CloseTimeout = new TimeSpan(0, 10, 0);
            binding.SendTimeout = new TimeSpan(0, 10, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 10, 0);

            binding.MaxBufferSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferPoolSize = int.MaxValue;

            binding.ReaderQuotas.MaxArrayLength = int.MaxValue;
            binding.ReaderQuotas.MaxBytesPerRead = int.MaxValue;
            binding.ReaderQuotas.MaxDepth = int.MaxValue;
            binding.ReaderQuotas.MaxNameTableCharCount = int.MaxValue;
            binding.ReaderQuotas.MaxStringContentLength = int.MaxValue;

            // BaseURI degli AIF Service di AX2009
            var baseAifUri = new Uri(this.appSettings.Ax2009AifServicesBaseSettings.BaseUrl);

            // relativeuri degli aif service di ax2009
            var aifRelativeUri =
                this.appSettings.Ax2009AifServicesSettings
                .FirstOrDefault(
                    aifsrv =>
                        aifsrv
                            .ServiceName
                            .ToLower()
                            .EndsWith(nameof(MRL_M2F_IntegrationTest_SrvClient).ToLower().Replace(CedentialConstantDto.ClientSuffixStringInWcfClientName, string.Empty)));

            // Creazione Endpoint senza EndpointIdentity
            EndpointAddress endpoint = new EndpointAddress(new Uri(baseAifUri, $"{aifRelativeUri.ServiceName}.svc"));

            // Creazione client WCF
            var client = new MRL_M2F_IntegrationTest_SrvClient(binding, endpoint);

            // Creazione istanza di NetworkCredential per user "axsvc"
            NetworkCredential netCred = new NetworkCredential(CedentialConstantDto.NetworkCredentialUsername, CedentialConstantDto.NetworkCredentialPassword, CedentialConstantDto.NetworkCredentialDomain);
            client.ClientCredentials.Windows.ClientCredential = netCred;
            client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

            // Apertura canale di comunicazione con endpoint WCF
            client.OpenAsync().Wait();

            return client;
        }

        #endregion
    }
}
