﻿using M2FControllers.LogHelpers;
using M2FDto.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Serilog;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace M2FControllers.Middlewares
{
    /// <summary>
    /// Defines the <see cref="RequestResponseLoggingMiddleware" />.
    /// </summary>
    public class RequestResponseLoggingMiddleware
    {
        #region Fields

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Defines the next.
        /// </summary>
        private readonly RequestDelegate next;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestResponseLoggingMiddleware"/> class.
        /// </summary>
        /// <param name="next">The next<see cref="RequestDelegate"/>.</param>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        public RequestResponseLoggingMiddleware(RequestDelegate next, ILogger logger)
        {
            this.next = next;
            this.logger = logger;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ClientSoftwareVersion.
        /// </summary>
        internal string ClientSoftwareVersion { get; set; }

        /// <summary>
        /// Gets or sets the ServerSoftwareVersion.
        /// </summary>
        internal string ServerSoftwareVersion { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The Invoke.
        /// </summary>
        /// <param name="context">The context<see cref="HttpContext"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task Invoke(HttpContext context)
        {
            // log request
            LogInfo logRequestInfo = new LogInfo();
            LogHelper logHelper = new LogHelper(this.logger);
            logRequestInfo.IsRequest = true;
            logRequestInfo.RemoteIpAddress = context.Connection.RemoteIpAddress;
            logRequestInfo.RemotePort = context.Connection.RemotePort;
            logRequestInfo.LocalIpAddress = context.Connection.LocalIpAddress;
            logRequestInfo.LocalPort = context.Connection.LocalPort;

            logRequestInfo.Path = context.Request.Path.ToString();

            logRequestInfo.User = string.IsNullOrWhiteSpace(context.User?.Identity?.Name) ? "<ANONYMOUS>" : context.User.Identity.Name;
            logRequestInfo.TraceIdentifier = context.TraceIdentifier;

            logHelper.LogInfo(logRequestInfo);
            try
            {
                if (this.CheckSoftwareVersionBetweenClientAndServer(context))
                {
                    // passo al successivo componente della pipeline
                    await this.next(context);
                }
                else
                {
                    this.logger.Error($"CLIENT Software Version {this.ClientSoftwareVersion} is not equals to SERVER Software Version {this.ServerSoftwareVersion}");

                    context.Response.StatusCode = StatusCodes.Status403Forbidden;
                }
            }

            // il log delle eccezioni viene gestito dal global exception handler
            finally
            {
                // log response
                logRequestInfo.EvaluateElapsed();
                logRequestInfo.TimeStart = DateTime.Now;
                logRequestInfo.IsRequest = false;
                logRequestInfo.HttpStatus = context.Response.StatusCode;

                logHelper.LogInfo(logRequestInfo);
            }
        }

        /// <summary>
        /// The CheckSoftwareVersionBetweenClientAndServer.
        /// </summary>
        /// <param name="context">The context<see cref="HttpContext"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        private bool CheckSoftwareVersionBetweenClientAndServer(HttpContext context)
        {
            bool softwareVersionCheckResult = false;

            Assembly entryAsm = Assembly.GetEntryAssembly();

            this.ServerSoftwareVersion = ((AssemblyInformationalVersionAttribute)Attribute.GetCustomAttribute(entryAsm, typeof(AssemblyInformationalVersionAttribute), false))?.InformationalVersion.ToString();

            StringValues softwareVersionFromHeader = string.Empty;

            ////// Test
            ////if (context.Request.Path.HasValue
            ////    && context.Request.Path.Value.StartsWith("/api/About/"))
            ////{
            ////    softwareVersionCheckResult = true;
            ////}

            if (context.Request.Path.HasValue
            && context.Request.Path.Value.StartsWith("/api/")
            && !context.Request.Path.Value.StartsWith("/api/Logger/"))
            {
                if (context.Request.Headers.TryGetValue(HttpConstantDto.DefaultHeaderValue, out softwareVersionFromHeader) && softwareVersionFromHeader.Count > 0)
                {
                    this.ClientSoftwareVersion = softwareVersionFromHeader[0];

                    if (string.Compare(this.ClientSoftwareVersion, this.ServerSoftwareVersion, true) == 0)
                    {
                        softwareVersionCheckResult = true;
                    }
                }
            }
            else
            {
                softwareVersionCheckResult = true;
            }

            return softwareVersionCheckResult;
        }

        #endregion
    }
}
