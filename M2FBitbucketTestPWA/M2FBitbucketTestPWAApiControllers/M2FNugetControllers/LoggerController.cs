﻿using M2FDto;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;

namespace M2FControllers
{
    /// <summary>
    /// Defines the <see cref="LoggerController" />.
    /// </summary>
    public class LoggerController : BaseController
    {
        #region Fields

        /// <summary>
        /// Defines the aboutInfo.
        /// </summary>
        private readonly AboutDto aboutInfo;

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggerController"/> class.
        /// </summary>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/>.</param>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        public LoggerController(AboutDto aboutInfo, ILogger logger)
        {
            this.aboutInfo = aboutInfo;
            this.logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The Debug.
        /// </summary>
        /// <param name="input">The input<see cref="string"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpPost]
        public ActionResult Debug([FromBody] string input)
        {
            try
            {
                this.logger.Debug($"Client {this.HttpContext.Connection.RemoteIpAddress}:{this.HttpContext.Connection.RemotePort}: {input}");

                return this.Ok();
            }
            catch (Exception ex)
            {
                this.logger.Error($"Exception Writing Client Log. {ex.Message}");
                return this.Problem();
            }
        }

        /// <summary>
        /// The Error.
        /// </summary>
        /// <param name="input">The input<see cref="string"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpPost]
        public ActionResult Error([FromBody] string input)
        {
            try
            {
                this.logger.Error($"Client {this.HttpContext.Connection.RemoteIpAddress}:{this.HttpContext.Connection.RemotePort}: {input}");

                return this.Ok();
            }
            catch (Exception ex)
            {
                this.logger.Error($"Exception Writing Client Log. {ex.Message}");
                return this.Problem();
            }
        }

        /// <summary>
        /// The Fatal.
        /// </summary>
        /// <param name="input">The input<see cref="string"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpPost]
        public ActionResult Fatal([FromBody] string input)
        {
            try
            {
                this.logger.Fatal($"Client {this.HttpContext.Connection.RemoteIpAddress}:{this.HttpContext.Connection.RemotePort}: {input}");

                return this.Ok();
            }
            catch (Exception ex)
            {
                this.logger.Error($"Exception Writing Client Log. {ex.Message}");
                return this.Problem();
            }
        }

        /// <summary>
        /// The Information.
        /// </summary>
        /// <param name="input">The input<see cref="string"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpPost]
        public ActionResult Information([FromBody] string input)
        {
            try
            {
                this.logger.Information($"Client {this.HttpContext.Connection.RemoteIpAddress}:{this.HttpContext.Connection.RemotePort}: {input}");

                return this.Ok();
            }
            catch (Exception ex)
            {
                this.logger.Error($"Exception Writing Client Log. {ex.Message}");
                return this.Problem();
            }
        }

        /// <summary>
        /// The Warning.
        /// </summary>
        /// <param name="input">The input<see cref="string"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpPost]
        public ActionResult Warning([FromBody] string input)
        {
            try
            {
                this.logger.Warning($"Client {this.HttpContext.Connection.RemoteIpAddress}:{this.HttpContext.Connection.RemotePort}: {input}");

                return this.Ok();
            }
            catch (Exception ex)
            {
                this.logger.Error($"Exception Writing Client Log. {ex.Message}");
                return this.Problem();
            }
        }

        #endregion
    }
}
