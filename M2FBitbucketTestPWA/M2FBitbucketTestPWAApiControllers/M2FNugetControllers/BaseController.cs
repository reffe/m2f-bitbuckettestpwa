﻿using Microsoft.AspNetCore.Mvc;

namespace M2FControllers
{
    /// <summary>
    /// Defines the <see cref="BaseController" />.
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
    }
}
