﻿using System;
using System.Diagnostics;
using System.Net;

namespace M2FControllers.LogHelpers
{
    /// <summary>
    /// Defines the <see cref="LogInfo" />.
    /// </summary>
    public class LogInfo
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogInfo"/> class.
        /// </summary>
        public LogInfo()
        {
            this.StopWatch = new Stopwatch();
            this.StopWatch.Start();
            this.TimeStart = DateTime.Now;
            this.ElapsedMs = 0;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the CalledMethod.
        /// </summary>
        public string CalledMethod { get; set; }

        /// <summary>
        /// Gets or sets the ControllerName.
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// Gets or sets the ElapsedMs.
        /// </summary>
        public double ElapsedMs { get; set; }

        /// <summary>
        /// Gets or sets the ExceptionMessage.
        /// </summary>
        public string ExceptionMessage { get; set; }

        /// <summary>
        /// Gets or sets the ExceptionType.
        /// </summary>
        public string ExceptionType { get; set; }

        /// <summary>
        /// Gets or sets the HttpStatus.
        /// </summary>
        public int HttpStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsRequest.
        /// </summary>
        public bool IsRequest { get; set; }

        /// <summary>
        /// Gets or sets the LocalIpAddress.
        /// </summary>
        public IPAddress LocalIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the LocalPort.
        /// </summary>
        public int LocalPort { get; set; }

        /// <summary>
        /// Gets or sets the Path.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the RemoteIpAddress.
        /// </summary>
        public IPAddress RemoteIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the RemotePort.
        /// </summary>
        public int RemotePort { get; set; }

        /// <summary>
        /// Gets or sets the StackTrace.
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// Gets or sets the StopWatch.
        /// </summary>
        public Stopwatch StopWatch { get; set; }

        /// <summary>
        /// Gets or sets the StringInputParam.
        /// </summary>
        public string StringInputParam { get; set; }

        /// <summary>
        /// Gets or sets the TimeStart.
        /// </summary>
        public DateTime TimeStart { get; set; }

        /// <summary>
        /// Gets or sets the TraceIdentifier.
        /// </summary>
        public string TraceIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the User.
        /// </summary>
        public string User { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The evaluateElapsed.
        /// </summary>
        public void EvaluateElapsed()
        {
            this.ElapsedMs = this.StopWatch.Elapsed.TotalMilliseconds;
        }

        #endregion
    }
}
