﻿using Serilog;

namespace M2FControllers.LogHelpers
{
    /// <summary>
    /// Defines the <see cref="LogHelper" />.
    /// </summary>
    public class LogHelper
    {
        #region Fields

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogHelper"/> class.
        /// </summary>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        public LogHelper(ILogger logger)
        {
            this.logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The LogError.
        /// </summary>
        /// <param name="logInfo">The logInfo<see cref="LogInfo"/>.</param>
        public void LogError(LogInfo logInfo)
        {
            this.logger.Error("TraceIdentifier:{TraceIdentifier} remoteIpAddress:{RemoteIpAddress} remotePort:{RemotePort} localIpAddress:{LocalIpAddress} localPort:{LocalPort} IsRequest:{IsRequest} controllerName:{ControllerName} calledMethod:{CalledMethod} httpStatus:{HttpStatus} timeStart:{TimeStart:yyyy-MM-dd HH:mm:ss.fffffff} InputParam:{StringInputParam} ElapsedMs:{ElapsedMs} User:{User} ExceptionType:{ExceptionType} ExceptionMessage:{ExceptionMessage} StackTrace:{StackTrace}", logInfo.TraceIdentifier, logInfo.RemoteIpAddress, logInfo.RemotePort, logInfo.LocalIpAddress, logInfo.LocalPort, logInfo.IsRequest, logInfo.ControllerName, logInfo.CalledMethod, logInfo.HttpStatus, logInfo.TimeStart, logInfo.StringInputParam, logInfo.ElapsedMs, logInfo.User, logInfo.ExceptionType, logInfo.ExceptionMessage, logInfo.StackTrace);
        }

        /// <summary>
        /// The LogInfo.
        /// </summary>
        /// <param name="logInfo">The logInfo<see cref="LogInfo"/>.</param>
        public void LogInfo(LogInfo logInfo)
        {
            this.logger.Information("TraceIdentifier:{TraceIdentifier} remoteIpAddress:{RemoteIpAddress} remotePort:{RemotePort} localIpAddress:{LocalIpAddress} localPort:{LocalPort} IsRequest:{IsRequest} Path:{Path} controllerName:{ControllerName} calledMethod:{CalledMethod} httpStatus:{HttpStatus} timeStart:{TimeStart:yyyy-MM-dd HH:mm:ss.fffffff} InputParam:{StringInputParam} Elapsed:{ElapsedMs} User:{User}", logInfo.TraceIdentifier, logInfo.RemoteIpAddress, logInfo.RemotePort, logInfo.LocalIpAddress, logInfo.LocalPort, logInfo.IsRequest, logInfo.Path, logInfo.ControllerName, logInfo.CalledMethod, logInfo.HttpStatus, logInfo.TimeStart, logInfo.StringInputParam, logInfo.ElapsedMs, logInfo.User);
        }

        #endregion
    }
}
