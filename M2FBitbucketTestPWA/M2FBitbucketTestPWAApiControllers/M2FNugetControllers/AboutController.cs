﻿using M2FDto;
using M2FDto.About;
using Microsoft.AspNetCore.Mvc;

namespace M2FControllers
{
    /// <summary>
    /// Defines the <see cref="AboutController" />.
    /// </summary>
    public class AboutController : BaseController
    {
        #region Fields

        /// <summary>
        /// Defines the aboutInfo.
        /// </summary>
        private readonly AboutDto aboutInfo;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutController"/> class.
        /// </summary>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/>.</param>
        public AboutController(AboutDto aboutInfo)
        {
            this.aboutInfo = aboutInfo;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<AssemblyInfoDto> GetAssemblyInformation()
        {
            return this.aboutInfo.AssemblyInformation;
        }

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<DevOpsInfoDto> GetDevOpsInformation()
        {
            return this.aboutInfo.DevOpsInformation;
        }

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<EnvironmentInfoDto> GetEnvironmentInformation()
        {
            return this.aboutInfo.EnvironmentInformation;
        }

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<HttpContextConnectionInfo> GetHttpContextConnectionInformation()
        {
            // Recupero il valore dell'utente loggato che sta effettuando la chiamata HTTP
            this.aboutInfo.LoggedUserInformation.LoggedUser = this.HttpContext.User.Identity.Name;

            // Recupero i valori delle proprietà della classe "HttpContext.Connection"
            this.aboutInfo.HttpContextConnectionInformation.ClientCertificate = this.HttpContext.Connection.ClientCertificate?.ToString();
            this.aboutInfo.HttpContextConnectionInformation.Id = this.HttpContext.Connection.Id;
            this.aboutInfo.HttpContextConnectionInformation.LocalIpAddress = this.HttpContext.Connection.LocalIpAddress.ToString();
            this.aboutInfo.HttpContextConnectionInformation.LocalPort = this.HttpContext.Connection.LocalPort;
            this.aboutInfo.HttpContextConnectionInformation.RemoteIpAddress = this.HttpContext.Connection.RemoteIpAddress.ToString();
            this.aboutInfo.HttpContextConnectionInformation.RemotePort = this.HttpContext.Connection.RemotePort;

            return this.aboutInfo.HttpContextConnectionInformation;
        }

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<AboutDto> GetInfo()
        {
            // Recupero il valore dell'utente loggato che sta effettuando la chiamata HTTP
            this.aboutInfo.LoggedUserInformation.LoggedUser = this.HttpContext.User.Identity.Name;

            // Recupero i valori delle proprietà della classe "HttpContext.Connection"
            this.aboutInfo.HttpContextConnectionInformation.ClientCertificate = this.HttpContext.Connection.ClientCertificate?.ToString();
            this.aboutInfo.HttpContextConnectionInformation.Id = this.HttpContext.Connection.Id;
            this.aboutInfo.HttpContextConnectionInformation.LocalIpAddress = this.HttpContext.Connection.LocalIpAddress.ToString();
            this.aboutInfo.HttpContextConnectionInformation.LocalPort = this.HttpContext.Connection.LocalPort;
            this.aboutInfo.HttpContextConnectionInformation.RemoteIpAddress = this.HttpContext.Connection.RemoteIpAddress.ToString();
            this.aboutInfo.HttpContextConnectionInformation.RemotePort = this.HttpContext.Connection.RemotePort;

            return this.aboutInfo;
        }

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<InstallationPackageInfoDto> GetInstallationPackageInformation()
        {
            return this.aboutInfo.InstallationPackageInformation;
        }

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<LoggedUserInfo> GetLoggedUserInformation()
        {
            return this.aboutInfo.LoggedUserInformation;
        }

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<RuntimeInfoDto> GetRuntimeInformation()
        {
            return this.aboutInfo.RuntimeInformation;
        }

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<SourceCodeRepositoryDto> GetSourceCodeRepositoryInformation()
        {
            return this.aboutInfo.SourceCodeRepositoryInformation;
        }

        #endregion
    }
}
