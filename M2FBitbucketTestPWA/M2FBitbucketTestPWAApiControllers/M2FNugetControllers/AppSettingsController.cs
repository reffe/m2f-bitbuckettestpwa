﻿using M2FDto;
using M2FDto.Constant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace M2FControllers
{
    /// <summary>
    /// Defines the <see cref="AboutController" />.
    /// </summary>
    [Authorize(Roles = IdentityServerConstantDto.AuthorizationRoleAdmin)]
    public class AppSettingsController : BaseController
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AppSettingsController"/> class.
        /// </summary>
        public AppSettingsController()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get MUKCustomsExtraction about info.
        /// </summary>
        /// <returns>Returns an instance of <see cref="AboutDto"/>.</returns>
        [HttpGet]
        public ActionResult<string> GetInfo()
        {
            string appSettingsJsonPath = System.IO.Path.Combine(System.Environment.CurrentDirectory, GeneralConstantDto.AppSettingFileName);

            string appSettingsJsonContent = System.IO.File.ReadAllText(appSettingsJsonPath);

            return appSettingsJsonContent;
        }

        #endregion
    }
}
