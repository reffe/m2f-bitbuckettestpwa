﻿using M2FControllers.LogHelpers;
using M2FDto.Constant;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Net;

namespace M2FControllers
{
    /// <summary>
    /// Defines the <see cref="GlobalExceptionHandlingController" />.
    /// </summary>
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class GlobalExceptionHandlingController : ControllerBase
    {
        #region Fields

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalExceptionHandlingController"/> class.
        /// </summary>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        public GlobalExceptionHandlingController(ILogger logger)
        {
            this.logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The HandleErrors.
        /// </summary>
        /// <returns>The <see cref="IActionResult"/>.</returns>
        [HttpGet]
        [Route(GeneralConstantDto.ErrorPage)]
        public IActionResult HandleErrors()
        {
            // capture the exception
            var contentException = this.HttpContext.Features.Get<IExceptionHandlerFeature>();
            var controllerName = contentException.Error.Source;
            var methodName = contentException.Error.TargetSite.Name;
            var stackTrace = contentException.Error.StackTrace;

            // set always bad request http status code
            var responseStautsCode = HttpStatusCode.InternalServerError;

            // info per log
            LogInfo logInfo = new LogInfo();
            LogHelper logHelper = new LogHelper(this.logger);

            logInfo.RemoteIpAddress = this.HttpContext.Connection.RemoteIpAddress;
            logInfo.RemotePort = this.HttpContext.Connection.RemotePort;
            logInfo.LocalIpAddress = this.HttpContext.Connection.LocalIpAddress;
            logInfo.LocalPort = this.HttpContext.Connection.LocalPort;
            logInfo.TraceIdentifier = this.HttpContext.TraceIdentifier;

            logInfo.ControllerName = controllerName;
            logInfo.CalledMethod = methodName;

            logInfo.HttpStatus = (int)responseStautsCode;

            logInfo.ExceptionMessage = contentException.Error.Message;
            logInfo.ExceptionType = contentException.Error.GetType().Name;
            logInfo.StackTrace = stackTrace;
            logInfo.ElapsedMs = -1;

            logInfo.User = string.IsNullOrWhiteSpace(this.HttpContext.User?.Identity?.Name) ? "<ANONYMOUS>" : this.HttpContext.User.Identity.Name;

            logHelper.LogError(logInfo);

            return this.Problem(detail: logInfo.StackTrace, title: logInfo.ExceptionMessage, statusCode: (int)logInfo.HttpStatus);
        }

        #endregion
    }
}
