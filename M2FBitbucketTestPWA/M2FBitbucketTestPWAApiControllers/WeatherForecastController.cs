using M2FBitbucketTestPWADto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2FBitbucketTestPWAServer.Controllers
{
    /// <summary>
    /// Defines the <see cref="WeatherForecastController" />.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        #region Fields

        /// <summary>
        /// Defines the Summaries.
        /// </summary>
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching",
        };

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger<WeatherForecastController> logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WeatherForecastController"/> class.
        /// </summary>
        /// <param name="logger">The logger<see cref="ILogger{WeatherForecastController}"/>.</param>
        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            this.logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The Get.
        /// </summary>
        /// <returns>The <see cref="IEnumerable{WeatherForecast}"/>.</returns>
        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)],
            })
            .ToArray();
        }

        #endregion
    }
}
