using M2FBitbucketTestPWADto;
using M2FDto;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Serilog;

namespace M2FBitbucketTestPWAApiControllers
{
    /// <summary>
    /// Defines the <see cref="OidcConfigurationController" />.
    /// </summary>
    public class OidcConfigurationController : Controller
    {
        #region Fields

        /// <summary>
        /// Defines the aboutInfo.
        /// </summary>
        private readonly AboutDto aboutInfo;

        /// <summary>
        /// Defines the appSettings.
        /// </summary>
        private readonly AppSettings<AppSpecificSettings> appSettings;

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OidcConfigurationController"/> class.
        /// </summary>
        /// <param name="appSettings">The appSettings<see cref="IOptions{AppSettings{AppSpecificSettings}}"/>.</param>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/>.</param>
        /// <param name="clientRequestParametersProvider">The clientRequestParametersProvider<see cref="IClientRequestParametersProvider"/>.</param>
        public OidcConfigurationController(
            IOptions<AppSettings<AppSpecificSettings>> appSettings,
            ILogger logger,
            AboutDto aboutInfo,
            IClientRequestParametersProvider clientRequestParametersProvider)
        {
            this.appSettings = appSettings.Value;
            this.logger = logger;
            this.aboutInfo = aboutInfo;
            this.ClientRequestParametersProvider = clientRequestParametersProvider;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the ClientRequestParametersProvider.
        /// </summary>
        public IClientRequestParametersProvider ClientRequestParametersProvider { get; }

        #endregion

        #region Methods

        /// <summary>
        /// The GetClientRequestParameters.
        /// </summary>
        /// <param name="clientId">The clientId<see cref="string"/>.</param>
        /// <returns>The <see cref="IActionResult"/>.</returns>
        [HttpGet("_configuration/{clientId}")]
        public IActionResult GetClientRequestParameters([FromRoute] string clientId)
        {
            var parameters = this.ClientRequestParametersProvider.GetClientParameters(this.HttpContext, clientId);
            return this.Ok(parameters);
        }

        #endregion
    }
}
