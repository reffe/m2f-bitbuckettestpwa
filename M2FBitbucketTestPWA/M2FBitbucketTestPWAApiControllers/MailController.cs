using M2FBitbucketTestPWADto;
using M2FBitbucketTestPWADto.Mail;
using M2FControllers;
using M2FDto;
using M2FUtility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.Collections.Generic;

namespace M2FBitbucketTestPWAApiControllers
{
    /// <summary>
    /// Defines the <see cref="MailController" />.
    /// </summary>
    public partial class MailController : BaseController
    {
        #region Fields

        /// <summary>
        /// Defines the aboutInfo.
        /// </summary>
        private readonly AboutDto aboutInfo;

        /// <summary>
        /// Defines the appSettings.
        /// </summary>
        private readonly AppSettings<AppSpecificSettings> appSettings;

        /// <summary>
        /// Defines the logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Defines the mailManagement.
        /// </summary>
        private readonly MailManagement mailManagement;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MailController"/> class.
        /// </summary>
        /// <param name="appSettings">The appSettings<see cref="IOptions{AppSettings}"/>.</param>
        /// <param name="logger">The logger<see cref="ILogger"/>.</param>
        /// <param name="aboutInfo">The aboutInfo<see cref="AboutDto"/>.</param>
        /// <param name="mailManagement">The mailManagement<see cref="MailManagement"/>.</param>
        public MailController(
            IOptions<AppSettings<AppSpecificSettings>> appSettings,
            ILogger logger,
            AboutDto aboutInfo,
            MailManagement mailManagement)
        {
            this.appSettings = appSettings.Value;
            this.logger = logger;
            this.aboutInfo = aboutInfo;
            this.mailManagement = mailManagement;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The TestAifService.
        /// </summary>
        /// <param name="mail">The input<see cref="MailDto"/>.</param>
        /// <returns>The <see cref="ActionResult"/>.</returns>
        [HttpPost]
        public ActionResult SendMail([FromBody] MailDto mail)
        {
            try
            {
                ////var mail = System.Text.Json.JsonSerializer.Deserialize<MailDto>(input);

                string officiality = this.aboutInfo.DevOpsInformation.Officiality;
                if (string.IsNullOrWhiteSpace(officiality))
                {
                    officiality = "VS";
                }

                this.mailManagement.SendMail(
                    mail.FullName,
                    mail.MailAddress,
                    mail.MailSubject + " - Officiality " + officiality,
                    mail.MailBody,
                    mail.Attachments,
                    new List<string>(),
                    this.appSettings.NotificationSettings.NotificationEventSettings.Find(element => element.EventName.Equals(mail.EventName)).To,
                    this.appSettings.NotificationSettings.NotificationEventSettings.Find(element => element.EventName.Equals(mail.EventName)).Cc);

                return this.Ok(int.MaxValue);
            }
            catch (Exception ex)
            {
                this.logger.Error($"Exception Writing Client Log. {ex.Message}");
                return this.Problem();
            }
        }

        #endregion
    }
}
